/*
Copyright 2007-2023 Guillaume Hanrot, Vincent Lefèvre, Damien Stehlé 
and Paul Zimmermann.

  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful, 

  but WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public 
  License along with this program; see the file COPYING.  If not, 

  write to the Free Software Foundation, Inc., 59 Temple Place - 
  Suite 330, Boston, MA 02111-1307, USA.
*/

#define NAME "hall" /* x^(3/2) */
#define EVAL(y, rnd_mode)			\
  mpfr_pow_ui (y, y, 3, rnd_mode);		\
  mpfr_sqrt (y, y, rnd_mode);

/* BNDDIFF(tmp, t0, t1, N, d) returns a bound for the (d+1)-th
   derivative of x^(3/2) for t0/N <= x < t1/N.
   d=1: the 2nd derivative is 3/4/sqrt(x).
   d=2: the 3rd derivative is -3/8/x^(3/2). */
#define BNDDIFF(tmp, t0, t1, N, d)                              \
  assert (mpz_cmp_ui (t0, 0) > 0);                              \
  assert (d <= 2);                                              \
  if (d == 1) {                                                 \
  mpfr_div_z (tmp, N, t0, MPFR_RNDU);                           \
  mpfr_sqrt (tmp, tmp, MPFR_RNDU);                              \
  mpfr_mul_ui (tmp, tmp, 3, MPFR_RNDU);                         \
  mpfr_div_ui (tmp, tmp, 4, MPFR_RNDU);                         \
  } else if (d == 2) {                                          \
  mpfr_div_z (tmp, N, t0, MPFR_RNDU);                           \
  mpfr_pow_ui (tmp, tmp, 3, MPFR_RNDU);                         \
  mpfr_sqrt (tmp, tmp, MPFR_RNDU);                              \
  mpfr_mul_ui (tmp, tmp, 3, MPFR_RNDU);                         \
  mpfr_div_ui (tmp, tmp, 8, MPFR_RNDU); }                       \

#define TAYLOR(tmp, tm, Nin, P, NN, d)			\
  { long i;						\
    mpfr_set_z (tmp, tm, MPFR_RNDN);			\
    mpfr_div (tmp, tmp, Nin, MPFR_RNDN);                \
    mpfr_pow_ui (P[0], tmp, 3, MPFR_RNDN);		\
    mpfr_sqrt (P[0], P[0], MPFR_RNDN);			\
    mpfr_ui_div (tmp, 1, tmp, MPFR_RNDN);		\
    mpfr_mul (P[0], P[0], NN, MPFR_RNDN);		\
    mpfr_mul (P[1], P[0], tmp, MPFR_RNDN);		\
    mpfr_mul_ui (P[1], P[1], 3, MPFR_RNDN);		\
    mpfr_div_ui (P[1], P[1], 2, MPFR_RNDN);		\
    mpfr_mul_z (P[1], P[1], T, MPFR_RNDN);		\
    mpfr_div (P[1], P[1], Nin, MPFR_RNDN);		\
    for (i=2; i<=d; i++) {				\
      mpfr_neg (P[i], P[i-1], MPFR_RNDN);		\
      mpfr_mul_z (P[i], P[i], T, MPFR_RNDN);		\
      mpfr_div (P[i], P[i], Nin, MPFR_RNDN);		\
      mpfr_mul (P[i], P[i], tmp, MPFR_RNDN);		\
      mpfr_div_ui (P[i], P[i], i, MPFR_RNDN);		\
      mpfr_div_ui (P[i], P[i], 2, MPFR_RNDN);		\
      mpfr_mul_si (P[i], P[i], 2*i-5, MPFR_RNDN);	\
    }                                                   \
  }
