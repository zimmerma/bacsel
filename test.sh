#!/bin/bash
# Examples from README
out=`mktemp`
make clean
make GMP=$GMP MPFR=$MPFR FPLLL=$FPLLL
echo Test 1 > $out
./bacsel -rnd_mode directed -n 8 -nn 8 -m 8.0 -t 2.0 -t0 70000000 -t1 100000000 -d 1 -alpha 1 -q >> $out
make clean
make MPFR=$MPFR GMP=$GMP FPLLL=$FPLLL DEFSAL="-DSIN -DAUTOMATIC -DBASIS=10 -DVBSE"
echo Test 2 >> $out
./bacsel -rnd_mode nearest -n 8 -nn 8 -m 8.0 -t 2.0 \
         -t0 60000000 -t1 100000000 -d 1 -alpha 1 -q >> $out
make clean
make MPFR=$MPFR GMP=$GMP FPLLL=$FPLLL DEFSAL="-DEXP -DAUTOMATIC -DBASIS=2 -DVBSE"
echo Test 3a >> $out
./bacsel -rnd_mode nearest -n 24 -nn 25 -m 22.0 -t 7.0 \
         -t0 11629080 -t1 16777216 -d 2 -alpha 2 -q >> $out
echo Test 3b >> $out
./bacsel -rnd_mode directed -n 24 -nn 25 -m 22.0 -t 7.0 \
         -t0 11629080 -t1 16777216 -d 2 -alpha 2 -q >> $out
echo Test 3c >> $out
./bacsel -rnd_mode all -n 24 -nn 25 -m 22.0 -t 7.0 \
         -t0 11629080 -t1 16777216 -d 2 -alpha 2 -q >> $out
echo Test 3d >> $out
./bacsel -rnd_mode all -prec 128 -n 53 -nn 54 -m 40 -t 18 \
-t0 4503599627370496 -t1 4504699138998272 -d 2 -alpha 2 -nthreads 4 -q | sort -n >> $out
# the following test does not work any more since e4bda4c
# make clean
# make GMP=$GMP MPFR=$MPFR FPLLL=$FPLLL DEFSAL="-DSINBIG -DBASIS=2"
# echo Test 4a >> $out
# ./bacsel -rnd_mode all -prec 128 -n 53 -nn 53 -m 17 -t 7 -Q 15106909301 \
# -t0 4503599627370496 -t1 9007199254740992 -d 1 -alpha 1 -e_in 1024 -q >> $out
# echo Test 4b >> $out
# ./bacsel -rnd_mode all -prec 128 -n 53 -nn 53 -m 25 -t 7 -Q 15106909301 -q \
# -t0 4503599627370496 -t1 9007199254740992 -d 1 -alpha 1 -e_in 1024 -nbr 200 >> $out
# echo Test 4c >> $out
# ./bacsel -rnd_mode all -prec 128 -n 53 -nn 53 -m 26 -t 7 -Q 15106909301 -q \
#         -t0 4503599627370496 -t1 9007199254740992 -d 1 -alpha 1 -e_in 1024 \
#         -nbr 500 -nthreads 4 >> $out
make clean
make GMP=$GMP MPFR=$MPFR FPLLL=$FPLLL DEFSAL="-DCBRT -DBASIS=2"
echo Test 5a >> $out
./bacsel -rnd_mode all -prec 128 -n 53 -nn 53 -m 38 -t 16 -t0 4503599627370496 -t1 4503737066323968 -d 1 -alpha 1 -nthreads 4 -q | sort -n >> $out
echo Test 5b >> $out
./bacsel -rnd_mode all -prec 128 -n 53 -nn 53 -m 38 -t 19 -t0 4503599627370496 -t1 4503737066323968 -d 2 -alpha 2 -nthreads 4 -q | sort -n >> $out
echo Test 5c >> $out
./bacsel -rnd_mode all -prec 192 -n 64 -nn 64 -m 43 -t 20 -t0 9223372036854775808 -t1 9223374235878031360 -d 1 -alpha 1 -nthreads 4 -q | sort -n >> $out
echo Test 5d >> $out
./bacsel -rnd_mode all -prec 192 -n 64 -nn 64 -m 43 -t 22 -t0 9223372036854775808 -t1 9223374235878031360 -d 2 -alpha 2 -nthreads 4 -q | sort -n >> $out
make clean
make GMP=$GMP MPFR=$MPFR FPLLL=$FPLLL DEFSAL="-DACOS -DBASIS=2"
echo Test 6a >> $out
./bacsel -rnd_mode all -prec 128 -n 53 -nn 53 -m 37 -t 16 -t0 4503599627370496 -t1 4503737066323968 -d 1 -alpha 1 -nthreads 4 -q >> $out
make clean
make GMP=$GMP MPFR=$MPFR FPLLL=$FPLLL DEFSAL="-DPOW -DBASIS=2 -DAUTOMATIC"
echo Test 7 >> $out
./bacsel -rnd_mode all -prec 256 -n 64 -nn 64 -m 169 -t 30 -t0 9223372036854775808 -t1 9223372036854775809 -d 4 -alpha 2 -e_in 1 -y 0x1.9p-1 -q >> $out
diff $out test.out > /dev/null
if [ $? == 0 ]; then
   echo "All tests are ok"
   rm $out
else
   echo "Tests failed"
   echo "diff test.out $out"
fi
