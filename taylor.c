/*
  Copyright 2007-2020 Guillaume Hanrot, Vincent Lefèvre, Damien 
  Stehlé and Paul Zimmermann.

  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful, 
  but WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public 
  License along with this program; see the file COPYING.  If not, 
  write to the Free Software Foundation, Inc., 59 Temple Place - 
  Suite 330, Boston, MA 02111-1307, USA.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <gmp.h>
#include <mpfr.h>
#include <assert.h>
#include "taylor.h"
#define NEED_TABLES
#include "function.h"
#include "io.h"

/* Put in 'eps' an upper bound for the remainder of the order d-Taylor
   formula on [t0, t1]. */ 
void
bnddiff (mpfr_t eps, mpfr_t tau MAYBE_UNUSED, mpfr_t tmp,
         mpfr_t Nin MAYBE_UNUSED, mpz_t T, const mpz_t t0 MAYBE_UNUSED,
         const mpz_t t1 MAYBE_UNUSED, unsigned long d)
{
  unsigned long i; 
  mpz_t u1;

  /* Bounding eps once for all: 
     eps <= max_{t0/N..t1/N} [ |(d^(d+1) f)/(dx)^(d+1) | ] 
            * 1/(d+1)! * [T^(d+1)/Nin^(d+1)]

     It remains to multiply eps by NN (afterwards).
*/

  mpfr_set_z (eps, T, MPFR_RNDU);
  mpfr_pow_ui (eps, eps, d+1, MPFR_RNDU);
  /* eps = T^(d+1) */

#ifdef SINBIG
#if DEBUG >= 2
  printf ("tau="); mpfr_out_str (stdout, 10, 10, tau, MPFR_RNDN); printf ("\n");
#endif
  /* the Taylor error is of the order of 
     1/(d+1)! * 2^(nn - e_out) * (T*tau)^(d+1) */
  mpfr_abs (tmp, tau, MPFR_RNDU);
  mpfr_ui_div (tmp, 1, tmp, MPFR_RNDD);
#else
  mpfr_set (tmp, Nin, MPFR_RNDD);
#endif

  mpfr_pow_ui (tmp, tmp, d+1, MPFR_RNDD);
  mpfr_div (eps, eps, tmp, MPFR_RNDU);
  /* eps is now (T/Nin)^(d+1) */

  for (i=2; i<=d+1; i++)
    mpfr_div_ui (eps, eps, i, MPFR_RNDU);

  /* eps is now 1/(d+1)! * (T/Nin)^(d+1) */

  /* eps is now 1/(d+1)! * [normNN*T^(d+1)/N^(d+1)] */
  /* At this point, t0 and t1 have the same sign */

  /* Since we check on [t0,t1), we pass t1-1 to BNDDIFF. */
  mpz_init (u1);
  mpz_sub_ui (u1, t1, 1);
  BNDDIFF(tmp, t0, u1, Nin, d);
  mpz_clear (u1);
  /* check tmp is positive */
  if (mpfr_sgn (tmp) < 0)
  {
    fprintf (stderr, "Error, bound for %lu-th derivative is negative\n", d+1);
    exit (1);
  }

  mpfr_mul (eps, eps, tmp, MPFR_RNDU);

  return; 
}
