/*
  Copyright 2007-2022 Guillaume Hanrot, Vincent Lefèvre, Damien Stehlé 
  and Paul Zimmermann.

  This program is free software; you can redistribute it and/or modify 
  it under the terms of the GNU General Public License as published by 
  the Free Software Foundation; either version 2 of the License, or 
  (at your option) any later version.

  This program is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
  General Public License for more details.

  You should have received a copy of the GNU General Public License 
  along with this program; see the file COPYING.  If not, write to 
  the Free Software Foundation, Inc., 59 Temple Place - Suite 330, 
  Boston, MA 02111-1307, USA.
*/

#define NAME "exp10"
#define EVAL(y, rnd_mode) mpfr_exp10 (y, y, rnd_mode);
/* BNDDIFF(tmp, t0, t1, N, d) returns a bound for the (d+1)-th
   derivative of exp10(x) for t0/N <= x < t1/N. Since the n-th derivative of
   exp10(x) is exp10(x)*log(10)^n, and exp10(x) is increasing, we use
   exp10(t1/N)*log(10)^(d+1) as bound. */
#define BNDDIFF(tmp, t0, t1, N, d)              \
  mpfr_set_z (tmp, t1, MPFR_RNDU);              \
  mpfr_div (tmp, tmp, N, MPFR_RNDU);            \
  mpfr_exp10 (tmp, tmp, MPFR_RNDU);             \
  { mpfr_t ump;                                 \
  mpfr_init2 (ump, mpfr_get_prec (tmp));        \
  mpfr_set_ui (ump, 10, MPFR_RNDU);             \
  mpfr_log (ump, ump, MPFR_RNDU);               \
  mpfr_pow_ui (ump, ump, d+1, MPFR_RNDU);       \
  mpfr_mul (tmp, tmp, ump, MPFR_RNDU);          \
  mpfr_clear (ump); }

/* TAYLOR(tmp,tm,Nin,P,NN,d) puts in P[0]..P[d] the Taylor expansion
   of NN*exp10(tm/Nin+x) at x=0, with the coefficient of degree i scaled by
   (T/Nin)^i (T is a global variable of type mpz_t) */
#define TAYLOR(tmp, tm, Nin, P, NN, d)		\
  { long i;					\
    mpfr_set_z (tmp, tm, MPFR_RNDN);		\
    mpfr_div (tmp, tmp, Nin, MPFR_RNDN);        \
    mpfr_exp10 (P[0], tmp, MPFR_RNDN);		\
    mpfr_mul (P[0], P[0], NN, MPFR_RNDN);	\
    mpfr_set_ui (tmp, 10, MPFR_RNDN);		\
    mpfr_log (tmp, tmp, MPFR_RNDN);		\
    for (i=1; i<=d; i++) {			\
      mpfr_mul_z (P[i], P[i-1], T, MPFR_RNDN);	\
      mpfr_div (P[i], P[i], Nin, MPFR_RNDN);	\
      mpfr_div_ui (P[i], P[i], i, MPFR_RNDN);	\
      mpfr_mul (P[i], P[i], tmp, MPFR_RNDN);	\
    }                                           \
  }
