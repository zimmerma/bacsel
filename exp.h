/*
  Copyright 2007-2020 Guillaume Hanrot, Vincent Lefèvre, Damien Stehlé 
  and Paul Zimmermann.

  This program is free software; you can redistribute it and/or modify 
  it under the terms of the GNU General Public License as published by 
  the Free Software Foundation; either version 2 of the License, or 
  (at your option) any later version.

  This program is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
  General Public License for more details.

  You should have received a copy of the GNU General Public License 
  along with this program; see the file COPYING.  If not, write to 
  the Free Software Foundation, Inc., 59 Temple Place - Suite 330, 
  Boston, MA 02111-1307, USA.
*/

#define NAME "exp"
#define EVAL(y, rnd_mode) mpfr_exp (y, y, rnd_mode);
/* BNDDIFF(tmp, t0, t1, N, d) returns a bound for the (d+1)-th
   derivative of exp(x) for t0/N <= x < t1/N. Since the n-th derivative of
   exp(x) is itself, and exp(x) is increasing, we use exp(t1/N) as bound. */
#define BNDDIFF(tmp, t0, t1, N, d)              \
  mpfr_set_z (tmp, t1, MPFR_RNDU);              \
  mpfr_div (tmp, tmp, N, MPFR_RNDU);            \
  mpfr_exp (tmp, tmp, MPFR_RNDU)

#define TAYLOR(tmp, tm, Nin, P, NN, d)          \
  {                                             \
    int i;                                      \
    mpfr_set_z (tmp, tm, MPFR_RNDN);            \
    mpfr_div (tmp, tmp, Nin, MPFR_RNDN);        \
    /* tmp = tm/Nin */                          \
    mpfr_exp (tmp, tmp, MPFR_RNDN);             \
    /* tmp = exp(tm/Nin) */                     \
    mpfr_mul (P[0], tmp, NN, MPFR_RNDN);        \
    /* P[0] = NN*exp(tm/Nin) */                 \
    for (i=1; i<=d; i++) {                      \
      mpfr_mul_z (P[i], P[i-1], T, MPFR_RNDN);  \
      mpfr_div (P[i], P[i], Nin, MPFR_RNDN);    \
      mpfr_div_ui (P[i], P[i], i, MPFR_RNDN);   \
      /* P[i] = NN*exp(tm/Nin)*(T/Nin)^i/i! */  \
    }                                           \
  }


    /* compute series expansion 
       P(x)=NN*[exp(tm/normN) +exp(tm.2^e_in/normN)*T/normN*x+...
       +exp(tm.2^e_in/normN)/d! *T^d/normN^d*x^d]

       if |NN.f(x_0/normN) cmod 1| < 1/M, 
       then |P((x_0-tm).2^e_in/T) cmod 1| < 1/C,
       where  |x0-tm|.2^e_in \leq T and C = floor (1/ (1/M+eps)).
    */

