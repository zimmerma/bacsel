/* Copyright 2007-2023 Guillaume Hanrot, Vincent Lefèvre, Damien Stehlé 
   and Paul Zimmermann.

  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful, 
  but WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public 
  License along with this program; see the file COPYING.  If not, 
  write to the Free Software Foundation, Inc., 59 Temple Place - 
  Suite 330, Boston, MA 02111-1307, USA.
*/

#include <assert.h>

#define NAME "tanh"
#define EVAL(y, rnd_mode) mpfr_tanh (y, y, rnd_mode);

/* tmp <- 2*(1 - tanh(x)^2)*tanh(x), where one can modify x */
static inline void
bound_d2 (mpfr_t tmp, mpfr_t x)
{
  mpfr_tanh (x, x, MPFR_RNDN);             /* tanh(x) */
  mpfr_mul (tmp, x, x, MPFR_RNDN);         /* tanh(x)^2 */
  mpfr_ui_sub (tmp, 1, tmp, MPFR_RNDN);    /* 1-tanh(x)^2 */
  mpfr_mul (tmp, tmp, x, MPFR_RNDN);       /* (1 - tanh(x)^2)*tanh(x) */
  mpfr_mul_2exp (tmp, tmp, 1, MPFR_RNDN);
}

/* tmp <- 2*(3*tanh(x)^2-1)*(1-tanh(x)^2), where one can modify x */
static inline void
bound_d3 (mpfr_t tmp, mpfr_t x)
{
  mpfr_tanh (x, x, MPFR_RNDN);             /* tanh(x) */
  mpfr_mul (x, x, x, MPFR_RNDN);           /* tanh(x)^2 */
  mpfr_ui_sub (tmp, 1, x, MPFR_RNDN);      /* 1-tanh(x)^2 */
  mpfr_mul_ui (x, x, 3, MPFR_RNDN);        /* 3*tanh(x)^2 */
  mpfr_sub_ui (x, x, 1, MPFR_RNDN);        /* 3*tanh(x)^2-1 */
  mpfr_mul (tmp, tmp, x, MPFR_RNDN);       /* (3*tanh(x)^2-1)*(1-tanh(x)^2) */
  mpfr_mul_2exp (tmp, tmp, 1, MPFR_RNDN);
}

/* BNDDIFF(tmp, t0, t1, N, d) returns a bound for the (d+1)-th
   derivative of tanh(x) for t0/N <= x < t1/N. We only implement
   it for d=1 and d=2 so far. For d=1 the 2nd derivative of tanh(x)
   is -2*(1 - tanh(x)^2)*tanh(x), and is maximal in absolute value
   at X1=atanh(1/sqrt(3)) ~ 0.658. For d=2 the 3rd derivative of tanh(x)
   is 2*(3*tanh(x)^2-1)*(1-tanh(x)^2), which increases from 0 to X2,
   where X2=atanh(sqrt(6)/3), and decreases from X2 to infinity, thus:
   (a) if t1/N < X2 or X2 < t0/N, the maximum is attained at t0/N or t1/N
   (b) otherwise t0/N < X2 < t1/N, and we bound by the value at X2, which is
       2/3 */
#define X1 0x1.5124271980434p-1 /* atanh(1/sqrt(3)) */
#define X2 0x1.256e66a48a3b6p+0 /* atanh(sqrt(6)/3) */
#define BNDDIFF(tmp, t0, t1, N, d) {                            \
    assert (d == 1 || d == 2);                                  \
    mpfr_t t;                                                   \
    mpfr_init2 (t, mpfr_get_prec (tmp));                        \
    if (d == 1) {                                               \
      mpfr_set_z (t, t1, MPFR_RNDU);                            \
      mpfr_div (t, t, N, MPFR_RNDU);                            \
      if (mpfr_cmp_d (t, X1) <= 0)                              \
        /* 2nd derivative is maximal at t1/N */                 \
        bound_d2 (tmp, t);                                      \
      else {                                                    \
        mpfr_set_z (t, t0, MPFR_RNDN);                          \
        mpfr_div (t, t, N, MPFR_RNDN);                          \
        if (mpfr_cmp_d (t, X1) >= 0)                            \
          /* 2nd derivative is maximal at t0/N */               \
          bound_d2 (tmp, t);                                    \
        else {                                                  \
          mpfr_set_d (t, X1, MPFR_RNDN);                        \
          bound_d2 (tmp, t);                                    \
        }                                                       \
      }                                                         \
    } else {                                                    \
      mpfr_t u;                                                 \
      mpfr_init2 (u, mpfr_get_prec (tmp));                      \
      mpfr_set_z (t, t0, MPFR_RNDN);                            \
      mpfr_div (t, t, N, MPFR_RNDN);                            \
      mpfr_set_z (u, t1, MPFR_RNDN);                            \
      mpfr_div (u, u, N, MPFR_RNDN);                            \
      /* if X2 < t or u < X2, the maximum is attained at t or u */ \
      if (mpfr_cmp_d (t, X2) > 0 || mpfr_cmp_d (u, X2) < 0) {   \
        bound_d3 (tmp, t);                                      \
        bound_d3 (t, u);                                        \
        if (mpfr_cmp_abs (t, tmp) > 0)                          \
          mpfr_swap (tmp, t);                                   \
      } else                                                    \
        mpfr_set_d (tmp, 0x1.5555555555556p-1, MPFR_RNDU);      \
      mpfr_clear (u);                                           \
    }                                                           \
    mpfr_abs (tmp, tmp, MPFR_RNDN);                             \
    mpfr_clear (t);                                             \
  }

/* Put in P[0]..P[d] the Taylor expansion of NN*f(tm/Nin+x) at x=0,
   with the coefficient of degree i scaled by (T/Nin)^i. */
#define TAYLOR(tmp, tm, Nin, P, NN, d) {                        \
    assert (d == 1 || d == 2);                                  \
    mpfr_t tmp2;                                                \
    mpfr_init2 (tmp2, mpfr_get_prec (tmp));                     \
    mpfr_set_z (tmp2, T, MPFR_RNDN);                            \
    mpfr_div (tmp2, tmp2, Nin, MPFR_RNDN);                      \
    /* tmp2 = T/Nin */                                          \
    mpfr_set_z (tmp, tm, MPFR_RNDN);                            \
    mpfr_div (tmp, tmp, Nin, MPFR_RNDN);			\
    mpfr_tanh (tmp, tmp, MPFR_RNDN);                            \
    /* tmp = tanh(tm/Nin) */                                    \
    mpfr_mul (P[0], tmp, NN, MPFR_RNDN);                        \
    /* the 1st derivative is 1-tanh(x)^2 */                     \
    mpfr_mul (P[1], tmp, tmp, MPFR_RNDN);                       \
    mpfr_ui_sub (P[1], 1, P[1], MPFR_RNDN);                     \
    /* multiply P[1] by tmp2=T/Nin */                           \
    mpfr_mul (P[1], P[1], tmp2, MPFR_RNDN);                     \
    mpfr_mul (P[1], P[1], NN, MPFR_RNDN);                       \
    if (d == 2) {                                               \
      /* the 2nd derivative is 2*(tanh(x)^2-1)*tanh(x) */       \
      mpfr_mul (P[2], P[1], tmp, MPFR_RNDN);                    \
      /* multiply by -2 and divide by 2! */                     \
      mpfr_neg (P[2], P[2], MPFR_RNDN);                         \
      /* multiply by tmp2=T/Nin */                              \
      mpfr_mul (P[2], P[2], tmp2, MPFR_RNDN);                   \
      /* P[1] was already scaled by NN */                       \
    }                                                           \
    mpfr_clear (tmp2);                                          \
  }

