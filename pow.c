/*
  Copyright 2007-2020 Guillaume Hanrot, Vincent LefÃ¨vre, Damien StehlÃ© 
  and Paul Zimmermann.

  This program is free software; you can redistribute it and/or modify 
  it under the terms of the GNU General Public License as published by 
  the Free Software Foundation; either version 2 of the License, or 
  (at your option) any later version.

  This program is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
  General Public License for more details.

  You should have received a copy of the GNU General Public License 
  along with this program; see the file COPYING.  If not, write to 
  the Free Software Foundation, Inc., 59 Temple Place - Suite 330, 
  Boston, MA 02111-1307, USA.
*/

#include <stdlib.h>
#include <assert.h>
#include <mpfr.h>

extern mpfr_t arg_y;

/* BNDDIFF(tmp, t0, t1, N, d) returns a bound for the (d+1)-th
   derivative of x^y for t0/N <= x < t1/N.
   The (d+1)th derivative of x^y with respect to x is:
   x^(y-(d+1))*y*(y-1)*...*(y-d), and it is maximal in absolute value
   at t1/N if y >= d+1, and at t0/N otherwise.
*/
void
bnddiff_pow (mpfr_t tmp, const mpz_t t0, const mpz_t t1, mpfr_t N, int d)
{
  mpfr_t u;
  mpfr_init2 (u, mpfr_get_prec (tmp));
  mpfr_sub_ui (u, arg_y, d + 1, MPFR_RNDA); // u = y - (d+1)
  if (mpfr_cmp_ui (u, 0) >= 0)
  {
    mpfr_set_z (tmp, t1, MPFR_RNDA);
    mpfr_div (tmp, tmp, N, MPFR_RNDA);
  }
  else
  {
    mpfr_set_z (tmp, t0, MPFR_RNDZ);
    mpfr_div (tmp, tmp, N, MPFR_RNDZ);
  }
  /* now tmp = t1/N if y >= d+1, t0/N otherwise */
  mpfr_pow (tmp, tmp, u, MPFR_RNDA);     /* x^(y-(d+1)) */
  mpfr_mul (tmp, tmp, arg_y, MPFR_RNDA); /* x^(y-(d+1))*y */
  for (int i = 1; i <= d; i++)
  {
    mpfr_sub_ui (u, arg_y, i, MPFR_RNDA);
    mpfr_mul (tmp, tmp, u, MPFR_RNDA);
  }
  mpfr_abs (tmp, tmp, MPFR_RNDA);
  mpfr_clear (u);
}

/* TAYLOR(tmp, tm, Nin, P, NN, d) puts in P[0]..P[d] the Taylor expansion of
   NN*f(tm/Nin+x) at x=0, with the coefficient of degree i scaled by (T/Nin)^i
*/
void
taylor_pow (mpfr_t tmp, mpz_t tm, mpfr_t Nin, mpfr_t *P, mpfr_t NN, int d,
            mpz_t T)
{
  mpfr_t u;
  mpfr_init2 (u, mpfr_get_prec (tmp));
  /* the degree-0 coefficient is NN*(tm/Nin)^y */
  mpfr_set_z (tmp, tm, MPFR_RNDN);
  mpfr_div (tmp, tmp, Nin, MPFR_RNDN);   /* tmp = tm/Nin */
  mpfr_pow (tmp, tmp, arg_y, MPFR_RNDN); /* (tm/Nin)^y */
  mpfr_mul (P[0], tmp, NN, MPFR_RNDN);   /* P[0] = NN*(tm/Nin)^y */
  /* the degree-i coefficient of the Taylor expansion of
     NN*(tm/Nin+x)^y at x=0 is NN*y*(y-1)*...*(y-i+1)*(tm/Nin)^(y-i)/i!, thus
     we have P[i] = NN*y*(y-1)*...*(y-i)*(tm/Nin)^(y-i+1)*(T/Nin)^i/i!
     = P[i-1]*(y-i)*T/tm/i */
  for (int i = 1; i <= d; i++)
  {
    mpfr_sub_ui (u, arg_y, i - 1, MPFR_RNDN);
    mpfr_mul (tmp, P[i-1], u, MPFR_RNDN);
    mpfr_mul_z (tmp, tmp, T, MPFR_RNDN);
    mpfr_div_z (tmp, tmp, tm, MPFR_RNDN);
    mpfr_div_ui (P[i], tmp, i, MPFR_RNDN);
  }
  mpfr_clear (u);
}
