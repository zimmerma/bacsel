/*
Copyright 2007-2020 Guillaume Hanrot, Vincent Lefèvre, Damien Stehlé 
and Paul Zimmermann.

  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful, 
  but WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public 
  License along with this program; see the file COPYING.  If not, 
  write to the Free Software Foundation, Inc., 59 Temple Place - 
  Suite 330, Boston, MA 02111-1307, USA.
*/

/* structure to store all the types used by Hensel, to avoid
   reallocating them */
typedef struct {
  mpz_t htmp1, htmp2, htmp3;
  mpz_t X1, _Y1; // avoid using Y1 since this is a Bessel function
  mpz_t D1X, D2X, D1Y, D2Y;
  mpz_t X, Y;
  mpz_t hM, hbnd1;
  int ncoefs;
  mpz_t *P1X, *P2X, *P1Y, *P2Y;
  long  *sP1X, *sP2X, *sP1Y, *sP2Y, *p1, *p2;
} hensel_struct_t;
typedef hensel_struct_t hensel_t[1];

void hensel_init (hensel_t, int, int);
void hensel_clear (hensel_t);
int hensel (int *, mpz_t *, mpz_t *, mpz_t, int, int, mpz_t **, hensel_t);
