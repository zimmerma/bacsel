/*
  Copyright 2024-2025 Paul Zimmermann.

  This program is free software; you can redistribute it and/or modify 
  it under the terms of the GNU General Public License as published by 
  the Free Software Foundation; either version 2 of the License, or 
  (at your option) any later version.

  This program is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
  General Public License for more details.

  You should have received a copy of the GNU General Public License 
  along with this program; see the file COPYING.  If not, write to 
  the Free Software Foundation, Inc., 59 Temple Place - Suite 330, 
  Boston, MA 02111-1307, USA.
*/

#include <assert.h>
#ifdef USE_PARI
#include <pari/pari.h>
#endif

#define NAME "lgamma"

#define EVAL(y, rnd_mode) do { int s; mpfr_lgamma (y, &s, y, rnd_mode); } while (0)

#ifdef NEED_TABLES
#ifdef USE_PARI
// put in y the value of psi(2,x) using Pari/GP
static void
psi2 (mpfr_t y, mpfr_t x)
{
  char *s, *t;
  mpfr_exp_t e;
  int n, bias;
  pari_sp av = avma;
  unsigned long prec = mpfr_get_prec (y);

  s = mpfr_get_str (NULL, &e, 10, 0, x, MPFR_RNDN);
  n = strlen (s);
  bias = n - (s[0] == '-');
  t = (char*) malloc (n + 10);
  strcpy (t, s);
  n += sprintf (t + strlen(s), "*10.^%ld", e - bias);
  t[n] = '\0';
  mpfr_free_str (s);
  GEN g = gp_read_str (t);
  // for tiny x, Pari yields "domain error in zetahurwitz: x <= 0"
  prec = (mpfr_get_exp (x) >= 0) ? prec : -mpfr_get_exp (x) + prec;
  g = gpsi_der (g, 2, prec);
  s = pari_sprintf ("%Ps", g);
  mpfr_set_str (y, s, 10, MPFR_RNDN);
  free (s);
  free (t);
  set_avma (av); // free all memory created by Pari in this function
}

// put in y the value of |lgamma'''(x)| using Pari/GP
// lgamma'''(x) = psi(2, x)
static void
bound_lgamma3_pari (mpfr_t y, mpfr_t x)
{
  psi2 (y, x);                       // p2 = psi(2,x)
  mpfr_abs (y, y, MPFR_RNDN);
}
#endif
#endif // NEED_TABLES

/* BNDDIFF(tmp, t0, t1, N, d) returns a bound for the absolute value of the
   (d+1)-th derivative of lgamma(x) for x0 := t0/N <= x < x1 := t1/N.
   If x0, x1 < 0, assume they are in the same interval (k,k+1) with k integer.
   For d=1, the 2nd derivative of log(gamma(x)) is psi(1,x):
   * for x > 0, psi(1,x) is decreasing on (0,+Inf) from +Inf to 0,
      thus the maximum is attained at x0;
   * for k-1 < x < k with k<=0 integer, psi(1,x) is positive, decreasing
     over (k-1,x_k), then increasing on (x_k,k),
     the maximum in absolute value is thus attained at x0 or x1.
   For d=2, the 3rd derivative of log(gamma(x)) is psi(2,x):
   * for x > 0, psi(2,x) is increasing on (0,+Inf) from -Inf to 0,
     thus the maximum in absolute value is attained at x0;
   * for k-1 < x < k with k<=0 integer, psi(1,x) is increasing,
     negative on (k-1,x_k), positive on (x_k,k),
     the maximum in absolute value is thus attained at x0 or x1.
   In all cases, for negative x, the maximum in absolute value is attained
   at x0 or x1.
*/
#define BNDDIFF(tmp, t0, t1, N, d)                              \
  do {                                                          \
    assert (d <= 2);                                            \
    mpfr_set_z (tmp, t0, MPFR_RNDU);                            \
    mpfr_div (tmp, tmp, N, MPFR_RNDU);                          \
    assert (mpfr_sgn (tmp) > 0 || !mpfr_integer_p (tmp));       \
    if (mpfr_sgn (tmp) > 0) {                                   \
      /* maximum is attained at t0/N */                         \
      if (d == 1)                                               \
        mpfr_trigamma (tmp, tmp, MPFR_RNDA);                    \
      else                                                      \
        bound_lgamma3_pari (tmp, tmp);                          \
    } else { /* x < 0: maximum at x0 or x1 */                   \
      mpfr_t tmp2;                                              \
      mpfr_init2 (tmp2, mpfr_get_prec (tmp));                   \
      mpfr_set_z (tmp2, t1, MPFR_RNDU);                         \
      mpfr_div (tmp2, tmp2, N, MPFR_RNDU);                      \
      assert (mpfr_sgn (tmp2) > 0 || !mpfr_integer_p (tmp2));   \
      if (d == 1) {                                             \
        mpfr_trigamma (tmp, tmp, MPFR_RNDA);                    \
        mpfr_trigamma (tmp2, tmp2, MPFR_RNDA);                  \
      } else {                                                  \
        bound_lgamma3_pari (tmp, tmp);                          \
        bound_lgamma3_pari (tmp2, tmp2);                        \
      }                                                         \
      if (mpfr_cmp_abs (tmp2, tmp) > 0)                         \
        mpfr_swap (tmp2, tmp);                                  \
      mpfr_clear (tmp2);                                        \
    }                                                           \
    assert (mpfr_sgn (tmp) > 0);                                \
  } while (0)

/* The derivative of lgamma(x) is psi(x),
   and the 2nd derivative is psi(1,x) = trigamma(x). */
#define TAYLOR(tmp, tm, Nin, P, NN, d)          \
  {                                             \
    int s;                                      \
    assert (d <= 2);                            \
    mpfr_set_z (tmp, tm, MPFR_RNDN);            \
    mpfr_div (tmp, tmp, Nin, MPFR_RNDN);        \
    assert (mpfr_sgn (tmp) > 0 || !mpfr_integer_p (tmp));       \
    /* x = tm/Nin */                            \
    mpfr_lgamma (P[0], &s, tmp, MPFR_RNDN);     \
    mpfr_mul (P[0], P[0], NN, MPFR_RNDN);       \
    /* P[0] = NN*lgamma(x) */                   \
    mpfr_digamma (P[1], tmp, MPFR_RNDN);        \
    mpfr_mul (P[1], P[1], NN, MPFR_RNDN);       \
    mpfr_mul_z (P[1], P[1], T, MPFR_RNDN);      \
    mpfr_div (P[1], P[1], Nin, MPFR_RNDN);      \
    /* P[1] = NN*lgamma'(x)*(T/Nin) */          \
    if (d >= 2) {                               \
      mpfr_trigamma (P[2], tmp, MPFR_RNDN);     \
      mpfr_mul (P[2], P[2], NN, MPFR_RNDN);     \
      mpfr_mul_z (P[2], P[2], T, MPFR_RNDN);    \
      mpfr_div (P[2], P[2], Nin, MPFR_RNDN);    \
      mpfr_mul_z (P[2], P[2], T, MPFR_RNDN);    \
      mpfr_div (P[2], P[2], Nin, MPFR_RNDN);    \
      /* divide by 2! */                        \
      mpfr_div_2ui (P[2], P[2], 1, MPFR_RNDN);  \
    }                                           \
  }

