/* Copyright 2007-2023 Guillaume Hanrot, Vincent Lefèvre, Damien Stehlé 
   and Paul Zimmermann.

  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful, 
  but WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public 
  License along with this program; see the file COPYING.  If not, 
  write to the Free Software Foundation, Inc., 59 Temple Place - 
  Suite 330, Boston, MA 02111-1307, USA.
*/

#include <assert.h>

#define NAME "tanpi"
#define EVAL(y, rnd_mode) mpfr_tanpi (y, y, rnd_mode);

/* tmp <- upper approximation of 2*pi^2*(1+tanpi(x)^2)*tanpi(x),
   where one can modify x */
static inline void
bound_d2 (mpfr_t tmp, mpfr_t x)
{
  mpfr_tanpi (x, x, MPFR_RNDU);            /* tanpi(x) */
  mpfr_mul (tmp, x, x, MPFR_RNDU);         /* tanpi(x)^2 */
  mpfr_add_ui (tmp, tmp, 1, MPFR_RNDU);    /* 1+tanpi(x)^2 */
  mpfr_mul (tmp, tmp, x, MPFR_RNDU);       /* (1+tanpi(x)^2)*tanpi(x) */
  /* 2*pi^2 < 0x1.3bd3cc9be45dfp+4 */
  mpfr_mul_d (tmp, tmp, 0x1.3bd3cc9be45dfp+4, MPFR_RNDU);
}

/* tmp <- upper approximation of 2*pi^3*(3*tanpi(x)^2+1)*(1+tanpi(x)^2),
   where one can modify x */
static inline void
bound_d3 (mpfr_t tmp, mpfr_t x)
{
  mpfr_tanpi (x, x, MPFR_RNDU);           /* tanpi(x) */
  mpfr_mul (x, x, x, MPFR_RNDU);          /* tanpi(x)^2 */
  mpfr_add_ui (tmp, x, 1, MPFR_RNDU);     /* 1+tanpi(x)^2 */
  mpfr_mul_ui (x, x, 3, MPFR_RNDU);       /* 3*tanpi(x)^2 */
  mpfr_add_ui (x, x, 1, MPFR_RNDU);       /* 3*tanpi(x)^2+1 */
  mpfr_mul (tmp, tmp, x, MPFR_RNDU);      /* (3*tanpi(x)^2+1)*(1+tanpi(x)^2) */
  /* 2*pi^3 < 0x1.f019b59389d7dp+5 */
  mpfr_mul_d (tmp, tmp, 0x1.f019b59389d7dp+5, MPFR_RNDU);
}

/* BNDDIFF(tmp, t0, t1, N, d) returns a bound for the (d+1)-th
   derivative of tanpi(x) for t0/N <= x < t1/N. We only implement
   it for d=1 and d=2 so far, and assume 0 < t0/N < t1/N < 0.5.
   For d=1 the 2nd derivative of tanpi(x) is 2*pi^2*(1+tanpi(x)^2)*tanpi(x),
   and is maximal in absolute value at t1/N. For d=2 the 3rd derivative of
   tanpi(x) is 2*pi^3*(3*tanpi(x)^2+1)*(1+tanpi(x)^2), and is maximal in
   absolute value at t1/N too. */
#define X1 0x1.5124271980434p-1 /* atanpi(1/sqrt(3)) */
#define X2 0x1.256e66a48a3b6p+0 /* atanpi(sqrt(6)/3) */
#define BNDDIFF(tmp, t0, t1, N, d) {                            \
    assert (d == 1 || d == 2);                                  \
    mpfr_t t;                                                   \
    mpfr_init2 (t, mpfr_get_prec (tmp));                        \
    mpfr_set_z (t, t1, MPFR_RNDU);                              \
    assert (mpfr_cmp_ui_2exp (t, 1, -1) < 0);                   \
    mpfr_div (t, t, N, MPFR_RNDU);                              \
    if (d == 1)                                                 \
      bound_d2 (tmp, t);                                        \
    else                                                        \
      bound_d3 (tmp, t);                                        \
    mpfr_abs (tmp, tmp, MPFR_RNDN);                             \
    mpfr_clear (t);                                             \
  }

/* Put in P[0]..P[d] the Taylor expansion of NN*f(tm/Nin+x) at x=0,
   with the coefficient of degree i scaled by (T/Nin)^i. */
#define TAYLOR(tmp, tm, Nin, P, NN, d) {                        \
    assert (d == 1 || d == 2);                                  \
    mpfr_t tmp2;                                                \
    mpfr_init2 (tmp2, mpfr_get_prec (tmp));                     \
    mpfr_set_z (tmp2, T, MPFR_RNDN);                            \
    mpfr_div (tmp2, tmp2, Nin, MPFR_RNDN);                      \
    mpfr_const_pi (P[0], MPFR_RNDN);                            \
    mpfr_mul (tmp2, tmp2, P[0], MPFR_RNDN);                     \
    /* tmp2 = pi*T/Nin */                                       \
    mpfr_set_z (tmp, tm, MPFR_RNDN);                            \
    mpfr_div (tmp, tmp, Nin, MPFR_RNDN);			\
    mpfr_tanpi (tmp, tmp, MPFR_RNDN);                           \
    /* tmp = tanpi(tm/Nin) */                                   \
    mpfr_mul (P[0], tmp, NN, MPFR_RNDN);                        \
    /* the 1st derivative is pi*(1+tanpi(x)^2) */               \
    mpfr_mul (P[1], tmp, tmp, MPFR_RNDN);                       \
    mpfr_add_ui (P[1], P[1], 1, MPFR_RNDN);                     \
    /* multiply P[1] by tmp2=pi*T/Nin */                        \
    mpfr_mul (P[1], P[1], tmp2, MPFR_RNDN);                     \
    /* multiply by NN */                                        \
    mpfr_mul (P[1], P[1], NN, MPFR_RNDN);                       \
    if (d == 2) {                                               \
      /* the 2nd derivative is 2*pi^2*(tanpi(x)^2+1)*tanpi(x) */\
      mpfr_mul (P[2], P[1], tmp, MPFR_RNDN);                    \
      /* multiply by 2 and divide by 2!: do nothing */          \
      /* multiply by tmp2=pi*T/Nin */                           \
      mpfr_mul (P[2], P[2], tmp2, MPFR_RNDN);                   \
      /* P[1] was already scaled by NN */                       \
    }                                                           \
    mpfr_clear (tmp2);                                          \
  }

