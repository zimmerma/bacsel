/*
  Copyright 2021-2023 Paul Zimmermann and Nathalie Revol.
  (inspired from atanh.h)

  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful, 
  but WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public 
  License along with this program; see the file COPYING.  If not, 
  write to the Free Software Foundation, Inc., 59 Temple Place - 
  Suite 330, Boston, MA 02111-1307, USA.
*/

#include <assert.h>

#define NAME "atanpi"

/* function to evaluate atanpi */
#define EVAL(y, rnd_mode) mpfr_atanpi (y, y, rnd_mode);

/* tmp <- 2x/(1+x^2)^2, where one can modify x */
static inline void
bound_d2 (mpfr_t tmp, mpfr_t x)
{
  mpfr_sqr (tmp, x, MPFR_RNDD);
  mpfr_add_ui (tmp, tmp, 1, MPFR_RNDD);
  mpfr_sqr (tmp, tmp, MPFR_RNDD);
  mpfr_div (tmp, x, tmp, MPFR_RNDU);
  mpfr_mul_2ui (tmp, tmp, 1, MPFR_RNDU);
}

/* tmp <- 2(3x^2-1)/(1+x^2)^3, where one can modify x */
static inline void
bound_d3 (mpfr_t tmp, mpfr_t x)
{
  mpfr_sqr (x, x, MPFR_RNDD);           // x^2
  mpfr_add_ui (tmp, x, 1, MPFR_RNDD);   // 1+x^2
  mpfr_pow_ui (tmp, tmp, 3, MPFR_RNDD); // (1+x^2)^3
  mpfr_mul_ui (x, x, 3, MPFR_RNDU);     // 3x^2
  mpfr_sub_ui (x, x, 1, MPFR_RNDU);   	// 3x^2-1
  mpfr_div (tmp, x, tmp, MPFR_RNDU);    // (3x^2-1)/(1+x^2)^3
  mpfr_mul_2ui (tmp, tmp, 1, MPFR_RNDU); // 2(3x^2-1)/(1+x^2)^3
}

/* BNDDIFF(tmp,t0,t1,n,d) puts in tmp an upper bound for the
   (d+1)-th derivative over [t0/N,t1/N].
   d=1: the 2nd derivative of atanpi(x) is -2x/(1+x^2)^2/pi
        and is bounded by 3/8*sqrt(3)/pi for t0/N < sqrt(3)/3,
        and by the value of 2x/(1+x^2)^2/pi at x=t0/N otherwise.
   d=2: the 3rd derivative of atanpi(x) is 2 (3x^2-1)/(1+x^2)^3/pi
        and is bounded by 2/pi in absolute value for t0/N < 1,
        and by the value of 2(3x^2-1)/(1+x^2)^3/pi at x=t0/N otherwise. */
#define BNDDIFF(tmp, t0, t1, N, d)                                \
  assert (mpz_sgn (t0) >= 0);                                     \
  assert (d <= 2);                                                \
  mpfr_t ump;                                                     \
  mpfr_init2 (ump, mpfr_get_prec (tmp));                          \
  mpfr_set_z (ump, t0, MPFR_RNDD);                                \
  mpfr_div (ump, ump, N, MPFR_RNDD);                              \
  if (d == 1) {                                                   \
    /* sqrt(3)/3 < 0x1.279a74590331dp-1 */                        \
    if (mpfr_cmp_d (ump, 0x1.279a74590331dp-1) < 0)               \
      /* 3/8*sqrt(3) < 0x1.4c8dc2e42398p-1 */                     \
      mpfr_set_d (tmp, 0x1.4c8dc2e42398p-1, MPFR_RNDU);           \
    else                                                          \
      bound_d2 (tmp, ump);                                        \
  } else if (d == 2) {                                            \
    if (mpfr_cmp_d (ump, 1) < 0)                                  \
      mpfr_set_ui (tmp, 2, MPFR_RNDU);                            \
    else                                                          \
      bound_d3 (tmp, ump);                                        \
  }                                                               \
  mpfr_abs (tmp, tmp, MPFR_RNDU);                                 \
  /* divide by pi */                                              \
  mpfr_const_pi (ump, MPFR_RNDD);                                 \
  mpfr_div (tmp, tmp, ump, MPFR_RNDU);                            \
  mpfr_clear (ump);

/* Put in P[0]..P[d] the Taylor expansion of NN*f(tm/Nin+x) at x=0,
   with the coefficient of degree i scaled by (T/Nin)^i.
   The n-th derivative of atanpi(x) is Q[n]/(1+x^2)^n/pi for n>=1,
   where the polynomials Q[n] satisfy the recurrence:
   Q[1] = 1, Q[n] = (1+x^2) Q[n-1]' - 2x(n-1) Q[n-1]
   This gives: Q[2] = -2x, Q[3] = 6x^2-2, Q[4] = -24x^3+24x,
   Q[5] = 120x^4-240x^2+24, ...
   By induction we can prove:
   Q[n] is of degree n-1, Q[n] has only odd-degree coefficients for n even,
   even-degree coefficients for n odd. */
#define TAYLOR(tmp, tm, Nin, P, NN, d)                          \
  { long n,i;                                                   \
    mpfr_set_z (tmp, tm, MPFR_RNDN);                            \
    mpfr_div (tmp, tmp, Nin, MPFR_RNDN);                        \
    mpfr_atanpi (P[0], tmp, MPFR_RNDN);                         \
    mpfr_mul (P[0], P[0], NN, MPFR_RNDN);                       \
    /* P[0] = NN*atanpi(tm/Nin) */                              \
    mpz_t *q, z;                                                \
    mpfr_t u, v;                                                \
    mpfr_init2 (u, mpfr_get_prec (tmp));                        \
    mpfr_init2 (v, mpfr_get_prec (tmp));                        \
    mpz_init (z);                                               \
    /* u <- 1/(1+x^2) for x=tm/Nin */                           \
    mpfr_sqr (u, tmp, MPFR_RNDN);                               \
    mpfr_add_ui (u, u, 1, MPFR_RNDN);                           \
    mpfr_ui_div (u, 1, u, MPFR_RNDN);                           \
    /* u <- (T/Nin)*u = (T/Nin)/(1+(tm/Nin)^2) */               \
    mpfr_mul_z (u, u, T, MPFR_RNDN);                            \
    mpfr_div (u, u, Nin, MPFR_RNDN);                            \
    /* v <- NN*u/pi */                                          \
    mpfr_mul (v, u, NN, MPFR_RNDN);                             \
    mpfr_const_pi (P[1], MPFR_RNDN);                            \
    mpfr_div (v, v, P[1], MPFR_RNDN);                           \
    q = (mpz_t*) malloc (d * sizeof (mpz_t));                   \
    for (i=0; i<d; i++) mpz_init (q[i]);                        \
    for (n=1; n<=d; n++) {                                      \
      /* compute coefficients Q[n] in q[0]..q[n-1] */           \
      if (n==1)                                                 \
        mpz_set_si (q[0], 1);                                   \
      else {                                                    \
        /* let p[i] be the coefficients of Q[n-1] */            \
        /* and q[i] the (new) coefficients of Q[n] */           \
        /* Q'[n-1] gives (i+1)*p[i+1] */                        \
        /* x^2 Q'[n-1] gives (i-1)*p[i-1] */                    \
        /* -2x(n-1)Q[n-1] gives -2(n-1)p[i-1] */                \
        /* we thus get: */                                      \
        /* q[i] <- (i+1)*p[i+1] - (2n-i-1) p[i-1] */            \
        /* since we read and write indices of different */      \
        /* parities, there is no overlap issue, and we */       \
        /* keep the coefficients of the other parity, */        \
        /* simply we don't have to consider them */             \
        for (i = n-1; i >= 0; i-=2) {                           \
          if (i < n-1)                                          \
            mpz_mul_ui (q[i], q[i+1], i+1);                     \
          else                                                  \
            mpz_set_ui (q[i], 0);                               \
          if (i > 0) {                                          \
            mpz_mul_ui (z, q[i-1], 2*n-i-1);                    \
            mpz_sub (q[i], q[i], z);                            \
          }                                                     \
        }                                                       \
      } /* end of else branch */                                \
      /* P[n] <- Q[n](tm/Nin) using Horner's scheme */          \
      mpfr_set_z (P[n], q[n-1], MPFR_RNDN);                     \
      for (i = n-3; i >= 0; i-=2) {                             \
        mpfr_mul (P[n], P[n], tmp, MPFR_RNDN);                  \
        mpfr_mul (P[n], P[n], tmp, MPFR_RNDN);                  \
        mpfr_add_z (P[n], P[n], q[i], MPFR_RNDN);               \
      } /* end of for loop */                                   \
      /* multiply by tm/Nin if n is even */                     \
      if ((n%2)==0)                                             \
        mpfr_mul (P[n], P[n], tmp, MPFR_RNDN);                  \
      /* divide by (1+x^2)^n and multiply by (T/Nin)^n */       \
      /* divide the n-th derivative by n! for Taylor */         \
      mpfr_div_ui (v, v, n, MPFR_RNDN);                         \
      /* invariant: v = (T/Nin)^n/((1+x^2)^n)/n!/pi */          \
      mpfr_mul (P[n], P[n], v, MPFR_RNDN);                      \
      /* u = (T/Nin)/(1+x^2) */                                 \
      mpfr_mul (v, v, u, MPFR_RNDN);                            \
    }                                                           \
    for (i=0; i<d; i++) mpz_clear (q[i]);                       \
    free (q);                                                   \
    mpfr_clear (u); mpfr_clear (v);                             \
    mpz_clear (z);                                              \
  }
