/*
Copyright 2007-2023 Guillaume Hanrot, Vincent Lefèvre, Damien Stehlé 
and Paul Zimmermann.

  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful, 
  but WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public 
  License along with this program; see the file COPYING.  If not, 
  write to the Free Software Foundation, Inc., 59 Temple Place - 
  Suite 330, Boston, MA 02111-1307, USA.
*/

#define NAME "sqrt"

#define EVAL(y, rnd_mode) mpfr_sqrt(y, y, rnd_mode); 
/* BNDDIFF(tmp,t0,t1,N,d) puts in tmp an upper bound for the (d+1)-th
   derivative of sqrt(x) over [t0/N,t1/N] (d >= 1).
   The d-th derivative of sqrt(x) is (up to sign)
   1*3*...*(2d-3)/2^d/x^((2d-1)/2), thus the (d+1)-th derivative is
   1*3*...*(2d-1)/2^(d+1)/x^((2d+1)/2). */

#define BNDDIFF(tmp, t0, t1, N, d)                      \
  mpfr_div_z (tmp, Nin, t0, MPFR_RNDU);			\
  mpfr_pow_ui (tmp, tmp, 2*d+1, MPFR_RNDU);             \
  mpfr_sqrt (tmp, tmp, MPFR_RNDU);                      \
  for (i=1; i<=d-1; i++)				\
    mpfr_mul_ui (tmp, tmp, 2*i+1, MPFR_RNDU);		\
  mpfr_div_2exp (tmp, tmp, d+1, MPFR_RNDU);

#define TAYLOR(tmp, tm, Nin, P, NN, d)			\
  { long i;						\
    mpfr_set_z (tmp, tm, MPFR_RNDN);			\
    mpfr_div (tmp, tmp, Nin, MPFR_RNDN);                \
    mpfr_sqrt (P[0], tmp, MPFR_RNDN);			\
    mpfr_ui_div (tmp, 1, tmp, MPFR_RNDN);		\
    mpfr_mul (P[0], P[0], NN, MPFR_RNDN);		\
    for (i=1; i<=d; i++) {				\
      /* Note: for i=1, the multiplication by -1 is */  \
      /* compensated by that by 2*i-3 below. */         \
      mpfr_neg (P[i], P[i-1], MPFR_RNDN);               \
      mpfr_mul_z (P[i], P[i], T, MPFR_RNDN);		\
      mpfr_div (P[i], P[i], Nin, MPFR_RNDN);		\
      mpfr_mul (P[i], P[i], tmp, MPFR_RNDN);		\
      mpfr_div_ui (P[i], P[i], i, MPFR_RNDN);		\
      mpfr_div_ui (P[i], P[i], 2, MPFR_RNDN);		\
      mpfr_mul_si (P[i], P[i], 2*i-3, MPFR_RNDN);	\
    }                                                   \
  }
 
