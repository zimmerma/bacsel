/* FPLLL wrapper.

  Copyright 2020-2021 Paul Zimmermann and Guillaume Hanrot.

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; see the file COPYING.  If not, write to the Free
  Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
  02111-1307, USA.

*/

#include <fplll.h>
#include <assert.h>
#include "delta_eta.h" /* for DELTA and ETA */
#include "io.h"        /* for MAYBE_UNUSED */
#include "mat.h"

using namespace fplll;

/* Random projection by a matrix R with entries uniform in [-bnd, bnd].
   M has dimensions nrows x ncols
   M_proj has dimensions nrows x nrows */
static void MAYBE_UNUSED
Uniform_projection (ZZ_mat<mpz_t> *M_proj, mpz_t **M, int nrows, int ncols,
		    int bnd)
{
  int i, j, k;
  mpz_t tmp;
  long B, proj_coeff;

  B = 2*bnd + 1;

  mpz_init (tmp);
  for (j = 0; j < ncols; j++)
    {
      for (k = 0; k < nrows; k++)
	{
	  // Proj_coeff = p_{jk}
	  proj_coeff = (random() % B) - bnd;
	  for (i = 0; i < nrows; i++)
	    {
	      if (j == 0)
		mpz_mul_si ((*M_proj)(i,k).get_data(), M[i][j], proj_coeff);
	      else
		{
		  mpz_mul_si (tmp, M[i][j], proj_coeff);
		  mpz_add ((*M_proj)(i,k).get_data(), (*M_proj)(i,k).get_data(),
			   tmp);
		}
	    }
	}
    }
  mpz_clear (tmp);
}

/* A <- U*A, where A has dimension d x n, and U dimension d x d */
static void MAYBE_UNUSED
mul_mat_inplace (mpz_t **A, ZZ_mat<mpz_t> U, int d, int n)
{
  mpz_t *a;
  a = init_vec (d);
  for (int j = 0; j < n; j++)
    {
      /* A[i,j] = sum(U[i,k]*A[k,j] for k=0..d-1) */
      for (int i = 0; i < d; i++)
	{
	  /* a[i] stores A[i,j] */
	  mpz_mul (a[i], U(i,0).get_data(), A[0][j]);
	  for (int k = 1; k < d; k++)
	    mpz_addmul (a[i], U(i,k).get_data(), A[k][j]);
	}
      /* now we will never read A[.,j] again, thus we copy a[i] to A[i,j] */
      for (int i = 0; i < d; i++)
	mpz_swap (A[i][j], a[i]);
    }
  clear_vec (a, d);
}

static void MAYBE_UNUSED
print_ZZmat (ZZ_mat<mpz_t> B, int d, int n)
{
  int i, j;

  printf("[");
  for (i=0;i<d;i++)
    {
      printf("[");
      for (j=0;j<n;j++)
	{
	  mpz_out_str (stdout, 10, B(i,j).get_data());
	  if (j < n) printf(" ");
	}
      printf("]\n");
    }
  printf("]\n");
}

/* inspired from https://github.com/fplll/fplll/blob/master/fplll/main.cpp,
   see also https://fplll.github.io/fplll/annotated.html */
/* B has d rows and n columns */
void
LLLReduce (mpz_t **B, int d, int n, int precision MAYBE_UNUSED)
{
  /* See page 195 of Serge's Torres PhD thesis (he is using LM_WRAPPER
     and FT_DEFAULT):
   * LM_WRAPPER: Tries to reduce the matrix with a combination of the
     following methods with increasing precision. Then, runs the proved
     version with the default precision. The floatType must be FT_DEFAULT
     and precision must be 0. The result is guaranteed.
   * LM_PROVED: Proved method. Uses integers to compute dot products. The
     result is guaranteed if floatType=FT_DEFAULT/FT_MPFR and precision=0
     (see above).
   * LM_HEURISTIC: Heuristic method. Uses floating-point numbers to compute
     dot products. The result is not guaranteed. It is more efficient that
     the proved version when the coefficients of the input are large (the
     threshold depends on the floating-point type and precision).
   * LM_FAST: Same as LM_HEURISTIC with floatType=FT_DOUBLE, with a
     special trick to handle larger inputs. */
  LLLMethod method = LM_WRAPPER;
  /* FT_DEFAULT: LM_WRAPPER: yes
                 LM_PROVED: yes, same as FT_MPFR
		 LM_HEURISTIC: yes, same as FT_DPE
		 LM_FAST: yes, same as FT_DOUBLE
     FT_DOUBLE:  LM_WRAPPER: no
                 LM_PROVED: yes
		 LM_HEURISTIC: yes
		 LM_FAST: yes
     FT_DPE:     LM_WRAPPER: no
                 LM_PROVED: yes
		 LM_HEURISTIC: yes
		 LM_FAST: no
     FT_MPFR:    LM_WRAPPER: no
                 LM_PROVED: yes
		 LM_HEURISTIC: yes
		 LM_FAST: no */
  FloatType float_type = FT_DEFAULT; /* FT_MPFR, FT_DPE, FT_DD, FT_QD, FT_DOUBLE, FT_LONG_DOUBLE */
  int status MAYBE_UNUSED;
  int flags = 0;
#ifndef AS_TRICK
  ZZ_mat<mpz_t> A(d,n);
  /* copy B into A, thanks to Marc Glisse for how to access the mpz_t
     elements of A */
  for (int i = 0; i < d; i++)
    for (int j = 0; j < n; j++)
      mpz_swap (A(i,j).get_data(), B[i][j]);
#else
  ZZ_mat<mpz_t> A(d,d);
  Uniform_projection (&A, B, d, n, 10);
#endif
#ifdef TIMER
  int st = fplll::cputime ();
#endif
  /* reduce A */
#ifndef AS_TRICK
  if (float_type == FT_MPFR)
    status = lll_reduction (A, DELTA, ETA, method, float_type, precision, flags);
  else
    status = lll_reduction (A, DELTA, ETA, method, float_type, flags);
#else
  ZZ_mat<mpz_t> V(d,d);
  if (float_type == FT_MPFR)
    status = lll_reduction (A, V, DELTA, ETA, method, float_type, precision, flags);
  else
    status = lll_reduction (A, V, DELTA, ETA, method, float_type, flags);
#endif
#ifdef TIMER
  printf ("lll_reduction(%d,%d) took %dms\n", d, A.get_cols(),
	  fplll::cputime () - st);
#endif
  assert (status == RED_SUCCESS);
#ifndef AS_TRICK
  /* copy back A into B */
  for (int i = 0; i < d; i++)
    for (int j = 0; j < n; j++)
      mpz_swap (B[i][j], A(i,j).get_data());
#else
  /* B <- V*B */
  mul_mat_inplace (B, V, d, n);
#ifdef REDUCE2
  /* reduce again */
  ZZ_mat<mpz_t> A2(d,n);
  for (int i = 0; i < d; i++)
    for (int j = 0; j < n; j++)
      mpz_swap (A2(i,j).get_data(), B[i][j]);
#ifdef TIMER
  st = fplll::cputime ();
#endif
  if (float_type == FT_MPFR)
    status = lll_reduction (A2, DELTA, ETA, method, float_type, precision, flags);
  else
    status = lll_reduction (A2, DELTA, ETA, method, float_type, flags);
#ifdef TIMER
  printf ("   lll_reduction(%d,%d) took %dms\n", d, n, fplll::cputime () - st);
#endif
  assert (status == RED_SUCCESS);
  /* copy back A2 into B */
  for (int i = 0; i < d; i++)
    for (int j = 0; j < n; j++)
      mpz_swap (B[i][j], A2(i,j).get_data());
#endif
#endif
}
