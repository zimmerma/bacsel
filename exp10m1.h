/*
Copyright 2007-2023 Guillaume Hanrot, Vincent Lefèvre, Damien Stehlé 
and Paul Zimmermann.

  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful, 
  but WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public 
  License along with this program; see the file COPYING.  If not, 
  write to the Free Software Foundation, Inc., 59 Temple Place - 
  Suite 330, Boston, MA 02111-1307, USA.
*/

#define NAME "exp10m1"
#define EVAL(y, rnd_mode) mpfr_exp10m1 (y, y, rnd_mode);

/* BNDDIFF(tmp,t0,t1,N,d) puts in tmp an upper bound for the (d+1)-th
   derivative of 10^x-1 over [t0/N,t1/N] (d >= 1).
   The d-th derivative of 10^x-1 is 10^x*log(10)^d,
   thus we bound by 10^x*log(10)^(d+1). */
#define BNDDIFF(tmp, t0, t1, N, d)                              \
  assert (mpz_cmp_ui (t0, 0) > 0);                              \
  mpfr_set_z (tmp, t1, MPFR_RNDU);				\
  mpfr_div (tmp, tmp, Nin, MPFR_RNDU);				\
  mpfr_exp10 (tmp, tmp, MPFR_RNDU);				\
  /* tmp = 10^x where x=t1/Nin */                               \
  { mpfr_t ump;                                                 \
    mpfr_init2 (ump, mpfr_get_prec (tmp));                      \
    mpfr_log_ui (ump, 10, MPFR_RNDU);                           \
    mpfr_pow_ui (ump, ump, d+1, MPFR_RNDU);                     \
    /* ump = 10^(d+1) */                                        \
    mpfr_mul (tmp, tmp, ump, MPFR_RNDU);                        \
    /* tmp = 10^x*log(10)^(d+1) */                              \
    mpfr_clear (ump); }

/* TAYLOR(tmp, tm, Nin, P, NN, d) puts in P[0]..P[d] the Taylor expansion of
   NN*exp10m1(tm/Nin+x) at x=0, with the coefficient of degree i scaled by
   (T/Nin)^i. */
#define TAYLOR(tmp, tm, Nin, P, NN, d)                  \
  { long i;                                             \
    mpfr_set_z (tmp, tm, MPFR_RNDN);                    \
    mpfr_div (tmp, tmp, Nin, MPFR_RNDN);                \
    /* tmp = x = tm/Nin */                              \
    mpfr_exp10m1 (P[0], tmp, MPFR_RNDN);                \
    /* P[0] = exp10m1(x) */                             \
    mpfr_mul (P[0], P[0], NN, MPFR_RNDN);               \
    /* P[0] = NN*exp10m1(tm/Nin) */                     \
    mpfr_exp10 (P[1], tmp, MPFR_RNDN);                  \
    /* P[1] = 10^x */                                   \
    mpfr_mul (P[1], P[1], NN, MPFR_RNDN);               \
    /* P[1] = NN*10^x */                                \
    mpfr_log_ui (tmp, 10, MPFR_RNDN);                   \
    for (i=1; i<=d; i++) {                              \
      /* multiply the previous coefficient by T/Nin */  \
      if (i==1)                                         \
        mpfr_mul_z (P[1], P[1], T, MPFR_RNDN);          \
      else                                              \
        mpfr_mul_z (P[i], P[i-1], T, MPFR_RNDN);	\
      mpfr_div (P[i], P[i], Nin, MPFR_RNDN);            \
      /* divide by i!/(i-1)! = i */                     \
      mpfr_div_ui (P[i], P[i], i, MPFR_RNDN);           \
      /* multiply by log(10) */                         \
      mpfr_mul (P[i], P[i], tmp, MPFR_RNDN);            \
    }                                                   \
  }

