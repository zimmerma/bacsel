/*
Copyright 2006-2007 Guillaume Hanrot, Vincent Lefèvre, Damien Stehlé 
and Paul Zimmermann.

  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful, 
  but WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public 
  License along with this program; see the file COPYING.  If not, 
  write to the Free Software Foundation, Inc., 59 Temple Place - 
  Suite 330, Boston, MA 02111-1307, USA.
*/

void 
diff1(long *, long *, long, int, int); 
void diff2(long *, long *, long, int, int); 
void deriv_pol1(mpz_t *, mpz_t *, int, int);
void deriv_pol2(mpz_t *, mpz_t *, int, int);
long eval_mod(long *, long, long, long, int, int);
void eval_pol(mpz_t, mpz_t *, mpz_t, mpz_t, mpz_t, int, int);
void divide_by_content (mpz_t *, int, int);
