#!/bin/bash
# worst cases of exp in binary128
# Table 3.14 page 237 of Serge Torres' PhD thesis
# m=6p d=15 alpha=5 2^75
make clean
make GMP=$GMP MPFR=$MPFR FPLLL=$FPLLL DEFSAL="-DEXP -DBASIS=2 $EXTRA"
t=75
n=100
t0=7343016637207169433382599627112448
t1=`echo "$t0 + $n*2*2^$t" | bc -q`
./bacsel -v -rnd_mode all -prec 1243 -n 113 -nn 113 -m 678 -t $t -t0 $t0 -t1 $t1 -d 15 -alpha 5
# tomate.loria.fr 0m57.533s (Failure rate: 0%)
# tomate.loria.fr 0m33.705s EXTRA="-DAS_TRICK -DREDUCE2" (Failure rate: 0%)
# tomate.loria.fr 0m29.777s EXTRA="-DAS_TRICK" (Failure rate: 0%)
