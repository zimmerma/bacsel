/*
  Copyright 2023 Paul Zimmermann

  This program is free software; you can redistribute it and/or modify 
  it under the terms of the GNU General Public License as published by 
  the Free Software Foundation; either version 2 of the License, or 
  (at your option) any later version.

  This program is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
  General Public License for more details.

  You should have received a copy of the GNU General Public License 
  along with this program; see the file COPYING.  If not, write to 
  the Free Software Foundation, Inc., 59 Temple Place - Suite 330, 
  Boston, MA 02111-1307, USA.
*/

#include <assert.h>

/* y <- -1/2*y2(x)+1/2*y0(x) */
static inline void
d1 (mpfr_t y, mpfr_t x)
{
  mpfr_t t;
  mpfr_init2 (t, mpfr_get_prec (y));
  mpfr_yn (y, 2, x, MPFR_RNDN);
  mpfr_y0 (t, x, MPFR_RNDN);
  mpfr_sub (y, t, y, MPFR_RNDN);
  mpfr_div_2ui (y, y, 1, MPFR_RNDN);
  mpfr_clear (t);
}

/* y <- 1/4*y3(x) - 1/2*y1(x) + 1/4*y-1(x) */
static inline void
d2 (mpfr_t y, mpfr_t x)
{
  mpfr_t t;
  mpfr_init2 (t, mpfr_get_prec (y));
  mpfr_yn (t, 3, x, MPFR_RNDN);
  mpfr_div_2ui (y, t, 2, MPFR_RNDN); // 1/4*y3(x)
  mpfr_y1 (t, x, MPFR_RNDN);
  mpfr_div_2ui (t, t, 1, MPFR_RNDN); // 1/2*y1(x)
  mpfr_sub (y, y, t, MPFR_RNDN);   
  mpfr_yn (t, -1, x, MPFR_RNDN);
  mpfr_div_2ui (t, t, 2, MPFR_RNDN); // 1/4*y-1(x)
  mpfr_add (y, y, t, MPFR_RNDN);
  mpfr_clear (t);
}

/* y <- -1/8*y4(x) + 3/8*y2(x) - 3/8*y0(x) + 1/8*y-2(x) */
static inline void
d3 (mpfr_t y, mpfr_t x)
{
  mpfr_t t;
  mpfr_init2 (t, mpfr_get_prec (y));
  mpfr_yn (t, 4, x, MPFR_RNDN);
  mpfr_neg (y, t, MPFR_RNDN);
  mpfr_yn (t, 2, x, MPFR_RNDN);
  mpfr_mul_ui (t, t, 3, MPFR_RNDN);
  mpfr_add (y, y, t, MPFR_RNDN);
  mpfr_y0 (t, x, MPFR_RNDN);
  mpfr_mul_ui (t, t, 3, MPFR_RNDN);
  mpfr_sub (y, y, t, MPFR_RNDN);
  mpfr_yn (t, -2, x, MPFR_RNDN);
  mpfr_add (y, y, t, MPFR_RNDN);
  mpfr_div_2ui (y, y, 3, MPFR_RNDN);
  mpfr_clear (t);
}

#define NAME "y1"
#define EVAL(y, rnd_mode) mpfr_y1 (y, y, rnd_mode);

/* BNDDIFF(tmp, t0, t1, N, d) returns a bound for the (d+1)-th
   derivative of y1(x) for t0/N <= x < t1/N. We only implement it for
   d=1 and d=2. For d=1 the 2nd derivative is
   1/4*y3(x) - 1/2*y1(x) + 1/4*y-1(x). For d=2 the 3rd derivative is
   -1/8*y4(x) + 3/8*y2(x) - 3/8*y0(x) + 1/8*y-2(x). */
#define BNDDIFF(tmp, t0, t1, N, d)              \
  {                                             \
    mpfr_t t, y0, y1;                           \
    assert (d == 1 && d == 2);                  \
    mpfr_init2 (t, mpfr_get_prec (tmp));        \
    mpfr_init2 (y0, mpfr_get_prec (tmp));       \
    mpfr_init2 (y1, mpfr_get_prec (tmp));       \
    mpfr_set_z (tmp, t0, MPFR_RNDD);            \
    mpfr_div (tmp, tmp, N, MPFR_RNDD);          \
    mpfr_set_z (t, t1, MPFR_RNDU);              \
    mpfr_div (t, t, N, MPFR_RNDU);              \
    if (d == 1) {                               \
      d2 (y0, tmp);                             \
      d2 (y1, t);                               \
    } else {                                    \
      d3 (y0, tmp);                             \
      d3 (y1, t);                               \
    }                                           \
      /* we assume the maximum is attained in t0 or t1, */            \
      /* which is clearly wrong for large intervals */                \
    mpfr_abs (tmp, mpfr_cmpabs (y0, y1) > 0 ? y0 : y1, MPFR_RNDU);    \
    mpfr_clear (t);                                                   \
    mpfr_clear (y0);                                                  \
    mpfr_clear (y1);                                                  \
}                                               

#define TAYLOR(tmp, tm, Nin, P, NN, d)          \
  {                                             \
    assert (d == 1 && d == 2);                  \
    mpfr_set_z (tmp, tm, MPFR_RNDN);            \
    mpfr_div (tmp, tmp, Nin, MPFR_RNDN);        \
    /* tmp = x = tm/Nin */                      \
    mpfr_y1 (P[0], tmp, MPFR_RNDN);             \
    /* P[0] = y1(x) */                          \
    mpfr_mul (P[0], P[0], NN, MPFR_RNDN);       \
    /* P[0] = NN*y1(x) */                       \
    d1 (P[1], tmp);                             \
    /* multiply by NN*T and divide by Nin */    \
    mpfr_mul_z (P[1], P[1], T, MPFR_RNDN);      \
    mpfr_mul (P[1], P[1], NN, MPFR_RNDN);       \
    mpfr_div (P[1], P[1], Nin, MPFR_RNDN);      \
    /* P[1] = NN*(T/Nin)*diff(y1,x) */          \
    if (d == 2) {                               \
      d2 (P[2], tmp);                           \
      mpfr_mul_z (P[2], P[2], T, MPFR_RNDN);    \
      mpfr_mul_z (P[2], P[2], T, MPFR_RNDN);    \
      mpfr_mul (P[2], P[2], NN, MPFR_RNDN);     \
      mpfr_div (P[2], P[2], Nin, MPFR_RNDN);    \
      mpfr_div (P[2], P[2], Nin, MPFR_RNDN);    \
      /* divide by 2! */                        \
      mpfr_div_2ui (P[2], P[2], 1, MPFR_RNDN);  \
      /* P[2] = NN*(T/Nin)^2/2*diff(y1,x) */    \
    }                                           \
  }
