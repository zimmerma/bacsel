/*
  Copyright 2007-2020 Guillaume Hanrot, Vincent Lefèvre, Damien Stehlé 
  and Paul Zimmermann.

  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful, 
  but WITHOUT ANY WARRANTY; without even the implied warranty of 

  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public 
  License along with this program; see the file COPYING.  If not, 
  write to the Free Software Foundation, Inc., 59 Temple Place - 
  Suite 330, Boston, MA 02111-1307, USA.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <assert.h>
#include <gmp.h>
#include <mpfr.h>
#include "mat.h"
#include "poly.h"
#include "hensel.h"
#include "io.h"
#include "utils.h"

// primes[] are the primes used for Hensel lifting
#define NPRIMES 5
const int primes[NPRIMES] = {5, 7, 11, 13, 17};

// #define BENCH_INVERSE 
// #define NBIT_INVERSE 100000
// #define DEBUGHENSEL 1
// #define EVAL_ROOTSMODP
// #define NBIT_ROOTSMODP 1000
// #define EVAL_LIFT
// #define NBIT_LIFT 1000
// #define EVAL_LARGELIFT
// #define NBIT_LARGELIFT 1000
// #define DEBUGROOTSMODP 3
/*********************************************************************/
/**********************************************************************
    HENSEL lifting for a set of 2 polynomials.
    --> to find the small common roots of 2 bivariate polynomials 
        over the integers
**********************************************************************/
/*********************************************************************/

/* lim_primes[0][i] contains the largest p[i]^k that fits into 16 bits,
   and lim_primes[1][i] the largest p[i]^k that fits into 32 bits */
const long lim_primes[2][NPRIMES] = {{15625, 16807, 14641, 28561, 4913},
         {1220703125LL, 1977326743LL, 2357947691LL, 815730721LL, 410338673LL}};

#define MAXZ 289 // square of largest prime

/* Finds a good p in the list, ie, either no root mod p, or an invertible
   Jacobian mod p for all roots,
   then returns all the roots mod p, set nb to the number of roots,
   set P to p
*/
static void
roots_mod_p (mpz_t *P1, mpz_t *P2, int *I, int *nb, int d, int alpha,
             long *sP1X, long *sP2X, long *sP1Y, long *sP2Y,
             long *p1, long *p2, int z[MAXZ][2])
{ 
  int ncoefs = (alpha + 1)*(d*alpha + 2)/2;
  int i, success=0, j, p=0, x, y, zz1=0, zz2=0, zz3=0, zz4=0;

  /* try to find a good p: either no solution mod p, or an invertible
     Jacobian matrix mod p 
  */
#if DEBUGROOTSMODP >= 2
  printf ("P1 = "); print_pol_mpz (P1, d, alpha); 
  printf ("\nP2 = "); print_pol_mpz (P2, d, alpha); 
  printf ("\n");
#endif

  i = 0;
  while ((i<NPRIMES) && (success==0))
    { 
      success = 1;
      p = primes[i];

      /* p1 is P1 mod p, p2 is P2 mod p */
	for (j = 0; j < ncoefs; j++) { 
	  p1[j] = mpz_fdiv_ui (P1[j], lim_primes[mp_bits_per_limb >> 6][i]); 
	  p2[j] = mpz_fdiv_ui (P2[j], lim_primes[mp_bits_per_limb >> 6][i]); 
	}

      /* Compute the Jacobian matrix mod p */
      diff1(sP1X, p1, p, d, alpha); 
      diff2(sP1Y, p1, p, d, alpha); 
      diff1(sP2X, p2, p, d, alpha); 
      diff2(sP2Y, p2, p, d, alpha);

      *nb = 0; 

      for (x = 0; success && (x < p); x++)
	for (y = 0; success && (y < p); y++) {
	  if (!eval_mod(p1, x, y, p, d, alpha) 
	      && !eval_mod(p2, x, y, p, d, alpha))
	    { 
	      zz1 = eval_mod (sP1X, x, y, p, d, alpha); 
	      zz2 = eval_mod (sP1Y, x, y, p, d, alpha); 
	      zz3 = eval_mod (sP2X, x, y, p, d, alpha); 
	      zz4 = eval_mod (sP2Y, x, y, p, d, alpha); 
	      if ((zz1*zz4 - zz2*zz3) % p == 0) success = 0;
              assert (*nb < MAXZ);
	      z[*nb][0] = x;
	      z[*nb][1] = y;
	      (*nb)++; 
	    }
	}

      if ((*nb==0)||(success==0))
	*nb = 0;

      i++;
    }

  // in case of success, we put in I the index of the successful prime
  *I = (success) ? i-1 : -1;

  /* For test 3d in test.sh (exp in binary64 with d=alpha=2), we get
     2% failures, 61% success with p=5, 26% success with p=7,
     8.4% success with p=11, 1.7% success with p=13, and 0.4% success
     with p=17.

     We get a failure in particular when P1 and P2 have a common
     factor, for example:
     10833099983*X^2 - 6449224909922304*X + 1349*Y - 122312976755546142720.
     In such a case, no prime will succeed. */

  return; 
}


void
hensel_init (hensel_t h, int d, int alpha)
{
  int ncoefs, j;
  mpz_init (h->htmp1);
  mpz_init (h->htmp2);
  mpz_init (h->htmp3);
  mpz_init (h->X1);
  mpz_init (h->_Y1);
  mpz_init (h->D1X);
  mpz_init (h->D2X);
  mpz_init (h->D1Y);
  mpz_init (h->D2Y);
  mpz_init (h->X);
  mpz_init (h->Y);
  mpz_init (h->hM);
  mpz_init (h->hbnd1);

  ncoefs = ((alpha + 1) * (d * alpha + 2)) / 2;
  h->ncoefs = ncoefs;
  h->P1X = (mpz_t *) malloc (ncoefs * sizeof(mpz_t)); 
  h->P2X = (mpz_t *) malloc (ncoefs * sizeof(mpz_t)); 
  h->P1Y = (mpz_t *) malloc (ncoefs * sizeof(mpz_t)); 
  h->P2Y = (mpz_t *) malloc (ncoefs * sizeof(mpz_t)); 
  for (j = 0; j < ncoefs; j++)
    {
      mpz_init (h->P1X[j]);
      mpz_init (h->P2X[j]);
      mpz_init (h->P1Y[j]);
      mpz_init (h->P2Y[j]);
    }

  h->sP1X = (long *) malloc (ncoefs * sizeof(long)); 
  h->sP2X = (long *) malloc (ncoefs * sizeof(long)); 
  h->sP1Y = (long *) malloc (ncoefs * sizeof(long)); 
  h->sP2Y = (long *) malloc (ncoefs * sizeof(long)); 

  h->p1 = (long *) malloc (ncoefs * sizeof(long)); 
  h->p2 = (long *) malloc (ncoefs * sizeof(long)); 
}

void
hensel_clear (hensel_t h)
{
  int j, ncoefs;
  mpz_clear (h->htmp1);
  mpz_clear (h->htmp2);
  mpz_clear (h->htmp3);
  mpz_clear (h->X1);
  mpz_clear (h->_Y1);
  mpz_clear (h->D1X);
  mpz_clear (h->D2X);
  mpz_clear (h->D1Y);
  mpz_clear (h->D2Y);
  mpz_clear (h->X);
  mpz_clear (h->Y);
  mpz_clear (h->hM);
  mpz_clear (h->hbnd1);

  ncoefs = h->ncoefs;
  for (j = 0; j < ncoefs; j++)
    {
      mpz_clear (h->P1X[j]);
      mpz_clear (h->P2X[j]);
      mpz_clear (h->P1Y[j]);
      mpz_clear (h->P2Y[j]);
    }
  free (h->P1X);
  free (h->P2X);
  free (h->P1Y);
  free (h->P2Y);

  free (h->sP1X);
  free (h->sP2X);
  free (h->sP1Y);
  free (h->sP2Y);

  free (h->p1);
  free (h->p2); 
}

/* Finds the roots (x,y) of P1 and P2 over the integers 
by using Hensel's lifting with a good p. 
Store the roots in Sol, and their number in Nb
*/

int 
hensel (int *Nb, mpz_t *P1, mpz_t *P2, mpz_t bnd, int d, int alpha,
        mpz_t **Sol, hensel_t h)
{ 
  int i, p, nb, j, success;
  long sX, sY, sD1X, sD1Y, sD2X, sD2Y, sM, p0;
  long sX1,sY1;
  mpz_ptr htmp1, htmp2, htmp3, X1, _Y1, D1X, D2X, D1Y, D2Y, X, Y, hM, hbnd1;
  mpz_t *P1X, *P2X, *P1Y, *P2Y;
  long  *sP1X, *sP2X, *sP1Y, *sP2Y;
  long *p1, *p2;
  int z[MAXZ][2];

  htmp1 = h->htmp1;
  htmp2 = h->htmp2;
  htmp3 = h->htmp3;
  X1 = h->X1;
  _Y1 = h->_Y1;
  D1X = h->D1X;
  D2X = h->D2X;
  D1Y = h->D1Y;
  D2Y = h->D2Y;
  X = h->X;
  Y = h->Y;
  hM = h->hM;
  hbnd1 = h->hbnd1;
  P1X = h->P1X;
  P2X = h->P2X;
  P1Y = h->P1Y;
  P2Y = h->P2Y;
  sP1X = h->sP1X;
  sP2X = h->sP2X;
  sP1Y = h->sP1Y;
  sP2Y = h->sP2Y;
  p1 = h->p1;
  p2 = h->p2;

#ifdef EVAL_ROOTSMODP
  int cptr_rmp, st_rmp = cputime(); 
  for (cptr_rmp = 0; cptr_rmp < NBIT_ROOTSMODP; cptr_rmp++)
#endif
    roots_mod_p (P1, P2, &i, &nb, d, alpha, sP1X, sP2X, sP1Y, sP2Y, p1, p2, z);

#ifdef EVAL_ROOTSMODP
  printf("RMP %d\n", (cputime() - st_rmp) / NBIT_ROOTSMODP); 
#endif

#ifdef EVAL_LIFT
  long st_lift = cputime(), compteur_lift;
  for (compteur_lift = 0; compteur_lift < NBIT_LIFT; compteur_lift++) 
    {
#endif
  if (i == -1) success = -1; 
  else success = 1;

  *Nb = nb;
  if (success == -1 || (nb == 0 && success == 1))
    goto end;

#ifdef DEBUGHENSEL
  printf("my_solve("); 
  print_pol_mpz(P1, d, alpha); printf(","); 
  print_pol_mpz(P2, d, alpha); printf(","); 
#endif

  /* half of the largest power of p fitting in a long */ 
  p = primes[i]; 
  p0 = lim_primes[mp_bits_per_limb >> 6][i]; 

#ifdef DEBUGHENSEL
  //   printf("Using p = %d, longs up to %d, bnd = 2*", p, p0); 
  printf("%d,", p); 
  // mpz_out_str(stdout, 10, bnd); printf("\n"); 
#endif
   
  diff1 (sP1X, p1, p0, d, alpha); 
  diff1 (sP2X, p2, p0, d, alpha); 
  diff2 (sP1Y, p1, p0, d, alpha); 
  diff2 (sP2Y, p2, p0, d, alpha); 

  for (j = 0; j < nb; j++) {

#if DEBUGHENSEL >= 3
      printf("p = %d, starting from x = %d, y = %d\n", 
	     p, z[j][0], z[j][1]); 
#endif
      sM = p;
      sX = z[j][0]; 
      sY = z[j][1]; 

    do 
      {
	long stmp1, stmp2; 

	sM = sM * sM; 
	if (sM > p0) sM = p0;
	sX1  = eval_mod (p1, sX, sY, sM, d, alpha); 
	sY1  = eval_mod (p2, sX, sY, sM, d, alpha); 
	sD1X = eval_mod (sP1X, sX, sY, sM, d, alpha); 
	sD2X = eval_mod (sP2X, sX, sY, sM, d, alpha);
	sD1Y = eval_mod (sP1Y, sX, sY, sM, d, alpha); 
	sD2Y = eval_mod (sP2Y, sX, sY, sM, d, alpha); 
	
#if DEBUGHENSEL >= 3
	printf ("(X1, Y1) = (%ld, %ld)\n", sX1, sY1); 
	printf ("(D1X, D1Y) = (%ld, %ld)\n", sD1X, sD1Y); 
	printf ("(D2X, D2Y) = (%ld, %ld)\n", sD2X, sD2Y); 
#endif

	stmp1 = (sD1X * sD2Y) % sM; 
	stmp1 -= sD2X * sD1Y; 

#ifdef BENCH_INVERSE 
      long compteur_inverse, st_inv = cputime(); 
      for (compteur_inverse = 0; compteur_inverse < NBIT_INVERSE; 
	   compteur_inverse++) 
#endif
	stmp1 = small_invert (stmp1, sM); 
#ifdef BENCH_INVERSE
      printf("Inverse %d\n", (cputime() - st_inv) / NBIT_INVERSE); 
#endif

	/*
        cur_inv = newton_invert (stmp1, cur_inv, sM);

	if ((stmp0 - cur_inv) % sM) 
	  printf("M = %ld cur_inv = %ld stmp1 = %ld\n", sM, cur_inv, stmp0); 
	  stmp1 = stmp0; */

#if DEBUGHENSEL >= 3
	printf ("invdet = %ld\n", stmp1); 
#endif
	stmp2 = (sD2Y * sX1) % sM; 
	stmp2 -= sD1Y * sY1; 
	stmp2 %= sM; 
	stmp2 = (stmp1 * stmp2) % sM; 
	sX -= stmp2; 
	sX = sX % sM; 
	
	stmp2 = (sD1X * sY1) % sM; 
	stmp2 -= sD2X * sX1; 
	stmp2 = stmp2 % sM; 
	stmp2 = (stmp1 * stmp2) % sM; 
	sY -= stmp2; 
	sY = sY % sM; 
#if DEBUGHENSEL >= 3
	printf ("(X, Y) = (%ld, %ld)\n", sX, sY); 
#endif
      }
    while (sM < p0 && mpz_cmp_ui(bnd, (sM-1)/2) > 0);

    if (mpz_cmp_ui(bnd, (sM-1)/2) < 0) {
      long sbnd = mpz_get_ui(bnd); 

      if (sX < 0) sX += sM; 
      if (sY < 0) sY += sM; 
      if (sX > sbnd)
	mpz_set_si(Sol[0][j], sX - sM); 
      else mpz_set_ui(Sol[0][j], sX); 
      
      if (sY > sbnd)
        mpz_set_si(Sol[1][j], sY - sM);
      else mpz_set_ui(Sol[1][j], sY);

      continue;       
    }
  
#if DEBUGHENSEL >= 3
      printf("Switching to mpz's\n"); 
#endif
      deriv_pol1 (P1X, P1, d, alpha);
      deriv_pol1 (P2X, P2, d, alpha);
      deriv_pol2 (P1Y, P1, d, alpha);
      deriv_pol2 (P2Y, P2, d, alpha);

#ifdef EVAL_LARGELIFT
      long compteur_largelift, st_largelift = cputime(); 
      for (compteur_largelift = 0; compteur_largelift < NBIT_LARGELIFT; 
	   compteur_largelift++) 
	{
#endif
    mpz_set_si(X, sX); 
    mpz_set_si(Y, sY); 
    mpz_set_ui(hM, sM); 
    mpz_mul_ui (hbnd1, bnd, 2);
    mpz_add_ui (hbnd1, hbnd1, 1); 

    do {
      mpz_mul(hM, hM, hM); 
      eval_pol (X1, P1, X, Y, hM, d, alpha); 
      eval_pol (_Y1, P2, X, Y, hM, d, alpha);
      eval_pol (D1X, P1X, X, Y, hM, d, alpha); 
      eval_pol (D2X, P2X, X, Y, hM, d, alpha);
      eval_pol (D1Y, P1Y, X, Y, hM, d, alpha); 
      eval_pol (D2Y, P2Y, X, Y, hM, d, alpha); 
      
#if DEBUGHENSEL >= 3
      gmp_printf ("(X1, Y1) = (%Zd, %Zd)\n", X1, _Y1);
      gmp_printf ("(D1X, D1Y) = (%Zd, %Zd)\n", D1X, D1Y);
      gmp_printf ("(D2X, D2Y) = (%Zd, %Zd)\n", D2X, D2Y);
#endif
	
      mpz_mul (htmp1, D1X, D2Y); 
      mpz_fdiv_r (htmp1, htmp1, hM); 
      
      mpz_mul (htmp2, D1Y, D2X); 
      mpz_fdiv_r (htmp2, htmp2, hM); 
      
      mpz_sub (htmp1, htmp1, htmp2); 
      mpz_fdiv_r (htmp1, htmp1, hM); 

#ifdef BENCH_INVERSE 
      long compteur_inverse, st_inv = cputime(); 
      for (compteur_inverse = 0; compteur_inverse < NBIT_INVERSE; 
	   compteur_inverse++) 
#endif
      mpz_invert (htmp1, htmp1, hM); 
#ifdef BENCH_INVERSE
      printf("Inverse %d\n", (cputime() - st_inv) / NBIT_INVERSE); 
#endif
      
#if DEBUGHENSEL >= 3
      gmp_printf ("invdet = %Zd\n", htmp1); 
#endif
	
      mpz_mul (htmp2, D2Y, X1); 
      mpz_mul (htmp3, D1Y, _Y1);
      mpz_sub (htmp2, htmp2, htmp3); 	
      mpz_fdiv_r (htmp2, htmp2, hM); 
      mpz_mul (htmp2, htmp2, htmp1); 
      mpz_fdiv_r (htmp2, htmp2, hM); 
      mpz_sub (X, X, htmp2); 
      mpz_fdiv_r (X, X, hM); 
	
      mpz_mul (htmp2, D1X, _Y1);
      mpz_mul (htmp3, D2X, X1);
      mpz_sub (htmp2, htmp2, htmp3);
      mpz_fdiv_r (htmp2, htmp2, hM);
      mpz_mul (htmp2, htmp2, htmp1);
      mpz_fdiv_r (htmp2, htmp2, hM);
      mpz_sub (Y, Y, htmp2); 
      mpz_fdiv_r (Y, Y, hM); 
      
#if DEBUGHENSEL >= 3
      gmp_printf ("(X, Y) = (%Zd, %Zd)\n", X, Y); 
#endif
    }
    while (mpz_cmp (hM, hbnd1) <=0); 
    
      /* Set tmp1 to ceil(M/2), and compute X cmod M and Y cmod M */
      
#if DEBUGHENSEL >= 3
    gmp_printf("-> X = %Zd Y = %Zd hM = %Zd bnd = %Zd\n", X, Y, hM, bnd);
#endif
      
    mpz_add_ui (htmp1, hM, 1);
    mpz_div_ui (htmp1, hM, 2);
    if (mpz_cmp (X, htmp1) >= 0) mpz_sub (X, X, hM);
    if (mpz_cmp (Y, htmp1) >= 0) mpz_sub (Y, Y, hM);
    
    mpz_set (Sol[0][j], X);
    mpz_set (Sol[1][j], Y);
#ifdef EVAL_LARGELIFT
	}
      printf("Large %d\n",( cputime() - st_largelift) / NBIT_LARGELIFT); 
#endif

#ifdef DEBUGHENSEL
    if (j == 0) gmp_printf("%Zd, [", hM); 
    gmp_printf("[%Zd, %Zd]", Sol[0][j], Sol[1][j]); 
    if (j < nb - 1) printf(","); 
#endif
  }
#ifdef EVAL_LIFT
    }
  printf("Lift %ld\n", (cputime() - st_lift) / NBIT_LIFT);
#endif
#ifdef DEBUGHENSEL
  if (nb) printf("])\n"); 
#endif

 end:
  return success;
}
