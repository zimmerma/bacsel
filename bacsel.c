/*
  Copyright 2007-2024 Guillaume Hanrot, Vincent Lefèvre, Damien Stehlé and
  Paul Zimmermann.

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; see the file COPYING.  If not, write to the Free
  Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
  02111-1307, USA.

*/

#define VERSION "4.0"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <omp.h>
#include <gmp.h>
#include <mpfr.h>
#include "mat.h"
#include "io.h"
#include "poly.h"
#include "hensel.h" /* must come before slz.h */
#include "slz.h"
#include "taylor.h"
#include "function.h"
#include "utils.h"
#include "getexp.h"
#include "delta_eta.h"

// #define VBSE
// #define VBSE1
// #define DEB
// #define DEBDEB
// #define DEB2
// #define VERIF
// #define AUTOMATIC
/*#define NO_OVERPASS_FAILURES
#define EVAL_TAYLOR
#define NBIT_TAYLOR 1000
#define EVAL_CHECK
#define NBIT_CHECK 1000
*/
#ifdef AUTOMATIC
#ifndef FAILURE_LOWERBOUND
#define FAILURE_LOWERBOUND 2.0
#endif
#ifndef FAILURE_UPPERBOUND
#define FAILURE_UPPERBOUND 4.0
#endif
#endif

/* Choose your basis. Default: 10. */
#ifndef BASIS
#define BASIS 10
#endif

#define LARGE_PREC 1200 /* FIXME: set according to n */

/* global variables that do not depend on [t0,t1] */
mpfr_t tau; /* elementary step */
#ifdef SINBIG
mpfr_t pi, twopi; /* Pi and 2*Pi */
#endif
mpfr_t one_half; /* 1/2 */
int near; /* rounding mode */
long Failures = 0, Good = 0;
long nsols = 0; // total number of solutions printed
mpz_t AverageT;
mpfr_t arg_y; /* 2nd argument for bivariate functions */

int verbose = 1;

#define DMAX 16

unsigned long Calls[DMAX], Successes[DMAX];

/********************************************************************/
/* checks that the returned possible solutions in  Sol are indeed
   solutions, in which case they are printed */
/********************************************************************/

/* For all x0 \in Res, print x iff. | NN. f( (qx0+tm)/N ) cmod 1| \leq 1/M */
/* and qx0+tm < t1 */

/* print solution m*BASIS^e */
static void
print_sol (mpz_t m, int e)
{
  nsols ++;
  printf ("*** x");
#ifdef POW
  printf (",y");
#endif
  printf ("=");
#if BASIS==10
#ifdef POW
#error "not yet implemented"
#endif
  mpz_out_str (stdout, 10, m);
  printf("E%d, distance is: ", e);
#else
  mpfr_t x;
  mpfr_init2 (x, mpz_sizeinbase (m, 2));
  mpfr_set_z (x, m, MPFR_RNDN);
  mpfr_mul_2si (x, x, e, MPFR_RNDN);
#ifndef POW
  mpfr_printf ("%Ra, distance is: ", x);
#else
  mpfr_printf ("%Ra,%Ra, distance is: ", x, arg_y);
#endif
  mpfr_clear (x);
#endif
}

static void
Check (mpz_t *Res, int Nb, mpz_t tm, mpz_t t1, mpfr_t N, mpfr_t NN,
       mpfr_t oneoverM, mpz_t T, int e_in, int n, mpz_t q MAYBE_UNUSED,
       mpfr_t yy MAYBE_UNUSED)
{
  int i;
  mpfr_t y1, y2;

  mpfr_init (y1);
  mpfr_init (y2);

  for (i = 0; i < Nb; i++)  {
    if (mpz_cmpabs(Res[i], T) <= 0) { /* | Res[i] | <= T  */
#ifdef SINBIG
      mpz_mul(Res[i], Res[i], q);
#endif
      mpz_add(Res[i], Res[i], tm);

      if (mpz_cmp (Res[i], t1) < 0){
	mpfr_set_z (y1, Res[i], MPFR_RNDN);
	mpfr_div (y1, y1, N, MPFR_RNDN); /* (Res[i]+tm)/N */

	EVAL(y1, MPFR_RNDN);

	mpfr_mul (y1, y1, NN, MPFR_RNDN); /* NN * f((tm + Res[i]) / N) */

	if (near == 1) mpfr_sub (y1, y1, one_half, MPFR_RNDN);

	mpfr_round (y2, y1);
	mpfr_sub (y1, y1, y2, MPFR_RNDN);
	mpfr_abs (y1, y1, MPFR_RNDN); /* { NN * f((tm + Res[i]) / N) } */

#if DEBUG >= 2
	printf ("oneover M is ");
	mpfr_out_str (stdout, 10, 10, oneoverM, MPFR_RNDN);
	printf ("\ny1 is ");
	mpfr_out_str (stdout, 10, 10, y1, MPFR_RNDN);
	printf ("\n");
#endif

	if ((mpfr_cmp (y1, oneoverM) <= 0)) /* Indeed a solution. */
#pragma omp critical
          {
            print_sol (Res[i], e_in - n);
            /* if mode is 'all', since we multiplied NN by 2,
               we divide the distance by 2 to get comparable distances */
            if (near == 2) mpfr_div_2ui (y1, y1, 1, MPFR_RNDN);
            mpfr_out_str (stdout, 10, 10, y1, MPFR_RNDN);
            printf ("\n");
            fflush (stdout);
          }
      }
    }
  }

  mpfr_clear (y1);
  mpfr_clear (y2);
}

static int
remove_doubles(mpz_t *Res, int Nb)
{
  int i, j, k;

  /* Remove several occurrences of the same number */
  for (i = 0; i < Nb; i++)
    for (j = i+1; j < Nb; j++){
      if (mpz_cmp (Res[i], Res[j]) == 0){
	for (k = j; k < Nb-1; k++)
	  mpz_set (Res[k], Res[k+1]);
	Nb--;
      }
    }

  return Nb;
}
/**************************************************************************/

/* naive search (d=0,t=1) */
static void
naive_search (const mpz_t t0, const mpz_t t1, unsigned int n, unsigned int nn,
              unsigned int m, int e_in)
{
  mpz_t u;
  mpfr_t Nin, x, y, z, t, y1;

  mpz_init_set (u, t0);
  mpfr_init (Nin);
  mpfr_init2 (x, n);
  mpfr_init2 (y, nn + 1);
  /* we should output values where f(x) is at distance less than 2^-m ulps
     from an nn-bit number (or the middle of two nn-bit numbers), thus when
     rounding to nearest to precision nn+m, we should get a number
     representable in nn+1 bits */
  mpfr_init2 (z, nn + m);
  mpfr_init2 (t, nn + 1 + m + 100);
  mpfr_init (y1);

  /* Nin = BASIS^(n-e_in) */
  mpfr_set_d (Nin, (double) n - (double) e_in, MPFR_RNDN);
  mpfr_ui_pow (Nin, BASIS, Nin, MPFR_RNDN);

  /* for the naive search, we don't need that t0 and t1-1 have the
     same exponent */

  while (mpz_cmp (u, t1) < 0)
  {
    /* x = u/Nin*B^e_in */
    mpfr_set_z (z, u, MPFR_RNDN);
    mpfr_div (z, z, Nin, MPFR_RNDN);
    EVAL (z, MPFR_RNDN);
    mpfr_set (y, z, MPFR_RNDN);
    if (mpfr_cmp (y, z) == 0)
    {
      /* discard for directed rounding if last bit of y is 1,
         or with rounding to nearest if last bit of y is 0 */
      if ((near == 0 && mpfr_min_prec (y) < nn + 1) ||
          (near == 1 && mpfr_min_prec (y) == nn + 1) || near == 2)
      {
        mpfr_set_z (z, u, MPFR_RNDN);
        mpfr_div (z, z, Nin, MPFR_RNDN);
        mpfr_set (t, z, MPFR_RNDN);
        EVAL (z, MPFR_RNDN);
        EVAL (t, MPFR_RNDN);
        mpfr_sub (t, t, z, MPFR_RNDN);
        mpfr_abs (t, t, MPFR_RNDN);
        /* divide by ulp(z) = 2^(EXP(z)-nn) */
        mpfr_div_2si (t, t, mpfr_get_exp (z) - nn, MPFR_RNDN);
#pragma omp critical
        {
          print_sol (u, e_in - n);
          mpfr_out_str (stdout, 10, 10, t, MPFR_RNDN);
          printf ("\n");
          fflush (stdout);
        }
      }
    }
    mpz_add_ui (u, u, 1);
  }

  mpz_clear (u);
  mpfr_clear (Nin);
  mpfr_clear (x);
  mpfr_clear (y);
  mpfr_clear (z);
  mpfr_clear (t);
  mpfr_clear (y1);
}

/* Process the interval [t0,t1-q] (t1 is excluded).
   Return a non-negative value in case of success,
   a negative value in case of failure.
   With -DAUTOMATIC, a return value of -2 means
   "Initial value of -t is too large".
   In case of failure, put in t1_done the largest value done so far,
   so that it remains only to process [t1_done,t1].
*/
static int
doit (const mpz_t t0, const mpz_t t1, double n, double nn, double m, double t,
      int d, int alpha, int e_in, mpz_t q, mpz_t t1_done)
{
  int Nb, success = -1, i, npols, ncoefs, *ok, j, nb;
  mpfr_prec_t e_out;
  double total_T = 0.0;
  mpz_t *Res = NULL, C, initial_T, T, tm, ztmp, ztmp2, Calpha, NZ;
  mpz_t initial_T0;
  mpz_t *PP, *Ptmp, t0current, done, t1current;
  mpfr_t tmp, tmp2, *P, M, oneoverM, Mprime, NN, Nin, eps;
  long failures = 0, good = 0; /* number of failures */
  mat_ZZ W;
  unsigned long int time_4_lll=0, time_4_Hensel=0;
  /* LLL variables */
  mpz_t **B_lll;
  mpfr_t yy; /* auxiliary variable used in EVAL() and TAYLOR() */
  mpz_t *Sol[2];
  hensel_t h;

#pragma omp atomic update
  Calls[d] ++;

  if (d == 0)
  {
    naive_search (t0, t1, (int) n, (int) nn, (int) m, e_in);
#pragma omp atomic update
    Successes[0] ++;
    return 1;
  }

  ncoefs = ((alpha + 1) * (d * alpha + 2)) / 2;
  npols  = ((alpha + 1) * (alpha + 2)) / 2;   /* Nb. columns */
  nb = d * alpha * d * alpha; /* Bezout */
#ifdef CHECKHENSEL
  pari_init(100000000, 100000);
#endif

  mpz_init (C); mpz_init (initial_T); mpz_init (T);
  mpz_init (tm); mpz_init (ztmp); mpz_init (ztmp2); mpz_init (Calpha);
  mpz_init (NZ); mpz_init (t1current); mpz_init (initial_T0);

  Sol[0] = (mpz_t *) malloc (nb * sizeof(mpz_t));
  Sol[1] = (mpz_t *) malloc (nb * sizeof(mpz_t));
  for (j = 0; j < nb; j++)
    {
      mpz_init (Sol[0][j]);
      mpz_init (Sol[1][j]);
    }

  mpfr_init (Nin);
  mpfr_init (tmp);
  mpfr_init (tmp2);

  mpfr_init2 (NN, 53);
  mpfr_init2 (eps, 53);
  mpfr_init2 (M, 53);
  mpfr_init2 (Mprime, 53);
  mpfr_init2 (oneoverM, 53);
  mpfr_init2 (yy, LARGE_PREC);

  ok   = (int *) malloc ((npols+1) * sizeof (int));
  PP   = init_vec (d+1);
  Ptmp = init_vec (ncoefs);

  P = (mpfr_t *) malloc ((d+1)*sizeof (mpfr_t));
  for (i = 0; i<=d; i++) mpfr_init (P[i]);

  init_matrix (&W, npols, ncoefs);

  allocate_matrix (&B_lll, npols, ncoefs);

  mpz_init (t0current);
  mpz_init (done);

  hensel_init (h, d, alpha);

  /***  END OF Initialization and argument parsing ***/

  /***  BEGINNING OF computing auxiliary variables ***/
  mpfr_set_d (tmp, n, MPFR_RNDN);
  mpfr_ui_pow (Nin, BASIS, tmp, MPFR_RNDN); /* B^p \pm 0.5 ulp(B^p) */
  check_t0_t1 (t0, t1, Nin);

  /* N = basis^n, NN=basis^nn, M=basis^m, T=initial_T=basis^t */
  // ERR. ANA.: exact value PREC >= (log_2 n) +1
  // ERR. ANA.: |N-BASIS^tmp| <= 0.5 ulp(BASIS^n).

  mpz_ui_pow_ui (T, BASIS, abs(e_in)); /* T = B^e_{in} */

  if (e_in > 0)
    mpfr_div_z (Nin, Nin, T, MPFR_RNDN);
  else
    mpfr_mul_z (Nin, Nin, T, MPFR_RNDN); /* B^{p - e_{in}} */

  mpfr_set_d (tmp, m, MPFR_RNDN);
  // ERR. ANA.: exact value
  mpfr_ui_pow (M, BASIS, tmp, MPFR_RNDN);
  // ERR. ANA.: |M-BASIS^m| <= 0.5 ulp (BASIS^m)
  mpfr_ui_div (oneoverM, 1, M, MPFR_RNDU);
  // ERR. ANA.: |oneoverM-BASIS^-m| <= 0.5 ulp (1/M)

  mpfr_set_d (tmp, t, MPFR_RNDN);
  mpfr_ui_pow (tmp, BASIS, tmp, MPFR_RNDN);
  mpfr_get_z (T, tmp, MPFR_RNDN);
  mpz_set (initial_T, T);
  mpz_set (initial_T0, initial_T);

#if DEBUG >= 2
  printf ("Nin= "); mpfr_out_str (stdout, 10, 10, Nin, MPFR_RNDN);
  printf ("\nM= "); mpfr_out_str (stdout, 10, 10, M, MPFR_RNDN);
  printf ("\n");
#endif

#ifndef SINBIG

  /* re-check that f(t0*2^(e_in-n)) and f((t1-q)*2^(e_in-n)) have same
     exponent, since for a non-monotonic function it might be the case
     for the global [t0,t1] range, but not for smaller ranges */
#ifndef MONOTONOUS
  check_exponents (t0, t1, e_in, (int) n, q);
#endif

  e_out = getexp (t0, e_in - n);
  mpfr_set_d (tmp, nn - (double) e_out, MPFR_RNDN);
  // ERR. ANA.: exact value.
  mpfr_ui_pow (NN, BASIS, tmp, MPFR_RNDN);
  // ERR. ANA.: |NN-BASIS^(nn-e_out)| <= 0.5 ulp(BASIS^(nn-e_out)).
  if (near == 2) mpfr_mul_ui (NN, NN, 2, MPFR_RNDN);
  // ERR. ANA.: |NN-2.BASIS^(nn-e_out)| <= 0.5 ulp(2*lastNN)
  //                                    <= ulp((1+0.5ulp(1))BASIS^(nn-e_out))
  //                                    <= 2 ulp(BASIS^(nn-e_out))

#if DEBUG >= 2
  printf ("NN= "); mpfr_out_str (stdout, 10, 10, NN, MPFR_RNDN);
  printf ("\n");
#endif

#endif /* ifndef SINBIG */
  /*** END OF computing auxiliary variables ***/

  /* BEGINNING OF main loop. t0 starts as its initial value, and is
     progressively increased, taking into account the part already
     studied. The loop ends when t0 reaches t1. At each step we have
     done all values < t0. */

  mpz_set (t0current, t0);
  mpz_set_ui (done, 0);

#ifdef AUTOMATIC
  int smallerT = 0;
#endif
  while (mpz_cmp (t0current, t1) < 0) {

      mpz_mul (ztmp, q, T);
      mpz_add (tm, t0current, ztmp); /* tm is the middle */
      mpz_add (t1current, tm, ztmp); /* t1current is the right bound */
      /* ensure that t1current <= t1 */
      if (mpz_cmp (t1, t1current) < 0)
        mpz_set (t1current, t1);

      /* compute eps, an upper bound for the remainder of the order d-Taylor
         formula on [t0current, t1current], and multiply by NN */
      bnddiff (eps, tau, tmp, Nin, T, t0current, t1current, d);
#if DEBUG >= 2
      printf ("eps= "); mpfr_out_str (stdout, 10, 10, eps, MPFR_RNDN); printf ("\n");
#endif
      mpfr_mul (eps, eps, NN, MPFR_RNDU);

      /* deduce Mprime, C and Calpha:
         Mprime := E(1/2[1/M+eps])
         The 1/2 factor takes into account numerical errors on the Taylor
         coefficients. */

      mpfr_ui_div (Mprime, 1, M, MPFR_RNDN);
      mpfr_add (Mprime, Mprime, eps, MPFR_RNDN);
      mpfr_mul_2ui (Mprime, Mprime, 1, MPFR_RNDN);
      mpfr_ui_div (Mprime, 1, Mprime, MPFR_RNDN);

      mpfr_get_z (C, Mprime, MPFR_RNDD);
      if (mpz_sgn (C) == 0)
      {
#ifndef AUTOMATIC
        fprintf (stderr, "Initial value of -t is too large\n");
        exit (1);
#else
        success = -2;
        goto clear_and_exit;
#endif
      }
      mpz_pow_ui (Calpha, C, alpha);

#ifdef SINBIG
      /* Check that t0current and t1current have the same e_out */
      /* If it is not the case, halve T */
      e_out = getexp (t0current, e_in - n);

      mpfr_set_prec (tmp, 53);
      mpfr_set_d (tmp, nn - (double) e_out, MPFR_RNDN);
      // ERR. ANA.: exact value.
      mpfr_ui_pow (NN, BASIS, tmp, MPFR_RNDN);
      // ERR. ANA.: |NN-BASIS^(nn-e_out)| <= 0.5 ulp(BASIS^(nn-e_out)).
      if (near == 2) mpfr_mul_ui (NN, NN, 2, MPFR_RNDN);
      // ERR. ANA.: |NN-2.BASIS^(nn-e_out)| <= 0.5 ulp(2*lastNN)
      //                         <= ulp((1+0.5ulp(1))BASIS^(nn-e_out))
      //                         <= 2 ulp(BASIS^(nn-e_out))

      mpfr_set_prec (tmp, mpfr_get_default_prec ());

      mpfr_prec_t e_out2 = getexp (t1current, e_in - n);
      if (e_out != e_out2)
        goto halve_T;

#if DEBUG >= 2
      printf ("NN= "); mpfr_out_str (stdout, 10, 10, NN, MPFR_RNDN);
      printf ("\n");
#endif

#endif /* SINBIG */

#if DEBUG >= 1
      gmp_printf ("Checking [%Zd,%Zd)\n", t0current, t1current);
#endif

#if DEBUG >= 2
      printf ("t0current= "); mpz_out_str (stdout, 10, t0current);
      printf ("\n");
      printf ("T="); mpz_out_str (stdout, 10, T); printf ("\n");
      printf ("tm="); mpz_out_str (stdout, 10, tm); printf ("\n");
#endif

#ifdef EVAL_TAYLOR
      int st_taylor = cputime(), cptr_taylor;
      for (cptr_taylor = 0; cptr_taylor < NBIT_TAYLOR; cptr_taylor++){
#endif
	/* Compute the Taylor polynomial */
#ifdef SIN_OR_COS
	TAYLOR(tmp, tmp2, tm, Nin, P, NN, d);
#elif defined(POW)
        TAYLOR(tmp, tm, Nin, P, NN, d, T);
#else
        // FIXME: pass T through TAYLOR as for pow instead as global
	TAYLOR(tmp, tm, Nin, P, NN, d);
#endif	
	if (near == 1) mpfr_sub (P[0], P[0], one_half, MPFR_RNDN);
#ifdef EVAL_TAYLOR
      }
      printf("Taylor %d\n", (cputime() - st_taylor)/NBIT_TAYLOR);
#endif

#if DEBUG >= 2
      printf ("P="); print_uni_pol_mpfr(P, d); printf("\n");
      /* P(x=i/T)/NN approximates f((tm+i)/Nin) */
#endif

      /* Look for the solutions of the equation |P(x_0/T) cmod 1| < 1/C,
	 where  |x0| \leq T and C = floor (1/(1/M+eps)).*/
      success = slz (&Res, &Nb, P, C, T, d, alpha, tmp, &W,
		     npols, ncoefs, ztmp, Calpha, ztmp2, ok, PP, Ptmp, B_lll,
		     &time_4_lll, &time_4_Hensel, Sol, h);

      if (success == -2) /* PP[d] = 0 */
        goto clear_and_exit;

      if (success >= 0 && verbose >= 3)
#pragma omp critical
        gmp_printf("d=%d %Zd-%Zd [%Zd]: success\n", d, t0current, t1current, T);

      if (success < 0) {
        if (verbose >= 3)
#pragma omp critical
          gmp_printf("d=%d %Zd-%Zd [%Zd]: failure\n", d, t0current, t1current, T);

	/* in case of failure, split the interval under consideration */
	failures ++;

#ifdef AUTOMATIC
	/* If too many failures, reduce T. */
	if (100.0 * (double) failures > FAILURE_UPPERBOUND * (double) good) {
          /* initial_T = floor(9/10*initial_T) */
	  mpz_mul_ui (initial_T, initial_T, 9);
	  mpz_fdiv_q_ui (initial_T, initial_T, 10);
#if DEBUG >= 2
          gmp_printf ("reduce T to %Zd\n", initial_T);
#endif
	  if (mpz_sgn (initial_T)==0)
	    mpz_set_ui (initial_T, 1);

	}
#endif

#ifdef SINBIG
      halve_T:
#endif
	mpz_div_ui (T, T, 2);

	if (mpz_cmp_ui (T, 0)==0)  {
#ifndef NO_OVERPASS_FAILURES
	  mpz_set_ui (ztmp, 0);
	  Check (&ztmp, 1, t0current, t1current, Nin, NN, oneoverM, T, e_in, n, q, yy);
	  total_T += 1.0;
	  good ++;
#ifdef SINBIG
	  mpz_add (t0current, t0current, q);
#else
	  mpz_add_ui (t0current, t0current, 1);
#endif

	  mpz_set_ui (T, 1);

#else
	  fprintf (stderr, "FAILURE at x =");
	  mpz_out_str (stderr, 10, t0current);
	  fprintf (stderr, "\n");
	  exit (1);
#endif
	}
      }
      else { /* success >= 0 */
        mpz_set (t1_done, t1current);
        if (verbose >= 4)
        {
          gmp_printf ("%Zd-%Zd: success, %d candidate(s) found\n",
                      t0current, t1current, Nb);
          if (Nb > 0)
            gmp_printf ("Res[0] is %Zd\n", Res[0]);
        }
	/* update total_T */
	total_T += (2.0*(mpz_get_d (T))+1.0);
	good++;

#ifdef AUTOMATIC
        /* If too few failures, increase T. */
	if (100.0 * (double) failures < FAILURE_LOWERBOUND * (double) good) {
          /* initial_T = ceil(10/9*initial_T), we use ceil so that when
             initial_T<9, it gets really increased */
	  mpz_mul_ui (initial_T, initial_T, 10);
	  mpz_cdiv_q_ui (initial_T, initial_T, 9);
#if DEBUG >= 2
	  gmp_printf ("increase T to %Zd\n", initial_T);
#endif
	}
        /* if initial_T=1, or from time to time, reset it to initial_T0
           to mitigate https://gitlab.inria.fr/zimmerma/bacsel/-/issues/1 */
        if (mpz_cmp (initial_T, initial_T0) < 0)
          smallerT ++;
        else
          smallerT = 0;
        /* smallerT is the number of consecutive times initial_T was smaller
           than initial_T0. The value 100 was determined experimentally
           with the following benchmark:
           make DEFSAL="-DATANH -DBASIS=2 -DAUTOMATIC -DMONOTONOUS"
           ./bacsel -rnd_mode all -prec 128 -n 53 -nn 53 -m 44 -t 16.3 -t0 5629499534213120 -t1 6755399441055744 -d 2 -alpha 2 -v -e_in -21 -nthreads 64
        */
        if (mpz_cmp_ui (initial_T, 1) == 0 || smallerT == 100)
        {
          mpz_set (initial_T, initial_T0);
          smallerT = 0;
        }
#endif /* AUTOMATIC */
#if DEBUG >= 2
	printf ("eps = ");
	mpfr_out_str (stdout, 10, 10, eps, MPFR_RNDN);
	printf ("\n");
	printf ("total_T is %E\n", total_T);
	printf ("failures is %lu\n", failures);
#endif

	Nb = remove_doubles (Res, Nb);
#ifdef EVAL_CHECK
	int cptr_check, st_check = cputime();

	for (cptr_check = 0; cptr_check < NBIT_CHECK; cptr_check++)
#endif
	  /* check potential solutions and print solutions */
	  Check (Res, Nb, tm, t1current, Nin, NN, oneoverM, T, e_in, n, q, yy);
#ifdef EVAL_CHECK
	printf("Check %d\n", (cputime() - st_check) / NBIT_CHECK);
#endif
	for (i = 0; i < Nb; i++) mpz_clear (Res[i]);
	if (Nb > 0) free(Res); /* ?? */

        /* t0current <- t1current */
	mpz_swap (t0current, t1current);

	/* If T is smaller than initial_T, then reset it */
        if (mpz_cmp (T, initial_T) < 0)
          mpz_set (T, initial_T);
      }
  }

#pragma omp atomic update
  Failures += failures;
#pragma omp atomic update
  Good += good;

#ifdef VBSE
#ifdef EVAL_HENSEL
#ifdef EVAL_LLL
  fprintf (stderr, "LLL: %ld ms, Hensel: %ld ms\n",
	   time_4_lll/1000, time_4_Hensel/1000);
#endif
#endif
#endif

  /* Clear everything before leaving */
 clear_and_exit:
  hensel_clear (h);

  mpz_clear (t0current);
  mpz_clear (done);

  free_matrix (&B_lll, npols, ncoefs);

  clear_matrix (&W, npols, ncoefs);

  for (i=0; i<=d; i++) mpfr_clear (P[i]);
  free (P);

  clear_vec (Ptmp, ncoefs);
  clear_vec (PP, d+1);
  free (ok);

  mpfr_clear (NN);
  mpfr_clear (eps);
  mpfr_clear (M);
  mpfr_clear (Mprime);
  mpfr_clear (oneoverM);
  mpfr_clear (yy);

  mpfr_clear (Nin);
  mpfr_clear (tmp);
  mpfr_clear (tmp2);

  for (j = 0; j < nb; j++)
    {
      mpz_clear (Sol[0][j]);
      mpz_clear	(Sol[1][j]);
    }
  free (Sol[0]);
  free (Sol[1]);

  mpz_clear (C); mpz_clear (initial_T); mpz_clear (T);
  mpz_clear (tm); mpz_clear (ztmp); mpz_clear (ztmp2); mpz_clear (Calpha);
  mpz_clear (NZ); mpz_clear (t1current); mpz_clear (initial_T0);

  if (success >= 0)
#pragma omp atomic update
    Successes[d] ++;

  return success;
}

#define MAXTHREADS 512

/* this routine assumes that f(t0*2^(e_in-n)) and f((t1-q)*2^(e_in-n)) have
   same exponent (for d > 0) */
static void
main_aux (mp_prec_t prec, int d, int alpha, int e_in, double n, double nn,
          double m, double t, mpz_t t0, mpz_t t1,
          unsigned long nbr MAYBE_UNUSED, mpz_t q, int nthreads)
{
  mpfr_t eps;
#ifdef USE_PARI
  struct pari_thread pth[MAXTHREADS];
#endif

  if (verbose > 2)
    gmp_printf ("Dealing with t0=%Zd t1=%Zd\n", t0, t1);

  mpfr_init2 (eps, 53);
  mpfr_init (one_half);
  mpfr_set_d (one_half, 0.5, MPFR_RNDN);

#ifdef SINBIG
  /* Compute the elementary step tau. */

  mpfr_init2 (tau, LARGE_PREC);
  mpfr_init2 (pi, LARGE_PREC);
  mpfr_init2 (twopi, LARGE_PREC);

  mpfr_set_z (tau, q, MPFR_RNDN);
  {
    mpfr_t tmp;
    mpfr_init (tmp);
    mpfr_set_ui (tmp, BASIS, MPFR_RNDN);
    mpfr_set_d (eps, n - ((double) e_in), MPFR_RNDN);
    mpfr_pow (tmp, tmp, eps, MPFR_RNDN); /* tmp = B^{p - e} */
    mpfr_div (tau, tau, tmp, MPFR_RNDN);
    mpfr_clear (tmp);
  }

  mpfr_set_prec (pi, LARGE_PREC);
  mpfr_set_prec (eps, LARGE_PREC);
  mpfr_const_pi (pi, MPFR_RNDN);
  mpfr_mul_2ui (twopi, pi, 1, MPFR_RNDN);
  mpfr_div (eps, tau, pi, MPFR_RNDN);
  mpfr_rint (eps, eps, MPFR_RNDN);
  mpfr_mul (eps, eps, pi, MPFR_RNDN);
  mpfr_sub (tau, tau, eps, MPFR_RNDN);

#if DEBUG >= 2
  printf ("tau="); mpfr_out_str (stdout, 10, 10, tau, MPFR_RNDN); printf ("\n");
#endif
#else
  mpz_set_ui (q, 1);
#endif

  omp_set_num_threads (nthreads);
  unsigned long i;

#ifdef USE_PARI
  for (int i = 1; i < nthreads; i++)
    pari_thread_alloc (&pth[i], 8000000, NULL);
#endif

#ifndef SINBIG
  /* since the tasks might take uneven times, we split in more tasks
     than the number of threads */
  unsigned long ntasks = nthreads * nthreads;
#pragma omp parallel for schedule(dynamic,1)
  for (i = 0 ; i < ntasks; i++)
    {
#ifdef USE_PARI
      int this_th = omp_get_thread_num ();
      if (this_th) (void) pari_thread_start(&pth[this_th]);
#endif
      mpz_t ti0, ti1;
      int success;
      mpz_init (ti0);
      mpz_init (ti1);
      /* ti0 = t0 + i*floor((t1-t0)/ntasks)
         ti1 = t0 + (i+1)*floor((t1-t0)/ntasks) */
      mpz_sub (ti1, t1, t0);
      mpz_fdiv_q_ui (ti1, ti1, ntasks);
      mpz_mul_ui (ti0, ti1, i);
      mpz_add (ti0, t0, ti0);
      if (i < ntasks - 1)
        {
          mpz_mul_ui (ti1, ti1, i + 1);
          mpz_add (ti1, t0, ti1);
        }
      else
        mpz_set (ti1, t1);
      /* warning: the default MPFR precision is local to each thread */
      mpfr_set_default_prec (prec);
      /* Warning: for small range [t0,t1) and large number of threads,
         we might get ti0 = ti1. */
      if (mpz_cmp (ti0, ti1) < 0)
      {
      retry:
        success = doit (ti0, ti1, n, nn, m, t, d, alpha, e_in, q, ti0);

#ifdef AUTOMATIC
        int new_alpha = alpha;
        double new_t = t;
        /* try to mitigate the error we get when PP[d] = 0 by reducing d */
        for (int new_d = d - 1; success == -2 && new_d >= 0; new_d--)
        {
          mpz_t right;
          mpz_init_set (right, ti1);
          new_alpha = (new_d <= 1 || new_alpha <= 1) ? 1 : new_alpha - 1; // reduce alpha
          new_t = 1.0 + new_t / 2.0; // also reduce t
          if (new_d == 0) {
            /* For d=0, we don't want to check huge intervals. Usually it
               suffices to check a few points, then we can proceed with the
               initial value of d. */
            mpz_add_ui (right, ti0, 1000000);
            if (mpz_cmp (right, ti1) > 0)
              mpz_set (right, ti1);
          }
          success = doit (ti0, right, n, nn, m, new_t, new_d, new_alpha, e_in, q, ti0);
          if (new_d == 0 && mpz_cmp (right, ti1) < 0) {
            /* It remains to check [right, ti1), since for d=0 the search
               always succeeds. We retry with the original parameters. */
            mpz_set (ti0, right);
            mpz_clear (right);
            goto retry;
          }
          mpz_clear (right);
        }

        /* we no longer can have success == -2 here */
#endif

        if (success < 0)
        {
          gmp_fprintf (stderr, "Program failed, change -d and/or -t: "
                       "[%Zd,%Zd]\n", ti0, ti1);
          exit (1);
        }
      }
      mpz_clear (ti0);
      mpz_clear (ti1);
#ifdef USE_PARI
      if (this_th) pari_thread_close();
#endif
    }
#else
  /* for SINBIG, we loop over the number 'nbr' of arithmetic progressions */
#pragma omp parallel for schedule(dynamic,1)
  for (i = 0 ; i < nbr; i++)
    {
      mpz_t ti0, t1_done;
      mpz_init (ti0);
      mpz_init (t1_done);
      mpz_add_ui (ti0, t0, i);
      mpfr_set_default_prec (prec);
      /* t1_done is not used here */
      doit (ti0, t1, n, nn, m, t, d, alpha, e_in, q, t1_done);
      mpz_clear (ti0);
      mpz_clear (t1_done);
    }
#endif

#ifdef USE_PARI
  for (int i = 1; i < nthreads; i++)
    pari_thread_free (&pth[i]);
#endif

  if (verbose >= 2)
    {
    }

  mpfr_clear (eps);
#ifdef SINBIG
  mpfr_clear (tau);
  mpfr_clear (pi);
  mpfr_clear (twopi);
#endif
  mpfr_clear (one_half);
}

int
main (int argc, char *argv[])
{
  mp_prec_t     prec;
  int           d, alpha, e_in;
  double        n, nn, m, t;
  mpz_t         t0, t1, q;
  unsigned long nthreads = 1; /* number of threads */
  unsigned long nbr;          /* number of loops for SINBIG */
  long st0 = cputime ();
  mpfr_t eps, left, right;

  /***  BEGINNING OF Initialization and argument parsing ***/ 

  mpz_init (t0);
  mpz_init (t1);
  mpz_init (q);
  mpz_init (AverageT);
  mpfr_init2 (arg_y, 128);
  analyse_argv (argc, argv, &near, &prec, &n, &nn, &m, &t, &d, &alpha, &e_in, 
	        t0, t1, &nbr, q, &nthreads, arg_y);

  if (verbose > 0)
    {
      fprintf (stderr, "Welcome to BaCSeL version %s, using %lu thread(s)\n",
               VERSION, nthreads);
      fprintf (stderr, "Compilation flags: -DBASIS=%d", BASIS);
#ifdef AS_TRICK
      fprintf (stderr, " -DAS_TRICK");
#endif
#ifdef REDUCE2
      fprintf (stderr, " -DREDUCE2");
#endif
#ifdef EXP
      fprintf (stderr, " -DEXP");
#endif
#ifdef SIN
      fprintf (stderr, " -DSIN");
#endif
#ifdef COS
      fprintf (stderr, " -DCOS");
#endif
#ifdef TAN
      fprintf (stderr, " -DTAN");
#endif
#ifdef SINBIG
      fprintf (stderr, " -DSINBIG");
#endif
#ifdef EXP2
      fprintf (stderr, " -DEXP2");
#endif
#ifdef LOG2
      fprintf (stderr, " -DLOG2");
#endif
#ifdef LOG
      fprintf (stderr, " -DLOG");
#endif
#ifdef LOG1P
      fprintf (stderr, " -DLOG1P");
#endif
#ifdef LOG2P1
      fprintf (stderr, " -DLOG2P1");
#endif
#ifdef LOG10P1
      fprintf (stderr, " -DLOG10P1");
#endif
#ifdef ONEOVERX
      fprintf (stderr, " -DONEOVERX");
#endif
#ifdef HALL
      fprintf (stderr, " -DHALL");
#endif
#ifdef SQRT
      fprintf (stderr, " -DSQRT");
#endif
#ifdef RSQRT
      fprintf (stderr, " -DRSQRT");
#endif
#ifdef CBRT
      fprintf (stderr, " -DCBRT");
#endif
#ifdef ACOS
      fprintf (stderr, " -DACOS");
#endif
#ifdef EXP10
      fprintf (stderr, " -DEXP10");
#endif
#ifdef ASIN
      fprintf (stderr, " -DASIN");
#endif
#ifdef ASINH
      fprintf (stderr, " -DASINH");
#endif
#ifdef ASINPI
      fprintf (stderr, " -DASINPI");
#endif
#ifdef ACOSH
      fprintf (stderr, " -DACOSH");
#endif
#ifdef ACOSPI
      fprintf (stderr, " -DACOSPI");
#endif
#ifdef ATAN
      fprintf (stderr, " -DATAN");
#endif
#ifdef ATANH
      fprintf (stderr, " -DATANH");
#endif
#ifdef ATANPI
      fprintf (stderr, " -DATANPI");
#endif
#ifdef SINPI
      fprintf (stderr, " -DSINPI");
#endif
#ifdef COSPI
      fprintf (stderr, " -DCOSPI");
#endif
#ifdef TANPI
      fprintf (stderr, " -DTANPI");
#endif
#ifdef SINH
      fprintf (stderr, " -DSINH");
#endif
#ifdef COSH
      fprintf (stderr, " -DCOSH");
#endif
#ifdef TANH
      fprintf (stderr, " -DTANH");
#endif
#ifdef ERF
      fprintf (stderr, " -DERF");
#endif
#ifdef ERFC
      fprintf (stderr, " -DERFC");
#endif
#ifdef EXPM1
      fprintf (stderr, " -DEXPM1");
#endif
#ifdef EXP2M1
      fprintf (stderr, " -DEXP2M1");
#endif
#ifdef EXP10M1
      fprintf (stderr, " -DEXP10M1");
#endif
#ifdef J1
      fprintf (stderr, " -DJ1");
#endif
#ifdef Y0
      fprintf (stderr, " -DY0");
#endif
#ifdef Y1
      fprintf (stderr, " -DY1");
#endif
#ifdef GAMMA
      fprintf (stderr, " -DGAMMA");
#endif
#ifdef LGAMMA
      fprintf (stderr, " -DLGAMMA");
#endif
/* insert new function here */
#ifdef NO_OVERPASS_FAILURES
      fprintf (stderr, " -DNO_OVERPASS_FAILURES");
#endif
#ifdef D3A2
      fprintf (stderr, " -DD3A2");
#endif
#ifdef AUTOMATIC
      fprintf (stderr, " -DAUTOMATIC");
#endif
#ifdef MONOTONOUS
      fprintf (stderr, " -DMONOTONOUS");
#endif
#ifdef VBSE
      fprintf (stderr, " -DVBSE");
#endif
#ifdef DEBUG
      fprintf (stderr, " -DDEBUG=%d", DEBUG);
#endif
      fprintf (stderr, "\n");
      fprintf (stderr, "LLL parameters: delta=%.3f eta=%.3f\n", DELTA, ETA);
#ifdef AUTOMATIC
      fprintf (stderr, "Parameters for AUTOMATIC: FAILURE_LOWERBOUND=%.1f FAILURE_UPPERBOUND=%.1f\n", FAILURE_LOWERBOUND, FAILURE_UPPERBOUND);
#endif
    }

#ifdef USE_PARI
  pari_init (8000000, 0);
#endif

  /* compute left and right bounds */
  mpfr_init2 (eps, 53);
  mpfr_init2 (left, 53);
  mpfr_init2 (right, 53);
  mpfr_set_d (eps, n - ((double) e_in), MPFR_RNDN);
  mpfr_ui_pow (eps, BASIS, eps, MPFR_RNDN); /* eps = B^{p - e} */

  mpfr_set_z (left, t0, MPFR_RNDN);
  mpfr_set_z (right, t1, MPFR_RNDN);
  mpfr_div (left, left, eps, MPFR_RNDN);   /* Min. floating point considered */
  mpfr_div (right, right, eps, MPFR_RNDN); /* Max. floating point considered */

  mpfr_init (one_half);
  mpfr_set_d (one_half, 0.5, MPFR_RNDN);
  
  if (verbose >= 2)
    print_args (left, right, m, n, nn, prec, d, alpha, t, e_in, near, q, arg_y);

  mpz_t global_t0, global_t1;
  mpz_init_set (global_t0, t0);
  mpz_init_set (global_t1, t1);
  while (mpz_cmp (t0, global_t1) < 0)
  {
    /* check that f(t0*2^(e_in-n)) and f((t1-q)*2^(e_in-n)) have same exponent,
       except when d=0 */
    int ok = check_exponents (t0, t1, e_in, (int) n, q);
#ifndef MONOTONOUS
    if (ok == 0 && d > 0)
    {
      fprintf (stderr, "Error, f at t0 and near t1 have different exponents\n");
      exit (1);
    }
#else
    if (ok == 0)
      split_exponents (t0, t1, e_in, (int) n);
    /* split_exponents modifies t1 into the largest value such that
       f(t0*2^(e_in-n)) and f((t1-q)*2^(e_in-n)) have same exponent */
#endif
    main_aux (prec, d, alpha, e_in, n, nn, m, t, t0, t1, nbr, q, nthreads);
    mpz_set (t0, t1);
    mpz_set (t1, global_t1);
  }

  if (verbose >= 2)
  {
    if (d != 0 && Good > 0)
    {
      fprintf (stderr, "Failure rate: %ld%%.\n", (100 * Failures) / Good);
      fprintf (stderr, "Number of calls to LLL: %ld (%ld successful).\n",
               Failures + Good, Good);
    }
#ifdef AUTOMATIC
      if (d != 0)
      {
        mpfr_t tmp;
        mpfr_init2 (tmp, 53);
        /* Compute the average width of successful intervals as the total
           width divided by the number of successful calls to LLL. */
        mpz_sub (AverageT, global_t1, global_t0);
        mpfr_set_z (tmp, AverageT, MPFR_RNDN);
        mpfr_div_si (tmp, tmp, Good, MPFR_RNDN);
        mpfr_log (tmp, tmp, MPFR_RNDN);
        mpfr_set_d (eps, (double) BASIS, MPFR_RNDN);
        mpfr_log (eps, eps, MPFR_RNDN);
        mpfr_div (tmp, tmp, eps, MPFR_RNDN);
        fprintf (stderr, "Final size of subintervals (log[%d](T)): %E.\n",
                 BASIS, mpfr_get_d (tmp, MPFR_RNDN));
        for (int i = 0; i <= d; i++)
          if (Calls[i])
            fprintf (stderr, "Successful calls with d=%d: %lu/%lu\n",
                     i, Successes[i], Calls[i]);
        fflush (stderr);
        mpfr_clear (tmp);
      }
#endif
    fprintf (stderr, "Found %ld solution(s).\n", nsols);
    fprintf (stderr, "Done\n");
  }

  mpz_clear (global_t0);
  mpz_clear (global_t1);

  mpfr_clear (eps);
  mpfr_clear (left);
  mpfr_clear (right);
  mpfr_clear (arg_y);
  mpz_clear (t0);
  mpz_clear (t1);
  mpz_clear (q);
  mpz_clear (AverageT);

  mpfr_free_cache ();
  if (verbose >= 2) /* -v */
    fprintf (stderr, "Total time: %ldms\n", cputime () - st0);
  return 0;
}
