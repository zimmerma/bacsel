/*
  Copyright 2007-2020 Guillaume Hanrot, Vincent Lefèvre, Damien Stehlé 
  and Paul Zimmermann.

  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful, 
  but WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public 
  License along with this program; see the file COPYING.  If not, 
  write to the Free Software Foundation, Inc., 59 Temple Place - 
  Suite 330, Boston, MA 02111-1307, USA.
*/


#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <gmp.h>
#include <mpfr.h>
#include "getexp.h"
#include "io.h"
#include "function.h"

/* Gives the exponent of f(x.2^e) */

long
getexp (const mpz_t x, long e)
{
  mpfr_t y;
  long res;
  mpfr_prec_t prec;
  int inex MAYBE_UNUSED;

  prec = mpz_sizeinbase (x, 2); /* to ensure that y is exact */
  if (prec < MPFR_PREC_MIN)
    prec = MPFR_PREC_MIN;
  mpfr_init2 (y, prec);
  inex = mpfr_set_z (y, x, MPFR_RNDN);
  assert (inex == 0);
#if BASIS == 2
  inex = mpfr_mul_2si (y, y, e, MPFR_RNDN);
  assert (inex == 0);
#else
  {
    mpfr_t pow10;
    mpfr_init2 (pow10, prec + 10);
    mpfr_set_si (pow10, e, MPFR_RNDN);
    mpfr_ui_pow (pow10, BASIS, pow10, MPFR_RNDN);
    mpfr_mul (y, y, pow10, MPFR_RNDN);
    mpfr_clear (pow10);
  }
#endif
#ifdef SINBIG
  mpfr_sin (y, y, MPFR_RNDN);
#else
  /* we round toward zero to get the right exponent */
  EVAL (y, MPFR_RNDZ);
#endif
  res = mpfr_get_exp (y);
  mpfr_clear (y);
  
  return (res);
}
