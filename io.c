/*
  Copyright 2007-2020 Guillaume Hanrot, Vincent Lefèvre, Damien 
  Stehlé and Paul Zimmermann.

  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful, 
  but WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public 
  License along with this program; see the file COPYING.  If not, 
  write to the Free Software Foundation, Inc., 59 Temple Place - 
  Suite 330, Boston, MA 02111-1307, USA.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <assert.h>
#include <gmp.h>
#include <mpfr.h>
#include "mat.h"
#include "io.h"
#include "function.h"
#include "getexp.h"

extern int verbose;

void
print_uni_pol_mpfr(mpfr_t *P, int d) 
{
  for (int k = 0; k <= d; k++) 
    {
      // mpfr_out_str(stdout, 10, 20, P[k], MPFR_RNDN);  
      if (k > 0 && mpfr_sgn (P[k]) > 0)
        printf ("+");
      mpfr_printf ("%Re*x^%d", P[k], k);
    }
}

void
print_uni_pol_mpz (mpz_t *P, int d) 
{
  for (int k = 0; k <= d; k++) 
    {
      mpz_out_str (stdout, 10, P[k]);
      printf(" X^%d + ", k); 
    }
}

/* P[0]...P[d*alpha] contains coefficients of degree 0 in Y
   then coefficients of degree 1 in Y
   ...
   and finally coefficients of degree alpha in Y */
void
print_pol_mpz(mpz_t *P, int d, int alpha) 
{
  int k = 0, xd, yd; 

  for (yd = 0; yd <= alpha; yd++) 
    for (xd = 0; xd <= d*alpha - d*yd; xd++) 
      {
	if ((xd || yd) && mpz_cmp_ui(P[k], 0) > 0)
	  printf("+"); 
	if (mpz_cmp_ui(P[k], 0))
	  gmp_printf("%Zd*X^%d*Y^%d", P[k], xd, yd);  
	k++; 
      }
}

void
print_pol_long (long *P, int d, int alpha)
{
  int k = 0, xd, yd;

  for (yd = 0; yd <= alpha; yd++)
    for (xd = 0; xd <= d*alpha - d*yd; xd++)
      {
	if ((xd || yd) && P[k] > 0)
	  printf("+");
	if (P[k] != 0)
	  printf("%ld*X^%d*Y^%d", P[k], xd, yd);
	k++;
      }
}

/* print polynomial of degree 2 */
void
print_q (long *q)
{
  printf ("(%ld)+Y*(%ld)+Y^2*(%ld)\n", q[0], q[1], q[2]);
}

void
print_args (mpfr_t left, mpfr_t right, double m, 
            double n, double nn, mp_prec_t prec,
            int d, int alpha, double t, int e_in, int near,
            mpz_t q MAYBE_UNUSED, mpfr_t arg_y MAYBE_UNUSED)
{
  fprintf (stderr, "You are looking for %e-bad cases for the \n", m);
  fprintf (stderr, "rounding of %s", NAME);
#ifdef POW
  mpfr_fprintf (stderr, " for y=%Ra", arg_y);
#endif
  fprintf (stderr, ", over [");
  mpfr_out_str (stderr, 10, 5, left, MPFR_RNDN);
  fprintf (stderr, ",");
  mpfr_out_str (stderr, 10, 5, right, MPFR_RNDN);
  fprintf (stderr, "] with:\n");

  if (near==0) fprintf (stderr, " -rounding mode: directed\n");
  if (near==1) fprintf (stderr, " -rounding mode: nearest\n");  
  if (near==2) fprintf (stderr, " -rounding mode: all\n");
  assert (n == (double) (int) n);
  fprintf (stderr," -input scale: %.0f base-%d digits\n", n, BASIS);
  assert (nn == (double) (int) nn);
  fprintf (stderr, " -output scale: %.0f base-%d digits\n", nn, BASIS);
  fprintf (stderr, " -working precision: %lu bits\n", prec);
  if (d == 0)
    fprintf (stderr, " -naive search\n");
  else
  {
    fprintf (stderr, " -degree of approximating polynomials: %d\n", d);
    fprintf (stderr, " -parameter for Coppersmith's technique: %d\n", alpha);
#ifdef SINBIG
  gmp_fprintf (stderr, " -parameter Q for SINBIG: %Zd\n", q);
#endif
  fprintf (stderr, " -size of subintervals: %e\n", 
	   exp(log(BASIS)*(t-((double) n))));
  fprintf (stderr, " -e_in=%d\n\n", e_in);
#ifdef D3A2
  fprintf (stderr, "Dimensions of the lattices: 6x8\n");
#else
  fprintf (stderr, "Dimensions of the lattices: %dx%d\n", 
           ((alpha+1)*(alpha+2))/2,
#ifndef AS_TRICK
           ((alpha+1)*(d*alpha+2))/2);
#else
           ((alpha+1)*(alpha+2))/2);
#endif
#endif
}
}

/* Check that f(t0*2^(e_in-n)) and f((t1-q)*2^(e_in-n)) have same exponent.
   For q > 1, we don't enforce (t1-t0) to be a multiple of q, to allow
   cutting the range to search at any place.
   Return a non-zero value iff the exponents do agree. */
int
check_exponents (const mpz_t t0, const mpz_t t1, int e_in, int n, mpz_t q)
{
  mpfr_exp_t e_out, e_out2;
  mpz_t ztmp;

  /* compute the exponent at t0 */
  e_out = getexp (t0, e_in - n);
  /* compute the exponent at the largest t = t0+i*q < t1 */
  mpz_init (ztmp);
  mpz_sub_ui (ztmp, t1, 1);
  mpz_sub (ztmp, ztmp, t0);
  mpz_fdiv_q (ztmp, ztmp, q); /* floor((t1-1-t0)/q) */
  mpz_mul (ztmp, ztmp, q);
  mpz_add (ztmp, t0, ztmp);
  e_out2 = getexp (ztmp, e_in - n);
  mpz_clear (ztmp);
  return e_out == e_out2;
}

/* modifies t1 into the smallest value in [t0, t1] such that
   f(t0*2^(e_in-n)) and f(t1*2^(e_in-n)) have different exponents */
void
split_exponents (const mpz_t t0_in, mpz_t t1, int e_in, int n)
{
  mpfr_prec_t e_out, e_out2;
  mpz_t t0, t2;

  mpz_init_set (t0, t0_in);
  mpz_init (t2);
  /* compute the exponent at t0 */
  e_out = getexp (t0, e_in - n);
  e_out2 = getexp (t1, e_in - n);
  /* invariant: E(f(t0*2^(e_in-n))) != E(f((t1-q)*2^(e_in-n))) */
  while (1)
  {
    mpz_add (t2, t0, t1);
    mpz_tdiv_q_2exp (t2, t2, 1);
    /* warning: for t0,t1 negative, we have t2=t1 when t1=t0+1 */
    if (mpz_cmp (t2, t0) == 0 || mpz_cmp (t2, t1) == 0)
      break;
    e_out2 = getexp (t2, e_in - n);
    if (e_out2 == e_out)
      mpz_set (t0, t2);
    else
      mpz_set (t1, t2);
  }
  mpz_clear (t0);
  mpz_clear (t2);
}

/* Sanity checks on the values of t0 and t1 */
void
check_t0_t1 (const mpz_t t0, const mpz_t t1, mpfr_t Nin)
{
  mpz_t C, NZ;

  /* One should have t0 <= t1 and both of same sign */
  mpz_init (C);
  mpz_init (NZ);
  if (mpz_sgn (t0) != mpz_sgn (t1) || mpz_cmp(t0, t1) > 0){
    fprintf (stderr, "t0 <= t1 and sgn(t0) = sgn(t1) are required, aborting.\n");
    exit (0);
  }
  /* ... and [t0, t1] \subset [Basis^{n-1}, Basis^n[ = [C, NZ[ */
  mpfr_get_z (NZ, Nin, MPFR_RNDN); /* Nin = BASIS^n */
  mpz_div_ui (C, NZ, BASIS); 

  /* TODO: Considerer l'intervalle en |.| peut permettre de 
     simplifier significativement le code */

  if (mpz_sgn(t0)>0){
    if (mpz_cmp (t1, NZ)>0){
      gmp_fprintf (stderr, "Warning: t1 too large, maximal value %Zd.\n", NZ);
      exit (1);
    }
    if (mpz_cmp (C, t0)>0){
      gmp_fprintf (stderr, "Warning: t0 too small, minimal value %Zd.\n", C);
      exit (1);
    }
  }
  else{
    /* for negative values, we should have |t0| < NZ */
    if (mpz_cmpabs (t0, NZ)>=0){
      mpz_sub_ui (NZ, NZ, 1);
      gmp_fprintf (stderr, "Warning: t0 too small, minimal value -%Zd.\n", NZ);
      exit (1);
    }
    /* for negative values, the maximal tested value is t1-1, thus we should
       have |t1-1| >= C, thus t1 <= -C+1 */
    mpz_neg (NZ, C);
    mpz_add_ui (NZ, NZ, 1);
    if (mpz_cmp (t1, NZ)>0){
      gmp_fprintf (stderr, "Warning: t1 too large, maximal value %Zd.\n", NZ);
      exit (1);
    }
  }

  mpz_clear (C);
  mpz_clear (NZ);
}

static void
usage (void)
{
  fprintf (stderr, 
           "Usage: ./bacsel -rnd_mode r -prec p -n n -nn nn -m m -t t -t0 t0 -t1 t1 -d d -alpha alpha [-e_in e] [-nbr n] [-y y]\n");
  fprintf (stderr, "   optional parameters:\n");
  fprintf (stderr, "   -e_in e      - input exponent (default 0)\n");
  fprintf (stderr, "   -nbr n       - number of loops (for -DSINBIG, default 1)\n");
  fprintf (stderr, "   -nthreads n  - use n threads (requires OpenMP)\n");
  fprintf (stderr, "   -q           - quiet option\n");
  fprintf (stderr, "   -v           - verbose option\n");
  fprintf (stderr, "   -y           - 2nd argument for bivariate functions (in hexadecimal)\n");
  exit (1);
}

  /* INITIALIZING n, nn, m, t, initial_T, t0, t1, tm, d, alpha */
  /* prec = working precision, 
     n = input precision,
     nn= output precision,
     m = quality of the bad cases,
     T=basis^(t-n) = size of the sub-intervals,
     t0= starting fp number is t0/basis^n,
     t1= arriving fp number is t1/basis^n.
     e_in [optional] input exponent  (default = 0)
     nbr [optional] number of successive considered t0's 
     (makes sense only for sinbig: ideally, nbr=Q (define in sinbig.h))

     ---> looking for the solutions of the equation
   | basis^nn . basis^(-e_out).f(t.basis^(e_in)/basis^n) cmod 1| \leq basis^-m,
        | t in [|t0, t1|]

     nn, n, m, t are doubles.
     d, alpha are ints: these are parameters of the method.
     e_out is automatically computed from the input function.
  */

void
analyse_argv (int argc, char **argv, int *near, mp_prec_t *prec, double *n,
              double *nn, double *m, double *t, int *d, int *alpha, int *e_in,
              mpz_t t0, mpz_t t1, unsigned long *nbr, mpz_t q,
              unsigned long *nthreads, mpfr_t arg_y)
{
  int argc0 = argc;
  char **argv0 = argv;
  /* ARGUMENTS CONTROL */

  /* set default values of optional arguments, and invalid values for other
     arguments */
  *e_in = 0;
  *nbr = 1;

  *near = -1;
  *prec = 0;
  *n = 0;
  *nn = 0;
  *m = 0;
  *t = -1;
  mpz_set_si (t0, -1);
  mpz_set_si (t1, -1);
  *d = -1;
  *alpha = 0;
  mpz_set_ui (q, 0);

  /* parse command-line arguments */
  while (argc > 1 && argv[1][0] == '-')
    {
      if (argc > 2 && strcmp (argv[1], "-rnd_mode") == 0)
        {
          if ((strcmp(argv[2], "directed")) == 0)
            *near = 0;
          else if ((strcmp(argv[2], "nearest")) == 0)
            *near = 1;
          else if ((strcmp(argv[2], "all")) == 0)
            *near = 2;
          else
            {
              fprintf (stderr, "Error, invalid -rnd_mode argument\n");
              exit (1);
            }
          argc -= 2;
          argv += 2;
        }
      else if (argc > 2 && strcmp (argv[1], "-prec") == 0)
        {
          *prec = atoi (argv[2]);
          argc -= 2;
          argv += 2;
        }
      else if (argc > 2 && strcmp (argv[1], "-n") == 0)
        {
          *n  = atof (argv[2]);
          argc -= 2;
          argv += 2;
        }
      else if (argc > 2 && strcmp (argv[1], "-nn") == 0)
        {
          *nn  = atof (argv[2]);
          argc -= 2;
          argv += 2;
        }
      else if (argc > 2 && strcmp (argv[1], "-m") == 0)
        {
          *m  = atof (argv[2]);
          argc -= 2;
          argv += 2;
        }
      else if (argc > 2 && strcmp (argv[1], "-t") == 0)
        {
          *t  = atof (argv[2]);
          argc -= 2;
          argv += 2;
        }
      else if (argc > 2 && strcmp (argv[1], "-t0") == 0)
        {
          mpz_set_str (t0, argv[2], 10);
          argc -= 2;
          argv += 2;
        }
      else if (argc > 2 && strcmp (argv[1], "-t1") == 0)
        {
          mpz_set_str (t1, argv[2], 10);
          argc -= 2;
          argv += 2;
        }
      else if (argc > 2 && strcmp (argv[1], "-d") == 0)
        {
          *d = atoi (argv[2]);
          argc -= 2;
          argv += 2;
        }
      else if (argc > 2 && strcmp (argv[1], "-alpha") == 0)
        {
          *alpha = atoi (argv[2]);
          argc -= 2;
          argv += 2;
        }
      else if (argc > 2 && strcmp (argv[1], "-e_in") == 0)
        {
          *e_in = atoi (argv[2]);
          argc -= 2;
          argv += 2;
        }
      else if (argc > 2 && strcmp (argv[1], "-nbr") == 0)
        {
#ifdef SINBIG
          *nbr = strtoul (argv[2], NULL, 10);
          if (*nbr == 0)
            {
              fprintf (stderr, "Error, nbr should be >= 1\n");
              exit (1);
            }
          argc -= 2;
          argv += 2;
#else
          fprintf (stderr, "Error, -nbr is only allowed with -DSINBIG\n");
          exit (1);
#endif
        }
      else if (argc > 2 && strcmp (argv[1], "-Q") == 0)
        {
#ifdef SINBIG
          mpz_set_str (q, argv[2], 10);
          argc -= 2;
          argv += 2;
#else
          fprintf (stderr, "Error, -Q is only allowed with -DSINBIG\n");
          exit (1);
#endif
        }
      else if (strcmp (argv[1], "-q") == 0)
        {
          verbose = 0;
          argc --;
          argv ++;
        }
      else if (strcmp (argv[1], "-v") == 0)
        {
          if (verbose == 0)
            {
              fprintf (stderr, "-v is incompatible with -q\n");
              exit (1);
            }
          verbose ++;
          argc --;
          argv ++;
        }
      else if (strcmp (argv[1], "-y") == 0)
        {
          mpfr_set_str (arg_y, argv[2], 16, MPFR_RNDN);
          argc -= 2;
          argv += 2;
        }
      else if (argc > 2 && strcmp (argv[1], "-nthreads") == 0)
        {
          *nthreads = strtoul (argv[2], NULL, 10);
          assert (*nthreads >= 1);
          argc -= 2;
          argv += 2;
        }
      else
        {
          fprintf (stderr, "Error, invalid argument %s\n", argv[1]);
          usage ();
        }
    }

  /* print command-line */
  if (verbose > 0)
    {
      int i;
      fprintf (stderr, "%s ", argv0[0]);
      for (i = 1; i < argc0; i++)
        fprintf (stderr, "%s ", argv0[i]);
      fprintf (stderr, "\n");
    }

  if (argc != 1)
    {
      fprintf (stderr, "Error, invalid argument %s\n", argv[1]);
      usage ();
    }

  if (*near == -1)
    {
      fprintf (stderr, "Error, missing argument -rnd_mode\n");
      usage ();
    }

  if (*n == 0)
    {
      fprintf (stderr, "Error, missing argument -n\n");
      usage ();
    }

  if (*nn == 0)
    {
      fprintf (stderr, "Error, missing argument -nn\n");
      usage ();
    }

  if (*m == 0)
    {
      fprintf (stderr, "Error, missing argument -m\n");
      usage ();
    }

  if (*t == -1)
    {
      fprintf (stderr, "Error, missing argument -t\n");
      usage ();
    }

  if (mpz_cmp_si (t0, -1) == 0)
    {
      fprintf (stderr, "Error, missing argument -t0\n");
      usage ();
    }

  /* we can either have 0 < t0 < t1, in which case we should have
     B^(n-1) <= t0 < t1 <= B^n, or t0 < t1 < 0, in which case we should have
     -B^n < t0 < t1 < -B^(n-1) */
  /* check that t0 and t1 have same sign */
  if (!((mpz_sgn (t0) == 1 && mpz_sgn (t1) == 1)  ||
        (mpz_sgn (t0) == -1 && mpz_sgn (t1) == -1)))
  {
    fprintf (stderr, "Error, t0 and t1 should have same sign\n");
    usage ();
  }

  /* check that B^(n-1) <= |t0| < B^n */
  mpz_t tmp;
  mpz_init (tmp);
  mpz_set_ui (tmp, BASIS);
  mpz_pow_ui (tmp, tmp, *n - 1);
  if (mpz_cmpabs (t0, tmp) < 0)
  {
    fprintf (stderr, "Error, |t0| should be >= %d^%.0f\n", BASIS, *n - 1);
    usage ();
  }
  mpz_mul_ui (tmp, tmp, BASIS);
  if (mpz_cmpabs (t0, tmp) >= 0)
  {
    fprintf (stderr, "Error, |t0| should be < %d^%.0f\n", BASIS, *n);
    usage ();
  }

  /* check that t0 < t1 and bounds are fulfilled (see above) */
  if (mpz_cmp (t1, t0) <= 0)
  {
    fprintf (stderr, "Error, t1 should be larger than t0\n");
    usage ();
  }
  /* tmp = B^n */
  if (mpz_sgn (t0) == 1)
  {
    if (mpz_cmp (t1, tmp) > 0)
    {
      fprintf (stderr, "Error, t1 should be <= %d^%.0f\n", BASIS, *n);
      usage ();
    }
  }
  else { /* t0, t1 < 0: we already checked that B^(n-1) <= |t0| < B^n,
            and since we also checked t0 < t1, we have |t1| < B^n.
            It remains to check |t1| >= B^(n-1)-1. */
    mpz_div_ui (tmp, tmp, BASIS); /* tmp = B^(n-1) */
    mpz_sub_ui (tmp, tmp, 1);     /* tmp = B^(n-1)-1 */
    if (mpz_cmpabs (t1, tmp) < 0)
    {
      fprintf (stderr, "Error, |t1| should be >= %d^%.0f-1\n", BASIS, *n-1);
      usage ();
    }
  }
  mpz_clear (tmp);

  if (*d == -1)
    {
      fprintf (stderr, "Error, missing argument -d\n");
      usage ();
    }

  if (*alpha == 0)
    {
      fprintf (stderr, "Error, missing argument -alpha\n");
      usage ();
    }

  /* if precision is not given, use nn + m + margin */
  if (*prec == 0)
    {
      *prec = *nn + *m;
#if BASIS == 10
      *prec = (mp_prec_t) (3.32193 * (double) (*prec));
#endif
      *prec += 17; /* margin */
    }
  /* otherwise check the precision is at least nn + m */
  else if (*prec < *nn + *m)
    {
      fprintf (stderr, "Error, precision should be at least nn + m\n");
      exit (1);
    }

/* with -DD3A2, check d=3 and alpha=2 */
#ifdef D3A2
  if (*d != 3 || *alpha != 2)
    {
      fprintf (stderr, "Error: compilation option -DD3A2 can only be used with d=3 and alpha=2\n");
      exit (1);
    }
#endif

#ifdef SINBIG
  if (mpz_cmp_ui (q, 0) == 0)
    {
      fprintf (stderr, "Error, missing argument -Q\n");
      usage ();
    }
#else
  /* initialize q to 1 */
  mpz_set_ui (q, 1);
#endif

  /* with 'all', we decrease m by 1 so that cases found with 'all'
     are the union of those found for 'nearest' and 'directed' for the
     same value of m */
  if (*near == 2)
    *m -= 1.0;
}
