/* Copyright 2007-2023 Guillaume Hanrot, Vincent Lefèvre, Damien Stehlé 
   and Paul Zimmermann.

  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful, 
  but WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public 
  License along with this program; see the file COPYING.  If not, 
  write to the Free Software Foundation, Inc., 59 Temple Place - 
  Suite 330, Boston, MA 02111-1307, USA.
*/

#include <assert.h>

#define NAME "tan"
#define EVAL(y, rnd_mode) mpfr_tan (y, y, rnd_mode);

/* tmp <- 2*(1 + tan(x)^2)*tan(x), where one can modify x */
static inline void
bound_d2 (mpfr_t tmp, mpfr_t x)
{
  mpfr_tan (x, x, MPFR_RNDN);              /* tan(x) */
  mpfr_mul (tmp, x, x, MPFR_RNDN);         /* tan(x)^2 */
  mpfr_add_ui (tmp, tmp, 1, MPFR_RNDN);    /* 1+tan(x)^2 */
  mpfr_mul (tmp, tmp, x, MPFR_RNDN);       /* (1 + tan(x)^2)*tan(x) */
  mpfr_mul_2exp (tmp, tmp, 1, MPFR_RNDN);
}

/* tmp <- 2*(3*tan(x)^2+1)*(1+tan(x)^2), where one can modify x */
static inline void
bound_d3 (mpfr_t tmp, mpfr_t x)
{
  mpfr_tan (x, x, MPFR_RNDN);              /* tan(x) */
  mpfr_mul (x, x, x, MPFR_RNDN);           /* tan(x)^2 */
  mpfr_add_ui (tmp, x, 1, MPFR_RNDN);      /* 1+tan(x)^2 */
  mpfr_mul_ui (x, x, 3, MPFR_RNDN);        /* 3*tan(x)^2 */
  mpfr_add_ui (x, x, 1, MPFR_RNDN);        /* 3*tan(x)^2+1 */
  mpfr_mul (tmp, tmp, x, MPFR_RNDN);       /* (3*tan(x)^2+1)*(1+tan(x)^2) */
  mpfr_mul_2exp (tmp, tmp, 1, MPFR_RNDN);
}

/* BNDDIFF(tmp, t0, t1, N, d) returns a bound for the (d+1)-th
   derivative of tan(x) for t0/N <= x < t1/N. We only implement
   it for d=1 and d=2 so far, and for 0 < t0/N < t1/N < pi/2.
   For d=1 the 2nd derivative of tan(x) is 2*(1 + tan(x)^2)*tan(x),
   and is maximal at t1/N.
   For d=2 the 3rd derivative of tan(x) is 2*(3*tan(x)^2+1)*(1+tan(x)^2),
   and is maximal at t1/N too. */
#define BNDDIFF(tmp, t0, t1, N, d) {                            \
    assert (d == 1 || d == 2);                                  \
    mpfr_t t;                                                   \
    mpfr_init2 (t, mpfr_get_prec (tmp));                        \
    mpfr_set_z (t, t1, MPFR_RNDU);                              \
    mpfr_div (t, t, N, MPFR_RNDU);                              \
    assert (mpfr_cmp_ui (t, 0) > 0);                            \
    assert (mpfr_cmp_d (t, 0x1.921fb54442d18p+0) < 0);          \
    if (d == 1)                                                 \
      bound_d2 (tmp, t);                                        \
    else                                                        \
      bound_d3 (tmp, t);                                        \
    mpfr_abs (tmp, tmp, MPFR_RNDU);                             \
    mpfr_clear (t);                                             \
  }

/* Put in P[0]..P[d] the Taylor expansion of NN*f(tm/Nin+x) at x=0,
   with the coefficient of degree i scaled by (T/Nin)^i. */
#define TAYLOR(tmp, tm, Nin, P, NN, d) {                        \
    assert (d == 1 || d == 2);                                  \
    mpfr_t tmp2;                                                \
    mpfr_init2 (tmp2, mpfr_get_prec (tmp));                     \
    mpfr_set_z (tmp2, T, MPFR_RNDN);                            \
    mpfr_div (tmp2, tmp2, Nin, MPFR_RNDN);                      \
    /* tmp2 = T/Nin */                                          \
    mpfr_set_z (tmp, tm, MPFR_RNDN);                            \
    mpfr_div (tmp, tmp, Nin, MPFR_RNDN);			\
    mpfr_tan (tmp, tmp, MPFR_RNDN);                             \
    /* tmp = tan(tm/Nin) */                                     \
    mpfr_mul (P[0], tmp, NN, MPFR_RNDN);                        \
    /* the 1st derivative is 1+tan(x)^2 */                      \
    mpfr_mul (P[1], tmp, tmp, MPFR_RNDN);                       \
    mpfr_add_ui (P[1], P[1], 1, MPFR_RNDN);                     \
    /* multiply P[1] by tmp2=T/Nin */                           \
    mpfr_mul (P[1], P[1], tmp2, MPFR_RNDN);                     \
    mpfr_mul (P[1], P[1], NN, MPFR_RNDN);                       \
    if (d == 2) {                                               \
      /* the 2nd derivative is 2*(tan(x)^2+1)*tan(x) */         \
      mpfr_mul (P[2], P[1], tmp, MPFR_RNDN);                    \
      /* multiply by 2 and divide by 2!: do nothing */          \
      /* multiply by tmp2=T/Nin */                              \
      mpfr_mul (P[2], P[2], tmp2, MPFR_RNDN);                   \
      /* P[1] was already scaled by NN */                       \
    }                                                           \
    mpfr_clear (tmp2);                                          \
  }

