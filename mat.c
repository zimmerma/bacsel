/*
  Copyright 2005-2021 Damien Stehl�, Paul Zimmermann.

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; see the file COPYING.  If not, write to the Free
  Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
  02111-1307, USA.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <float.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <ctype.h>
#include <limits.h>
#include <gmp.h>

#include "mat.h"

/****************************** vectors **************************************/

mpz_t*
init_vec (int n)
{
  mpz_t *a;
  int i;

  a = (mpz_t*) malloc (n * sizeof (mpz_t));
  for (i=0; i<n; i++)
    mpz_init (a[i]);
  return a;
}

void
clear_vec (mpz_t *a, int n)
{
  int i;

  for (i=0; i<n; i++)
    mpz_clear (a[i]);
  free (a);
}

void
Norm1 (mpz_t tmp, mpz_t *V, int d)
{
  int i;

  mpz_set_ui (tmp, 0);

  for (i = 0; i < d; i++)
    if (mpz_sgn(V[i]) >= 0)
      mpz_add (tmp, tmp, V[i]);
    else
      mpz_sub (tmp, tmp, V[i]);
}

/****************************** mat_ZZ matrices ******************************/

void
init_matrix (mat_ZZ *B, int d, int n)
{
  int i;

  B->NumCols=d;
  B->NumRows=n;
  B->coeff = (mpz_t**) malloc ((d + 1) * sizeof (mpz_t*));
  for (i=0; i <= d; i++)
    B->coeff[i] = init_vec (n + 1);
}


void
clear_matrix (mat_ZZ *B, int d, int n)
{
  int i;
  for (i = 0; i <= d; i++)
    clear_vec (B->coeff[i], n + 1);
  free (B->coeff);
}

/***************************** matrices of mpz_t's ***************************/

/* allocate a mpz_t matrix of d rows and n columns,
   and initializes its entries */
void
allocate_matrix (mpz_t ***B, int d, int n)
{
  int i, j;

  *B = (mpz_t**) malloc (d * sizeof (mpz_t *));

  for (i = 0; i < d; i++)
  {
    (*B)[i] = (mpz_t*) malloc (n * sizeof (mpz_t));
    for (j = 0; j < n; j++)
      mpz_init ((*B)[i][j]);
  }
}

/* frees a mpz_t matrix of d rows and n columns,
   after clearing its entries */
void
free_matrix (mpz_t ***B, int d, int n)
{
  int i, j;

  for (i = 0; i < d; i++)
  {
    for (j = 0; j < n; j++)
      mpz_clear ((*B)[i][j]);
    free ((*B)[i]);
  }

  free (*B);
}

void
print_mat (mpz_t **B, int d, int n)
{
  int i, j;

  printf("[");
  for (i=0;i<d;i++)
    {
      printf("[");
      for (j=0;j<n;j++)
	{
	  mpz_out_str (stdout, 10, B[i][j]);
	  if (j < n) printf(" ");
	}
      printf("]\n");
    }
  printf("]\n");
}
