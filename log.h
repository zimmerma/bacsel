/*
Copyright 2007-2023 Guillaume Hanrot, Vincent Lefèvre, Damien Stehlé
and Paul Zimmermann.

  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful, 

  but WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public 
  License along with this program; see the file COPYING.  If not, 

  write to the Free Software Foundation, Inc., 59 Temple Place - 
  Suite 330, Boston, MA 02111-1307, USA.
*/

#define NAME "log"
#define EVAL(y, rnd_mode) mpfr_log(y, y, rnd_mode); 
/* BNDDIFF(tmp,t0,t1,N,d) puts in tmp an upper bound for the (d+1)-th
   derivative of log(x) over [t0/N,t1/N] (d >= 1).
   The (d+1)-th derivative of log(x) is (up to sign) d!/x^(d+1). */
#define BNDDIFF(tmp, t0, t1, N, d)              \
  mpfr_div_z (tmp, Nin, t0, MPFR_RNDU);		\
  mpfr_pow_ui (tmp, tmp, d+1, MPFR_RNDU);	\
  for (i=2; i<=d; i++)				\
    mpfr_mul_ui (tmp, tmp, i, MPFR_RNDU);

#define TAYLOR(tmp, tm, Nin, P, NN, d)		\
  { long i;					\
    mpfr_set_z (tmp, tm, MPFR_RNDN);		\
    mpfr_div (tmp, tmp, Nin, MPFR_RNDN);        \
    mpfr_log (P[0], tmp, MPFR_RNDN);		\
    mpfr_mul (P[0], P[0], NN, MPFR_RNDN);	\
    mpfr_ui_div (tmp, 1, tmp, MPFR_RNDN);	\
    mpfr_set (P[1], tmp, MPFR_RNDN);		\
    mpfr_mul_z (P[1], P[1], T, MPFR_RNDN);	\
    mpfr_div (P[1], P[1], Nin, MPFR_RNDN);	\
    mpfr_mul (P[1], P[1], NN, MPFR_RNDN);	\
    for (i=2; i<=d; i++) {			\
      mpfr_neg (P[i], P[i-1], MPFR_RNDN);	\
      mpfr_mul_z (P[i], P[i], T, MPFR_RNDN);	\
      mpfr_div (P[i], P[i], Nin, MPFR_RNDN);	\
      mpfr_mul (P[i], P[i], tmp, MPFR_RNDN);	\
      mpfr_mul_ui (P[i], P[i], i-1, MPFR_RNDN);	\
      mpfr_div_ui (P[i], P[i], i, MPFR_RNDN);	\
    }                                           \
  }
