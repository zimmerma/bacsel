#!/bin/bash
# worst cases of cbrt in binary64
make clean
make GMP=$GMP MPFR=$MPFR FPLLL=$FPLLL DEFSAL="-DCBRT -DBASIS=2 $EXTRA"
./bacsel -v -rnd_mode all -prec 128 -n 53 -nn 53 -m 45 -t 19 -t0 6756881204772864 -t1 6757430960586752 -d 2 -alpha 2 -e_in 0
# *** x=6756962635986944*2^-53, distance is: 0
# *** x=6757132843702692*2^-53, distance is: 0
# *** x=6757156744606965*2^-53, distance is: 3.437474317e-15
# *** x=6757303054276768*2^-53, distance is: 0
# tomate: user	1m9.046s  with 3540cc6 and EXTRA="-DPRERED_TRICK"
# tomate: user	0m57.590s
# tomate: user  1m35.169s EXTRA="-DAS_TRICK" (Failure rate: 17%)
# tomate: user  1m37.722s EXTRA="-DAS_TRICK -DREDUCE2" (Failure rate: 0%)
