/* Copyright 2007-2023 Guillaume Hanrot, Vincent Lefèvre, Damien Stehlé 
   and Paul Zimmermann.

  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful, 
  but WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public 
  License along with this program; see the file COPYING.  If not, 
  write to the Free Software Foundation, Inc., 59 Temple Place - 
  Suite 330, Boston, MA 02111-1307, USA.
*/

#define NAME "cospi"

#define EVAL(y, rnd_mode) mpfr_cospi(y, y, rnd_mode); 

/* BNDDIFF(tmp, t0, t1, N, d) returns a bound for the (d+1)-th derivative of
   cospi(x) for t0/N <= x < t1/N. Since the n-th derivative of cos(pi*x) is
   +/-pi^n*cos(pi*x) or +/-pi^n*sin(pi*x), we use pi^(d+1) as bound. */
#define BNDDIFF(tmp, t0, t1, N, d)              \
  {                                             \
    mpfr_const_pi (tmp, MPFR_RNDU);             \
    mpfr_pow_ui (tmp, tmp, d+1, MPFR_RNDU);     \
  }

/* Put in P[0]..P[d] the Taylor expansion of NN*f(tm/Nin+x) at x=0,
   with the coefficient of degree i scaled by (T/Nin)^i.
   The n-th derivative of cospi(x) is:
   * (-1)^(n/2)*pi^n*cos(pi*x) for n even
   * (-1)^((n+1)/2)*pi^n*sin(pi*x) for n odd */
#define TAYLOR(tmp, tm, Nin, P, NN, d)                          \
  { long i, j;                                                  \
    mpfr_t scale, u;                                            \
    mpfr_init2 (scale, mpfr_get_prec (tmp));                    \
    mpfr_init2 (u, mpfr_get_prec (tmp));                        \
    /* scale <- pi*T/Nin */                                     \
    mpfr_const_pi (scale, MPFR_RNDN);                           \
    mpfr_mul_z (scale, scale, T, MPFR_RNDN);                    \
    mpfr_div (scale, scale, Nin, MPFR_RNDN);                    \
    /* tmp <- tm/Nin */                                         \
    mpfr_set_z (tmp, tm, MPFR_RNDN);                            \
    mpfr_div (tmp, tmp, Nin, MPFR_RNDN);                        \
    mpfr_cospi (P[0], tmp, MPFR_RNDN);                          \
    mpfr_mul (P[0], P[0], NN, MPFR_RNDN);                       \
    mpfr_set (u, NN, MPFR_RNDN);                                \
    for (i=1; i<=d; i++) {                                      \
      if (i%2==0)                                               \
	mpfr_cospi (P[i], tmp, MPFR_RNDN);                      \
      else                                                      \
	mpfr_sinpi (P[i], tmp, MPFR_RNDN);                      \
      /* divide by i! */                                        \
      for (j=1; j<=i; j++)                                      \
        mpfr_div_ui (P[i], P[i], j, MPFR_RNDN);                 \
      /* multiply by -1 if needed */                            \
      if ((i-1)%4<2)                                            \
        mpfr_neg (P[i], P[i], MPFR_RNDN);                       \
      /* at each loop we multiply u by scale, so that */        \
      /* u=NN*(pi*T/Nin)^i */                                   \
      mpfr_mul (u, u, scale, MPFR_RNDN);                        \
      mpfr_mul (P[i], P[i], u, MPFR_RNDN);                      \
    }                                                           \
    mpfr_clear (scale);                                         \
    mpfr_clear (u);                                             \
  }

