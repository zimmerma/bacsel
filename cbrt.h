/*
  Copyright 2021-2023 Paul Zimmermann.

  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful, 
  but WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public 
  License along with this program; see the file COPYING.  If not, 
  write to the Free Software Foundation, Inc., 59 Temple Place - 
  Suite 330, Boston, MA 02111-1307, USA.
*/

#define NAME "cbrt"
/* function to evaluate the cubic root */
#define EVAL(y, rnd_mode) mpfr_cbrt(y, y, rnd_mode);
/* BNDDIFF(tmp,t0,t1,N,d) puts in tmp an upper bound for the (d+1)-th
   derivative of x^(1/3) over [t0/N,t1/N] (d >= 1).
   d=1: the second derivative of x^(1/3) is -2/9/x^(5/3).
   d=2: the third derivative of x^(1/3) is 10/27/x^(8/3). */
#define BNDDIFF(tmp, t0, t1, N, d)                              \
  assert (mpz_cmp_ui (t0, 0) > 0);                              \
  assert (d <= 2);                                              \
  if (d == 1) {                                                 \
  mpfr_div_z (tmp, N, t0, MPFR_RNDU);                           \
  mpfr_pow_ui (tmp, tmp, 5, MPFR_RNDU);                         \
  mpfr_cbrt (tmp, tmp, MPFR_RNDU);                              \
  mpfr_mul_ui (tmp, tmp, 2, MPFR_RNDU);                         \
  mpfr_div_ui (tmp, tmp, 9, MPFR_RNDU);                         \
  } else if (d == 2) {                                          \
  mpfr_div_z (tmp, N, t0, MPFR_RNDU);                           \
  mpfr_pow_ui (tmp, tmp, 8, MPFR_RNDU);                         \
  mpfr_cbrt (tmp, tmp, MPFR_RNDU);                              \
  mpfr_mul_ui (tmp, tmp, 10, MPFR_RNDU);                        \
  mpfr_div_ui (tmp, tmp, 27, MPFR_RNDU); }                      \

#define TAYLOR(tmp, tm, Nin, P, NN, d)			\
  { long i;						\
    mpfr_set_z (tmp, tm, MPFR_RNDN);			\
    mpfr_div (tmp, tmp, Nin, MPFR_RNDN);                \
    mpfr_cbrt (P[0], tmp, MPFR_RNDN);			\
    mpfr_ui_div (tmp, 1, tmp, MPFR_RNDN);		\
    /* tmp = Nin/tm */                                  \
    mpfr_mul (P[0], P[0], NN, MPFR_RNDN);		\
    /* P[0] = NN*cbrt(tm/Nin) */                        \
    /* expansion of cbrt(x+h) wrt h */                  \
    for (i=1; i<=d; i++) {				\
      /* Note: for i=1, the multiplication by -1 is */  \
      /* compensated by that by 3*i-4 below. */         \
      mpfr_neg (P[i], P[i-1], MPFR_RNDN);               \
      mpfr_mul_z (P[i], P[i], T, MPFR_RNDN);		\
      mpfr_div (P[i], P[i], Nin, MPFR_RNDN);		\
      mpfr_mul (P[i], P[i], tmp, MPFR_RNDN);		\
      mpfr_div_ui (P[i], P[i], i, MPFR_RNDN);		\
      mpfr_div_ui (P[i], P[i], 3, MPFR_RNDN);		\
      mpfr_mul_si (P[i], P[i], 3*i-4, MPFR_RNDN);	\
    }                                                   \
  }
