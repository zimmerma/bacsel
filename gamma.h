/*
  Copyright 2024 Paul Zimmermann.

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; see the file COPYING.  If not, write to
  the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
  Boston, MA 02111-1307, USA.
*/

#include <assert.h>
#ifdef USE_PARI
#include <pari/pari.h>
#endif

#define NAME "gamma"

#define EVAL(y, rnd_mode) mpfr_gamma (y, y, rnd_mode);

#ifdef NEED_TABLES
#ifdef USE_PARI
// put in y the value of psi(2,x) using Pari/GP
static void
psi2 (mpfr_t y, mpfr_t x)
{
  char *s, *t;
  mpfr_exp_t e;
  int n, bias;
  pari_sp av = avma;

  s = mpfr_get_str (NULL, &e, 10, 0, x, MPFR_RNDN);
  n = strlen (s);
  bias = n - (s[0] == '-');
  t = (char*) malloc (n + 10);
  strcpy (t, s);
  n += sprintf (t + strlen(s), "*10.^%ld", e - bias);
  t[n] = '\0';
  mpfr_free_str (s);
  GEN g = gp_read_str (t);
  g = gpsi_der (g, 2, 128);
  s = pari_sprintf ("%Ps", g);
  mpfr_set_str (y, s, 10, MPFR_RNDN);
  free (s);
  free (t);
  set_avma (av); // free all memory created by Pari in this function
}

// put in y the value of |gamma'''(x)| using Pari/GP
// gamma'''(x) = gamma(x)*(psi(x)^3 + 3*psi(1, x)*psi(x) + psi(2, x))
static void
bound_gamma3_pari (mpfr_t y, mpfr_t x)
{
  mpfr_t p0, p1, p2;
  mpfr_init2 (p0, mpfr_get_prec (y));
  mpfr_init2 (p1, mpfr_get_prec (y));
  mpfr_init2 (p2, mpfr_get_prec (y));
  mpfr_digamma (p0, x, MPFR_RNDN);    // p0 = psi(x)
  mpfr_sqr (p2, p0, MPFR_RNDN);       // p2 = psi(x)^2
  mpfr_trigamma (p1, x, MPFR_RNDN);   // p1 = psi(1,x)
  mpfr_mul_ui (p1, p1, 3, MPFR_RNDN); // p1 = 3*psi(1, x)
  mpfr_add (p1, p1, p2, MPFR_RNDN);   // p1 = psi(x)^2+3*psi(1, x)
  mpfr_mul (p1, p1, p0, MPFR_RNDN);   // p1 = psi(x)^3+3*psi(1, x)*psi(x)
  psi2 (p2, x);                       // p2 = psi(2,x)
  mpfr_add (p1, p1, p2, MPFR_RNDN);   // p1 = psi(x)^3 + 3*psi(1, x)*psi(x) + psi(2, x)
  mpfr_gamma (p0, x, MPFR_RNDN);
  mpfr_mul (y, p0, p1, MPFR_RNDN);
  mpfr_abs (y, y, MPFR_RNDN);
  mpfr_clear (p0);
  mpfr_clear (p1);
  mpfr_clear (p2);
}
#endif

/* For 1 <= k <= 184, T2[k] is the binary64 number where |gamma''(x)| is minimal
   in the range (-k,-k+1).
   T2[0] is not used. */
static const double T2[] = {0,
   -0x1.002d390e51925p-1, -0x1.8adf05e957a5fp+0, -0x1.488375bd3db39p+1, 
   -0x1.caa57771fe077p+1, -0x1.2621faabb03b3p+2, -0x1.66c808252af18p+2, 
   -0x1.a751f19253d11p+2, -0x1.e7c77738f609cp+2, -0x1.1416c8c62f0ffp+3, 
   -0x1.3443d006982ep+3, -0x1.546c036125c13p+3, -0x1.7490435ad5147p+3, 
   -0x1.94b13962a9974p+3, -0x1.b4cf685b8d7b4p+3, -0x1.d4eb37636d158p+3, 
   -0x1.f504f91a78849p+3, -0x1.0a8e7858b10d4p+4, -0x1.1a99aabf4b285p+4, 
   -0x1.2aa42acb6ba0dp+4, -0x1.3aae0be1281c9p+4, -0x1.4ab75e75d5b05p+4, 
   -0x1.5ac0309fc0444p+4, -0x1.6ac88e824a01cp+4, -0x1.7ad082a88fac7p+4, 
   -0x1.8ad81645eb3dp+4, -0x1.9adf516e33c43p+4, -0x1.aae63b42afc8fp+4, 
   -0x1.baecda15de5e7p+4, -0x1.caf33389ef5ecp+4, -0x1.daf94ca74acb5p+4, 
   -0x1.eaff29f2ee633p+4, -0x1.fb04cf800b199p+4, -0x1.0585207e5892p+5, 
   -0x1.0d87c0df57e61p+5, -0x1.158a4a67c2b63p+5, -0x1.1d8cbe796ab25p+5, 
   -0x1.258f1e5862eb5p+5, -0x1.2d916b2d69d61p+5, -0x1.3593a608e7617p+5, 
   -0x1.3d95cfe611711p+5, -0x1.4597e9ac42a1bp+5, -0x1.4d99f431833d4p+5, 
   -0x1.559bf03c222e8p+5, -0x1.5d9dde840650dp+5, -0x1.659fbfb3c1a89p+5, 
   -0x1.6da1946b16508p+5, -0x1.75a35d3df0287p+5, -0x1.7da51ab6fd5ddp+5, 
   -0x1.85a6cd57de5b5p+5, -0x1.8da875999b67dp+5, -0x1.95aa13ee48bb3p+5, 
   -0x1.9daba8c08831bp+5, -0x1.a5ad3474cf544p+5, -0x1.adaeb7697efa3p+5, 
   -0x1.b5b031f7ea829p+5, -0x1.bdb1a473a609fp+5, -0x1.c5b30f2c432d5p+5, 
   -0x1.cdb4726c88cedp+5, -0x1.d5b5ce7ba1bb1p+5, -0x1.ddb7239c856c7p+5, 
   -0x1.e5b8720e96d6bp+5, -0x1.edb9ba0e74831p+5, -0x1.f5bafbd5834c3p+5, 
   -0x1.fdbc379a2a729p+5, -0x1.02deb6c824ed1p+6, -0x1.06df4ef469b23p+6, 
   -0x1.0adfe4699976p+6, -0x1.0ee0773e1fa9cp+6, -0x1.12e10786ee53fp+6, 
   -0x1.16e19558dbb83p+6, -0x1.1ae220c742924p+6, -0x1.1ee2a9e4804e5p+6, 
   -0x1.22e330c29edc7p+6, -0x1.26e3b5729432bp+6, -0x1.2ae43804c3445p+6, 
   -0x1.2ee4b8888ab4fp+6, -0x1.32e5370d3b00fp+6, -0x1.36e5b3a0e54f5p+6, 
   -0x1.3ae62e517d1c3p+6, -0x1.3ee6a72c30c2dp+6, -0x1.42e71e3db43ddp+6, 
   -0x1.46e7939259035p+6, -0x1.4ae80735c33cfp+6, -0x1.4ee879333769fp+6, 
   -0x1.52e8e995b5eccp+6, -0x1.56e95867b7cbdp+6, -0x1.5ae9c5b3512cdp+6, 
   -0x1.5eea31821fc8cp+6, -0x1.62ea9bdda4cf8p+6, -0x1.66eb04ced9187p+6, 
   -0x1.6aeb6c5e9a0a7p+6, -0x1.6eebd295100b7p+6, -0x1.72ec377ab9701p+6, 
   -0x1.76ec9b173730dp+6, -0x1.7aecfd7248c38p+6, -0x1.7eed5e931082cp+6, 
   -0x1.82edbe80d6247p+6, -0x1.86ee1d426779fp+6, -0x1.8aee7ade8db7cp+6, 
   -0x1.8eeed75b7e2c1p+6, -0x1.92ef32bf87444p+6, -0x1.96ef8d10f066fp+6, 
   -0x1.9aefe65543804p+6, -0x1.9ef03e926449p+6, -0x1.a2f095cd9dd1fp+6, 
   -0x1.a6f0ec0c7d204p+6, -0x1.aaf141542d2dbp+6, -0x1.aef195a989b31p+6, 
   -0x1.b2f1e9119a8dbp+6, -0x1.b6f23b90f4007p+6, -0x1.baf28d2c5e7ap+6, 
   -0x1.bef2dde837a35p+6, -0x1.c2f32dc8e406p+6, -0x1.c6f37cd29c291p+6, 
   -0x1.caf3cb095d6d8p+6, -0x1.cef4187151309p+6, -0x1.d2f4650e2d83fp+6, 
   -0x1.d6f4b0e3e60dbp+6, -0x1.daf4fbf60d4bp+6, -0x1.def5464833453p+6, 
   -0x1.e2f58fddebe08p+6, -0x1.e6f5d8ba980b3p+6, -0x1.eaf620e16bcc8p+6, 
   -0x1.eef66855a1c4fp+6, -0x1.f2f6af1a8833fp+6, -0x1.f6f6f532fb91p+6, 
   -0x1.faf73aa20f72p+6, -0x1.fef77f6aafecbp+6, -0x1.017be1c7d52fp+7, 
   -0x1.037c0389dea09p+7, -0x1.057c24fcd0574p+7, -0x1.077c4621fecf8p+7, 
   -0x1.097c66faacd7dp+7, -0x1.0b7c878816f9fp+7, -0x1.0d7ca7cb91bfcp+7, 
   -0x1.0f7cc7c62e139p+7, -0x1.117ce779311e9p+7, -0x1.137d06e5a7acfp+7, 
   -0x1.157d260cb4813p+7, -0x1.177d44ef83031p+7, -0x1.197d638f0fa55p+7, 
   -0x1.1b7d81ec6933fp+7, -0x1.1d7da00897297p+7, -0x1.1f7dbde4940d1p+7, 
   -0x1.217ddb815744cp+7, -0x1.237df8dfd9b2p+7, -0x1.257e160107bbdp+7, 
   -0x1.277e32e5ca7fdp+7, -0x1.297e4f8f0fe63p+7, -0x1.2b7e6bfda2087p+7, 
   -0x1.2d7e88326783p+7, -0x1.2f7ea42e3606fp+7, -0x1.317ebff1dcfa1p+7, 
   -0x1.337edb7e383a3p+7, -0x1.357ef6d3fa6bfp+7, -0x1.377f11f3edd1bp+7, 
   -0x1.397f2cdedb597p+7, -0x1.3b7f4795859c9p+7, -0x1.3d7f621890695p+7, 
   -0x1.3f7f7c68c5dc3p+7, -0x1.417f9686cb0f3p+7, -0x1.437fb07349ca7p+7, 
   -0x1.457fca2f011cfp+7, -0x1.477fe3ba89e33p+7, -0x1.497ffd168aa2dp+7, 
   -0x1.4b8016439c064p+7, -0x1.4d802f42684f3p+7, -0x1.4f80481392931p+7, 
   -0x1.518060b7a42dfp+7, -0x1.5380792f31b9bp+7, -0x1.5580917ad2031p+7, 
   -0x1.5780a99b18933p+7, -0x1.5980c19080be1p+7, -0x1.5b80d95bb8181p+7, 
   -0x1.5d80f0fd2ac21p+7, -0x1.5f8108755f354p+7, -0x1.61811fc4e2e63p+7, 
   -0x1.638136ec32561p+7, -0x1.65814debce569p+7, -0x1.678164c42b569p+7, 
   -0x1.69817b75ce5d9p+7, -0x1.6b81920120ea3p+7, -0x1.6d81a866ab665p+7, 
   -0x1.6f81bea6d8b28p+7, };

/* for 1 <= e <= 1032, Gamma3[e] is the smallest x such that gamma'''(x) >= 2^e
   (and we set Gamma3[0] = 1 arbitrarily) */
static double Gamma3[1033] = {
0x1p+0, 0x1.54bdd4bba162ep+1, 0x1.8bd32e330cecep+1,
0x1.c2a0294dd3935p+1, 0x1.f7d89990ed70fp+1, 0x1.15af360e2a8f1p+2,
0x1.2ea8a1b444046p+2, 0x1.46ec9ed9c127bp+2, 0x1.5e8dd64aa65ap+2,
0x1.759c94a8fd1f8p+2, 0x1.8c26e48a0954p+2, 0x1.a238ddfbe83acp+2,
0x1.b7dcf689b5b3bp+2, 0x1.cd1c463371d5bp+2, 0x1.e1fec066a00d5p+2,
0x1.f68b62863aa5ep+2, 0x1.05642cf32adadp+3, 0x1.0f5d917492904p+3,
0x1.1934514e2055fp+3, 0x1.22ea9df678692p+3, 0x1.2c8270a7a7668p+3,
0x1.35fd91c2bb38bp+3, 0x1.3f5d9f07e21c5p+3, 0x1.48a410d975292p+3,
0x1.51d23eb55af64p+3, 0x1.5ae963079be48p+3, 0x1.63ea9e724644ap+3,
0x1.6cd6faa170c49p+3, 0x1.75af6cbdf3163p+3, 0x1.7e74d78e10fd1p+3,
0x1.87280d50a3055p+3, 0x1.8fc9d15d1d5e6p+3, 0x1.985ad9911465fp+3,
0x1.a0dbcf92716a1p+3, 0x1.a94d51ec60052p+3, 0x1.b1aff50c07d51p+3,
0x1.ba0444215cbe8p+3, 0x1.c24ac1e7a91e1p+3, 0x1.ca83e958eab79p+3,
0x1.d2b02e4ea798ap+3, 0x1.dacffe127f605p+3, 0x1.e2e3bfe06c08ap+3,
0x1.eaebd55c60b96p+3, 0x1.f2e89afcbb25bp+3, 0x1.fada686acaafdp+3,
0x1.0160c86d434c4p+4, 0x1.054f31ad34543p+4, 0x1.0938958e21837p+4,
0x1.0d1d17dbe6c8dp+4, 0x1.10fcdab3c821p+4, 0x1.14d7fea05e46cp+4,
0x1.18aea2b33b50ap+4, 0x1.1c80e49c80498p+4, 0x1.204ee0c0965f2p+4,
0x1.2418b24c38a5cp+4, 0x1.27de7346f684p+4, 0x1.2ba03ca4528c8p+4,
0x1.2f5e26539dc09p+4, 0x1.3318474eabe39p+4, 0x1.36ceb5a77a94dp+4,
0x1.3a818694e24dp+4, 0x1.3e30ce7e660f1p+4, 0x1.41dca1073492p+4,
0x1.458511186bd73p+4, 0x1.492a30eaae84dp+4, 0x1.4ccc120f18e99p+4,
0x1.506ac577a2459p+4, 0x1.54065b7ef5cd1p+4, 0x1.579ee3efcdd27p+4,
0x1.5b346e0bda94bp+4, 0x1.5ec708923d5d4p+4, 0x1.6256c1c59fd6ap+4,
0x1.65e3a771eed6ap+4, 0x1.696dc6f1bf3dbp+4, 0x1.6cf52d3362fc7p+4,
0x1.7079e6bdb3d25p+4, 0x1.73fbffb498e39p+4, 0x1.777b83dd4bdbep+4,
0x1.7af87ea261f2dp+4, 0x1.7e72fb179cd07p+4, 0x1.81eb03fd8702p+4,
0x1.8560a3c4df648p+4, 0x1.88d3e491d6abp+4, 0x1.8c44d03f21e95p+4,
0x1.8fb37060e4d4p+4, 0x1.931fce477635cp+4, 0x1.9689f30200ea3p+4,
0x1.99f1e7610384ep+4, 0x1.9d57b3f8b0a46p+4, 0x1.a0bb612331cbap+4,
0x1.a41cf702ce7e4p+4, 0x1.a77c7d83f939dp+4, 0x1.aad9fc5f43ce7p+4,
0x1.ae357b1b3c7bbp+4, 0x1.b18f010e3520cp+4, 0x1.b4e6955ff5b96p+4,
0x1.b83c3f0b5b4a3p+4, 0x1.bb9004dfe44f2p+4, 0x1.bee1ed832bab6p+4,
0x1.c231ff72530a1p+4, 0x1.c58041035d915p+4, 0x1.c8ccb8667bb98p+4,
0x1.cc176ba7490f2p+4, 0x1.cf6060adfc987p+4, 0x1.d2a79d408c8e3p+4,
0x1.d5ed2703c60a4p+4, 0x1.d931037c5947ep+4, 0x1.dc73380fdb065p+4,
0x1.dfb3ca05bb95ep+4, 0x1.e2f2be883411fp+4, 0x1.e6301aa52a3ffp+4,
0x1.e96be34f0b873p+4, 0x1.eca61d5d9f6d1p+4, 0x1.efdecd8ed1fcp+4,
0x1.f315f8877675bp+4, 0x1.f64ba2d402acbp+4, 0x1.f97fd0e94359dp+4,
0x1.fcb2872509c13p+4, 0x1.ffe3c9ced2f31p+4, 0x1.0189ce8c3478ap+5,
0x1.0321028f3effep+5, 0x1.04b782f4a130dp+5, 0x1.064d51b67a605p+5,
0x1.07e270c536bcdp+5, 0x1.0976e207d2849p+5, 0x1.0b0aa75c1ae45p+5,
0x1.0c9dc296ec9a1p+5, 0x1.0e30358470722p+5, 0x1.0fc201e855b65p+5,
0x1.1153297e0aa5p+5, 0x1.12e3adf8f3046p+5, 0x1.147391049ce59p+5,
0x1.1602d444f3ab2p+5, 0x1.179179567163ap+5, 0x1.191f81ce4e8a4p+5,
0x1.1aacef3ab03d9p+5, 0x1.1c39c322d4fb5p+5, 0x1.1dc5ff073ff05p+5,
0x1.1f51a461e2eb5p+5, 0x1.20dcb4a646fe7p+5, 0x1.22673141b3de8p+5,
0x1.23f11b9b5609bp+5, 0x1.257a751463c3fp+5, 0x1.27033f0840f26p+5,
0x1.288b7acca1e0fp+5, 0x1.2a1329b1acfdap+5, 0x1.2b9a4d021b90dp+5,
0x1.2d20e603597e7p+5, 0x1.2ea6f5f5a4178p+5, 0x1.302c7e142804bp+5,
0x1.31b17f951e53p+5, 0x1.3335fba9e8a99p+5, 0x1.34b9f37f2cb07p+5,
0x1.363d683ceeaf9p+5, 0x1.37c05b06ab6d5p+5, 0x1.3942ccfb71523p+5,
0x1.3ac4bf35f8d92p+5, 0x1.3c4632ccbc526p+5, 0x1.3dc728d20efdcp+5,
0x1.3f47a2543383ep+5, 0x1.40c7a05d71d19p+5, 0x1.424723f42c5cap+5,
0x1.43c62e1af4d59p+5, 0x1.4544bfd0a04bfp+5, 0x1.46c2da105ac98p+5,
0x1.48407dd1ba689p+5, 0x1.49bdac08d1ea9p+5, 0x1.4b3a65a642d19p+5,
0x1.4cb6ab974f026p+5, 0x1.4e327ec5e9f19p+5, 0x1.4fade018c95ffp+5,
0x1.5128d07375a9fp+5, 0x1.52a350b659acbp+5, 0x1.541d61bed244fp+5,
0x1.559704673d6a5p+5, 0x1.5710398708ea9p+5, 0x1.588901f2c0c6ap+5,
0x1.5a015e7c1d35ep+5, 0x1.5b794ff210511p+5, 0x1.5cf0d720d3682p+5,
0x1.5e67f4d1f404fp+5, 0x1.5fdea9cc609eap+5, 0x1.6154f6d474fddp+5,
0x1.62cadcac06566p+5, 0x1.64405c126f16cp+5, 0x1.65b575c49a71p+5,
0x1.672a2a7d0f9d9p+5, 0x1.689e7af3fcdb9p+5, 0x1.6a1267df422f6p+5,
0x1.6b85f1f27be1bp+5, 0x1.6cf919df0cc14p+5, 0x1.6e6be0542828p+5,
0x1.6fde45fedbc6ap+5, 0x1.71504b8a1936ep+5, 0x1.72c1f19ebf571p+5,
0x1.743338e3a36fcp+5, 0x1.75a421fd9a25cp+5, 0x1.7714ad8f8038dp+5,
0x1.7884dc3a43118p+5, 0x1.79f4ae9ce91ebp+5, 0x1.7b6425549a048p+5,
0x1.7cd340fca69dep+5, 0x1.7e42022e90d19p+5, 0x1.7fb06982133c6p+5,
0x1.811e778d28b14p+5, 0x1.828c2ce413908p+5, 0x1.83f98a1964f73p+5,
0x1.85668fbe03c7fp+5, 0x1.86d33e61338dap+5, 0x1.883f96909b39cp+5,
0x1.89ab98d84bbe7p+5, 0x1.8b1745c2c685bp+5, 0x1.8c829dd903c6ep+5,
0x1.8deda1a278ba6p+5, 0x1.8f5851a51dad7p+5, 0x1.90c2ae6573f61p+5,
0x1.922cb8668bc85p+5, 0x1.9396702a09edbp+5, 0x1.94ffd6302d5edp+5,
0x1.9668eaf7d4c0ep+5, 0x1.97d1aefe83c75p+5, 0x1.993a22c06879dp+5,
0x1.9aa246b860604p+5, 0x1.9c0a1b5ffd94ap+5, 0x1.9d71a12f8bbb4p+5,
0x1.9ed8d89e14e33p+5, 0x1.a03fc221664d8p+5, 0x1.a1a65e2e151ddp+5,
0x1.a30cad3782f3dp+5, 0x1.a472afafe26d8p+5, 0x1.a5d866083b94dp+5,
0x1.a73dd0b07036cp+5, 0x1.a8a2f01740263p+5, 0x1.aa07c4aa4d6ap+5,
0x1.ab6c4ed620578p+5, 0x1.acd08f062b99p+5, 0x1.ae3485a4d021bp+5,
0x1.af98331b610eap+5, 0x1.b0fb97d227759p+5, 0x1.b25eb43066222p+5,
0x1.b3c1889c5d417p+5, 0x1.b524157b4dfcdp+5, 0x1.b6865b317e03ap+5,
0x1.b7e85a223b056p+5, 0x1.b94a12afde1b4p+5, 0x1.baab853bcf228p+5,
0x1.bc0cb22688078p+5, 0x1.bd6d99cf9801fp+5, 0x1.bece3c95a6c26p+5,
0x1.c02e9ad67791bp+5, 0x1.c18eb4eeec622p+5, 0x1.c2ee8b3b08d3ap+5,
0x1.c44e1e15f529ap+5, 0x1.c5ad6dda01349p+5, 0x1.c70c7ae0a72e9p+5,
0x1.c86b45828e8b1p+5, 0x1.c9c9ce178ebabp+5, 0x1.cb2814f6b1e3p+5,
0x1.cc861a76378a3p+5, 0x1.cde3deeb9737ap+5, 0x1.cf4162ab8309p+5,
0x1.d09ea609ea3c4p+5, 0x1.d1fba959fbaecp+5, 0x1.d3586cee28523p+5,
0x1.d4b4f1182596ap+5, 0x1.d6113628efca8p+5, 0x1.d76d3c70cc70fp+5,
0x1.d8c9043f4c8dcp+5, 0x1.da248de34ee89p+5, 0x1.db7fd9ab0245dp+5,
0x1.dcdae7e3e7971p+5, 0x1.de35b8dad4224p+5, 0x1.df904cdbf39fcp+5,
0x1.e0eaa432ca503p+5, 0x1.e244bf2a37096p+5, 0x1.e39e9e0c753b6p+5,
0x1.e4f841231eed1p+5, 0x1.e651a8b72eb08p+5, 0x1.e7aad51101902p+5,
0x1.e903c67858f36p+5, 0x1.ea5c7d345c7ccp+5, 0x1.ebb4f98b9bdf4p+5,
0x1.ed0d3bc410adcp+5, 0x1.ee65442320226p+5, 0x1.efbd12ed9cdf6p+5,
0x1.f114a867c8a8dp+5, 0x1.f26c04d55617dp+5, 0x1.f3c328796a46ep+5,
0x1.f51a13969e77ep+5, 0x1.f670c66f01b3ep+5, 0x1.f7c741441a643p+5,
0x1.f91d8456e7e5fp+5, 0x1.fa738fe7e416fp+5, 0x1.fbc9643704dd6p+5,
0x1.fd1f0183bda8dp+5, 0x1.fe74680d00ee8p+5, 0x1.ffc99811419f1p+5,
0x1.008f48e73a4bep+6, 0x1.0139aac109069p+6, 0x1.01e3f1b48b78dp+6,
0x1.028e1de0032b1p+6, 0x1.03382f617562bp+6, 0x1.03e22656abcdbp+6,
0x1.048c02dd352b3p+6, 0x1.0535c51265f1fp+6, 0x1.05df6d1358f4cp+6,
0x1.0688fafcf003ap+6, 0x1.07326eebd48bcp+6, 0x1.07dbc8fc78344p+6,
0x1.0885094b15799p+6, 0x1.092e2ff3b045bp+6, 0x1.09d73d1216874p+6,
0x1.0a8030c1e0c5fp+6, 0x1.0b290b1e72b4ep+6, 0x1.0bd1cc42fbc2fp+6,
0x1.0c7a744a77a97p+6, 0x1.0d23034faef89p+6, 0x1.0dcb796d37a1bp+6,
0x1.0e73d6bd75803p+6, 0x1.0f1c1b5a9ae01p+6, 0x1.0fc4475ea903p+6,
0x1.106c5ae370a38p+6, 0x1.111456029276p+6, 0x1.11bc38d57fa8dp+6,
0x1.126403757a61cp+6, 0x1.130bb5fb963a6p+6, 0x1.13b35080b8ba9p+6,
0x1.145ad31d99d17p+6, 0x1.15023deac44cap+6, 0x1.15a99100964ddp+6,
0x1.1650cc7741bf5p+6, 0x1.16f7f066ccc63p+6, 0x1.179efce71233cp+6,
0x1.1845f20fc1f5p+6, 0x1.18eccff86181p+6, 0x1.199396b84c455p+6,
0x1.1a3a4666b411bp+6, 0x1.1ae0df1aa181dp+6, 0x1.1b8760eaf465dp+6,
0x1.1c2dcbee6429dp+6, 0x1.1cd4203b803b8p+6, 0x1.1d7a5de8b06f1p+6,
0x1.1e20850c35621p+6, 0x1.1ec695bc28de3p+6, 0x1.1f6c900e7e395p+6,
0x1.2012741902b5bp+6, 0x1.20b841f15de01p+6, 0x1.215df9ad11ecfp+6,
0x1.22039b617c14ap+6, 0x1.22a92723d4ee4p+6, 0x1.234e9d0930c95p+6,
0x1.23f3fd2680066p+6, 0x1.249947908f6eep+6, 0x1.253e7c5c088b4p+6,
0x1.25e39b9d71f87p+6, 0x1.2688a5692fbc9p+6, 0x1.272d99d38399ap+6,
0x1.27d278f08d607p+6, 0x1.287742d44b41ap+6, 0x1.291bf7929a1ddp+6,
0x1.29c0973f35d55p+6, 0x1.2a6521edb9966p+6, 0x1.2b0997b1a02a8p+6,
0x1.2badf89e4443p+6, 0x1.2c5244c6e0c4dp+6, 0x1.2cf67c3e9113p+6,
0x1.2d9a9f1851588p+6, 0x1.2e3ead66fed14p+6, 0x1.2ee2a73d58123p+6,
0x1.2f868cadfd506p+6, 0x1.302a5dcb70a78p+6, 0x1.30ce1aa8165f4p+6,
0x1.3171c35635305p+6, 0x1.321557e7f6883p+6, 0x1.32b8d86f66cc3p+6,
0x1.335c44fe759c1p+6, 0x1.33ff9da6f6134p+6, 0x1.34a2e27a9f0a3p+6,
0x1.3546138b0b56p+6, 0x1.35e930e9ba07fp+6, 0x1.368c3aa80eac2p+6,
0x1.372f30d751877p+6, 0x1.37d21388afd48p+6, 0x1.3874e2cd3c008p+6,
0x1.39179eb5ede6fp+6, 0x1.39ba4753a30c7p+6, 0x1.3a5cdcb71ed9cp+6,
0x1.3aff5ef10ad53p+6, 0x1.3ba1ce11f6dbfp+6, 0x1.3c442a2a595a9p+6,
0x1.3ce6734a8f84fp+6, 0x1.3d88a982dd8d7p+6, 0x1.3e2acce36edbap+6,
0x1.3eccdd7c56428p+6, 0x1.3f6edb5d8e35ap+6, 0x1.4010c696f8fe4p+6,
0x1.40b29f3860ef8p+6, 0x1.41546551789a1p+6, 0x1.41f618f1daff7p+6,
0x1.4297ba290bc45p+6, 0x1.433949067762fp+6, 0x1.43dac599735c5p+6,
0x1.447c2ff13e695p+6, 0x1.451d881d00ab1p+6, 0x1.45bece2bcbdaep+6,
0x1.4660022c9b798p+6, 0x1.4701242e54fe4p+6, 0x1.47a2343fc8054p+6,
0x1.4843326fae7d5p+6, 0x1.48e41eccacd55p+6, 0x1.4984f96552293p+6,
0x1.4a25c248186e7p+6, 0x1.4ac67983649fdp+6, 0x1.4b671f2586e92p+6,
0x1.4c07b33cbad22p+6, 0x1.4ca835d727694p+6, 0x1.4d48a702df6dap+6,
0x1.4de906cde178bp+6, 0x1.4e8955461827ep+6, 0x1.4f2992795a45p+6,
0x1.4fc9be756aefp+6, 0x1.5069d947f9c1cp+6, 0x1.5109e2fea2fdcp+6,
0x1.51a9dba6efaf5p+6, 0x1.5249c34e55d55p+6, 0x1.52e99a0238879p+6,
0x1.53895fcfe81cfp+6, 0x1.542914c4a250dp+6, 0x1.54c8b8ed92684p+6,
0x1.55684c57d1573p+6, 0x1.5607cf1065e48p+6, 0x1.56a7412444ce4p+6,
0x1.5746a2a050edap+6, 0x1.57e5f3915b5ap+6, 0x1.58853404238c4p+6,
0x1.5924640557812p+6, 0x1.59c383a193dcp+6, 0x1.5a6292e564085p+6,
0x1.5b0191dd425bep+6, 0x1.5ba0809598377p+6, 0x1.5c3f5f1abe287p+6,
0x1.5cde2d78fc09p+6, 0x1.5d7cebbc8920ep+6, 0x1.5e1b99f18c44dp+6,
0x1.5eba38241bf6ep+6, 0x1.5f58c6603e859p+6, 0x1.5ff744b1ea2adp+6,
0x1.6095b325052afp+6, 0x1.613411c565f32p+6, 0x1.61d2609ed3379p+6,
0x1.62709fbd04115p+6, 0x1.630ecf2ba01cp+6, 0x1.63aceef63f933p+6,
0x1.644aff286b6f5p+6, 0x1.64e8ffcd9d827p+6, 0x1.6586f0f14094ep+6,
0x1.6624d29eb0814p+6, 0x1.66c2a4e13a50cp+6, 0x1.676067c41c565p+6,
0x1.67fe1b52864a8p+6, 0x1.689bbf9799669p+6, 0x1.6939549e687f3p+6,
0x1.69d6da71f81f4p+6, 0x1.6a74511d3ea25p+6, 0x1.6b11b8ab244e9p+6,
0x1.6baf1126836f1p+6, 0x1.6c4c5a9a286dp+6, 0x1.6ce99510d1e98p+6,
0x1.6d86c09530d68p+6, 0x1.6e23dd31e89p+6, 0x1.6ec0eaf18ef4bp+6,
0x1.6f5de9deac7e3p+6, 0x1.6ffada03bc59ep+6, 0x1.7097bb6b2c805p+6,
0x1.71348e1f5dcd9p+6, 0x1.71d1522aa418bp+6, 0x1.726e0797464adp+6,
0x1.730aae6f7e76ep+6, 0x1.73a746bd79f03p+6, 0x1.7443d08b59614p+6,
0x1.74e04be330e27p+6, 0x1.757cb8cf08104p+6, 0x1.76191758da21bp+6,
0x1.76b5678a95fddp+6, 0x1.7751a96e1e51dp+6, 0x1.77eddd0d49a66p+6,
0x1.788a0271e2755p+6, 0x1.792619a5a73e7p+6, 0x1.79c222b24a9ccp+6,
0x1.7a5e1da1735b2p+6, 0x1.7afa0a7cbc891p+6, 0x1.7b95e94db58eep+6,
0x1.7c31ba1de2425p+6, 0x1.7ccd7cf6bafa5p+6, 0x1.7d6931e1aca2fp+6,
0x1.7e04d8e818d16p+6, 0x1.7ea0721355d73p+6, 0x1.7f3bfd6caed5ap+6,
0x1.7fd77afd63d16p+6, 0x1.8072eacea9c4dp+6, 0x1.810e4ce9aab3ap+6,
0x1.81a9a15785bdp+6, 0x1.8244e8214f2e7p+6, 0x1.82e0215010961p+6,
0x1.837b4cecc8d4fp+6, 0x1.84166b006c31p+6, 0x1.84b17b93e467p+6,
0x1.854c7eb010bc6p+6, 0x1.85e7745dc610dp+6, 0x1.86825ca5ceefbp+6,
0x1.871d3790eba17p+6, 0x1.87b80527d23c9p+6, 0x1.8852c5732eb6fp+6,
0x1.88ed787ba2f65p+6, 0x1.89881e49c6e18p+6, 0x1.8a22b6e628708p+6,
0x1.8abd42594bbd5p+6, 0x1.8b57c0abab14p+6, 0x1.8bf231e5b7032p+6,
0x1.8c8c960fd66b8p+6, 0x1.8d26ed3266905p+6, 0x1.8dc13755bb26ep+6,
0x1.8e5b74821e662p+6, 0x1.8ef5a4bfd1164p+6, 0x1.8f8fc8170aa02p+6,
0x1.9029de8ff91c7p+6, 0x1.90c3e832c162fp+6, 0x1.915de5077f194p+6,
0x1.91f7d51644c22p+6, 0x1.9291b8671bcbdp+6, 0x1.932b8f02049efp+6,
0x1.93c558eef6acdp+6, 0x1.945f1635e07e1p+6, 0x1.94f8c6dea7c0fp+6,
0x1.95926af129572p+6, 0x1.962c027539643p+6, 0x1.96c58d72a35b4p+6,
0x1.975f0bf12a0dp+6, 0x1.97f87df887b54p+6, 0x1.9891e3906e08cp+6,
0x1.992b3cc086427p+6, 0x1.99c489907130ep+6, 0x1.9a5dca07c743bp+6,
0x1.9af6fe2e1898ap+6, 0x1.9b90260aed089p+6, 0x1.9c2941a5c4349p+6,
0x1.9cc251061592dp+6, 0x1.9d5b5433507b1p+6, 0x1.9df44b34dc33ap+6,
0x1.9e8d361217fddp+6, 0x1.9f2614d25b226p+6, 0x1.9fbee77cf4fdbp+6,
0x1.a057ae192d0c7p+6, 0x1.a0f068ae42f76p+6, 0x1.a18917436e9f9p+6,
0x1.a221b9dfe02a6p+6, 0x1.a2ba508ac00d4p+6, 0x1.a352db4b2f19bp+6,
0x1.a3eb5a284688bp+6, 0x1.a483cd2918068p+6, 0x1.a51c3454adbe4p+6,
0x1.a5b48fb20a652p+6, 0x1.a64cdf482945dp+6, 0x1.a6e5231dfe4bcp+6,
0x1.a77d5b3a760e7p+6, 0x1.a81587a475dc5p+6, 0x1.a8ada862dbc5bp+6,
0x1.a945bd7c7ea7ep+6, 0x1.a9ddc6f82e38p+6, 0x1.aa75c4dcb30d8p+6,
0x1.ab0db730cead1p+6, 0x1.aba59dfb3b93p+6, 0x1.ac3d7942ad3dep+6,
0x1.acd5490dd038fp+6, 0x1.ad6d0d634a264p+6, 0x1.ae04c649b9c94p+6,
0x1.ae9c73c7b710bp+6, 0x1.af3415e3d320fp+6, 0x1.afcbaca4985dep+6,
0x1.b06338108a74dp+6, 0x1.b0fab82e2666ap+6, 0x1.b1922d03e2912p+6,
0x1.b22996982eb97p+6, 0x1.b2c0f4f17414ep+6, 0x1.b358481615534p+6,
0x1.b3ef900c6ea7fp+6, 0x1.b486ccdad5d38p+6, 0x1.b51dfe879a2dp+6,
0x1.b5b5251904ab7p+6, 0x1.b64c409557eecp+6, 0x1.b6e35102d0495p+6,
0x1.b77a5667a3c8cp+6, 0x1.b81150ca023f1p+6, 0x1.b8a84030154bcp+6,
0x1.b93f24a000647p+6, 0x1.b9d5fe1fe0dep+6, 0x1.ba6cccb5cdf52p+6,
0x1.bb039067d8d73p+6, 0x1.bb9a493c0caadp+6, 0x1.bc30f7386e986p+6,
0x1.bcc79a62fdd2cp+6, 0x1.bd5e32c1b39fap+6, 0x1.bdf4c05a835ffp+6,
0x1.be8b43335a981p+6, 0x1.bf21bb5220f88p+6, 0x1.bfb828bcb8659p+6,
0x1.c04e8b78fd001p+6, 0x1.c0e4e38cc52dp+6, 0x1.c17b30fde19ddp+6,
0x1.c21173d21d587p+6, 0x1.c2a7ac0f3dbefp+6, 0x1.c33dd9bb0297dp+6,
0x1.c3d3fcdb26157p+6, 0x1.c46a15755cdep+6, 0x1.c500238f56132p+6,
0x1.c596272ebb59dp+6, 0x1.c62c205930e19p+6, 0x1.c6c20f14556c5p+6,
0x1.c757f365c255dp+6, 0x1.c7edcd530b9bp+6, 0x1.c8839ce1bfe18p+6,
0x1.c9196217687edp+6, 0x1.c9af1cf9897fdp+6, 0x1.ca44cd8da1afep+6,
0x1.cada73d92aap+6, 0x1.cb700fe198adfp+6, 0x1.cc05a1ac5b0b8p+6,
0x1.cc9b293edbc56p+6, 0x1.cd30a69e7fca3p+6, 0x1.cdc619d0a6f17p+6,
0x1.ce5b82daac027p+6, 0x1.cef0e1c1e4bbp+6, 0x1.cf86368ba1d67p+6,
0x1.d01b813d2f145p+6, 0x1.d0b0c1dbd33efp+6, 0x1.d145f86cd0322p+6,
0x1.d1db24f562e1fp+6, 0x1.d270477ac3612p+6, 0x1.d305600224e7bp+6,
0x1.d39a6e90b5d95p+6, 0x1.d42f732b9fcbfp+6, 0x1.d4c46dd8078ddp+6,
0x1.d5595e9b0d2c7p+6, 0x1.d5ee4579cbfa3p+6, 0x1.d68322795a953p+6,
0x1.d717f59ecaed1p+6, 0x1.d7acbeef2a497p+6, 0x1.d8417e6f814fdp+6,
0x1.d8d63424d40ap+6, 0x1.d96ae01421ebcp+6, 0x1.d9ff824265d92p+6,
0x1.da941ab4962c7p+6, 0x1.db28a96fa4bbep+6, 0x1.dbbd2e787edfcp+6,
0x1.dc51a9d40d785p+6, 0x1.dce61b8734f35p+6, 0x1.dd7a8396d5521p+6,
0x1.de0ee207ca2efp+6, 0x1.dea336deeac37p+6, 0x1.df37822109ed8p+6,
0x1.dfcbc3d2f6352p+6, 0x1.e05ffbf979d26p+6, 0x1.e0f42a995ab27p+6,
0x1.e1884fb75a7d8p+6, 0x1.e21c6b58369cp+6, 0x1.e2b07d80a83c3p+6,
0x1.e34486356457ap+6, 0x1.e3d8857b1bb85p+6, 0x1.e46c7b567afe4p+6,
0x1.e50067cc2aa4cp+6, 0x1.e5944ae0cf077p+6, 0x1.e62824990867dp+6,
0x1.e6bbf4f972f26p+6, 0x1.e74fbc06a6c3ap+6, 0x1.e7e379c537ed6p+6,
0x1.e8772e39b67bep+6, 0x1.e90ad968ae7abp+6, 0x1.e99e7b56a7f9fp+6,
0x1.ea32140827133p+6, 0x1.eac5a381abee9p+6, 0x1.eb5929c7b2c77p+6,
0x1.ebeca6deb3f1bp+6, 0x1.ec801acb23de3p+6, 0x1.ed138591732p+6,
0x1.eda6e7360e71p+6, 0x1.ee3a3fbd5eb6dp+6, 0x1.eecd8f2bc9076p+6,
0x1.ef60d585aeadcp+6, 0x1.eff412cf6d2efp+6, 0x1.f087470d5e4e6p+6,
0x1.f11a7243d812cp+6, 0x1.f1ad94772cca7p+6, 0x1.f240adabab102p+6,
0x1.f2d3bde59dcf7p+6, 0x1.f366c5294c498p+6, 0x1.f3f9c37afa193p+6,
0x1.f48cb8dee737ep+6, 0x1.f51fa5595001dp+6, 0x1.f5b288ee6d3a6p+6,
0x1.f64563a274109p+6, 0x1.f6d8357996237p+6, 0x1.f76afe7801866p+6,
0x1.f7fdbea1e0c55p+6, 0x1.f89075fb5ae92p+6, 0x1.f9232488937bdp+6,
0x1.f9b5ca4daa8cbp+6, 0x1.fa48674ebcb4dp+6, 0x1.fadafb8fe31acp+6,
0x1.fb6d871533774p+6, 0x1.fc0009e2c018dp+6, 0x1.fc9283fc97e84p+6,
0x1.fd24f566c66c6p+6, 0x1.fdb75e2553ce6p+6, 0x1.fe49be3c44ddbp+6,
0x1.fedc15af9b13fp+6, 0x1.ff6e648354991p+6, 0x1.0000555db6239p+7,
0x1.0049742decd72p+7, 0x1.00928eb4488c5p+7, 0x1.00dba4f2c1c73p+7,
0x1.0124b6eb4f6cbp+7, 0x1.016dc49fe6c47p+7, 0x1.01b6ce127b7aep+7,
0x1.01ffd344ffa31p+7, 0x1.0248d43963b85p+7, 0x1.0291d0f196a07p+7,
0x1.02dac96f85ad8p+7, 0x1.0323bdb51c9f8p+7, 0x1.036cadc445a65p+7,
0x1.03b5999ee963ap+7, 0x1.03fe8146eeecap+7, 0x1.044764be3bcbbp+7,
0x1.04904406b4028p+7, 0x1.04d91f223a0b9p+7, 0x1.0521f612aedc2p+7,
0x1.056ac8d9f1e5dp+7, 0x1.05b39779e1187p+7, 0x1.05fc61f458e3ep+7,
0x1.0645284b34398p+7, 0x1.068dea804c8e3p+7, 0x1.06d6a89579dbep+7,
0x1.071f628c92a36p+7, 0x1.076818676bedep+7, 0x1.07b0ca27d94edp+7,
0x1.07f977cface58p+7, 0x1.08422160b75ecp+7, 0x1.088ac6dcc7f68p+7,
0x1.08d36845ac798p+7, 0x1.091c059d3146fp+7, 0x1.09649ee521521p+7,
0x1.09ad341f4623ep+7, 0x1.09f5c54d67dc9p+7, 0x1.0a3e52714d354p+7,
0x1.0a86db8cbb817p+7, 0x1.0acf60a176b0ep+7, 0x1.0b17e1b14150ap+7,
0x1.0b605ebddc8d3p+7, 0x1.0ba8d7c90833ap+7, 0x1.0bf14cd482b36p+7,
0x1.0c39bde2091f9p+7, 0x1.0c822af35730ap+7, 0x1.0cca940a2746p+7,
0x1.0d12f92832674p+7, 0x1.0d5b5a4f3045fp+7, 0x1.0da3b780d73efp+7,
0x1.0dec10bedc5bdp+7, 0x1.0e34660af3548p+7, 0x1.0e7cb766ce90bp+7,
0x1.0ec504d41f295p+7, 0x1.0f0d4e5494e9ep+7, 0x1.0f5593e9de51fp+7,
0x1.0f9dd595a896cp+7, 0x1.0fe613599fa46p+7, 0x1.102e4d376e1f3p+7,
0x1.10768330bd659p+7, 0x1.10beb5473590dp+7, 0x1.1106e37c7d76fp+7,
0x1.114f0dd23aabdp+7, 0x1.1197344a1182bp+7, 0x1.11df56e5a50f8p+7,
0x1.122775a697285p+7, 0x1.126f908e88666p+7, 0x1.12b7a79f1827fp+7,
0x1.12ffbad9e4914p+7, 0x1.1347ca408a8dfp+7, 0x1.138fd5d4a5d27p+7,
0x1.13d7dd97d0dd3p+7, 0x1.141fe18ba4f82p+7, 0x1.1467e1b1ba39cp+7,
0x1.14afde0ba7869p+7, 0x1.14f7d69b02926p+7, 0x1.153fcb615fe17p+7,
0x1.1587bc6052cap+7, 0x1.15cfa9996d752p+7, 0x1.1617930e40e08p+7,
0x1.165f78c05cdf3p+7, 0x1.16a75ab1501b4p+7, 0x1.16ef38e2a816bp+7,
0x1.17371355f12d1p+7, 0x1.177eea0cb6942p+7, 0x1.17c6bd08825dbp+7,
0x1.180e8c4add786p+7, 0x1.185657d54fb12p+7, 0x1.189e1fa95fb42p+7,
0x1.18e5e3c8930e2p+7, 0x1.192da4346e2ddp+7, 0x1.197560ee7464ap+7,
0x1.19bd19f827e83p+7, 0x1.1a04cf5309d38p+7, 0x1.1a4c81009a27ep+7,
0x1.1a942f0257ce4p+7, 0x1.1adbd959c0986p+7, 0x1.1b2380085141fp+7,
0x1.1b6b230f85717p+7, 0x1.1bb2c270d7b9cp+7, 0x1.1bfa5e2dc19afp+7,
0x1.1c41f647bb83ap+7, 0x1.1c898ac03cd1ap+7, 0x1.1cd11b98bbd3cp+7,
0x1.1d18a8d2adca5p+7, 0x1.1d60326f86e89p+7, 0x1.1da7b870ba558p+7,
0x1.1def3ad7ba2d6p+7, 0x1.1e36b9a5f7824p+7, 0x1.1e7e34dce25d9p+7,
0x1.1ec5ac7de9c0cp+7, 0x1.1f0d208a7ba6cp+7, 0x1.1f5491040504bp+7,
0x1.1f9bfdebf1cb2p+7, 0x1.1fe36743ace7p+7, 0x1.202acd0ca042dp+7,
0x1.20722f4834c77p+7, 0x1.20b98df7d25d6p+7, 0x1.2100e91cdfedbp+7,
0x1.214840b8c362fp+7, 0x1.218f94cce1aa5p+7, 0x1.21d6e55a9eb49p+7,
0x1.221e32635d772p+7, 0x1.22657be87fecep+7, 0x1.22acc1eb67177p+7,
0x1.22f4046d72ffep+7, 0x1.233b437002b7ep+7, 0x1.23827ef4745aap+7,
0x1.23c9b6fc250dcp+7, 0x1.2410eb8871028p+7, 0x1.24581c9ab3766p+7,
0x1.249f4a3446b47p+7, 0x1.24e674568415ep+7, 0x1.252d9b02c4036p+7,
0x1.2574be3a5df5ap+7, 0x1.25bbddfea876cp+7, 0x1.2602fa50f922bp+7,
0x1.264a1332a4a89p+7, 0x1.269128a4fecb7p+7, 0x1.26d83aa95a633p+7,
0x1.271f4941095d8p+7, 0x1.2766546d5cbecp+7, 0x1.27ad5c2fa4a2dp+7,
0x1.27f46089303e4p+7, 0x1.283b617b4ddedp+7, 0x1.28825f074aecap+7,
0x1.28c9592e73eafp+7, 0x1.29104ff214792p+7, 0x1.2957435377537p+7,
0x1.299e3353e653ep+7, 0x1.29e51ff4aa732p+7, 0x1.2a2c09370bc97p+7,
0x1.2a72ef1c518f7p+7, 0x1.2ab9d1a5c21fp+7, 0x1.2b00b0d4a2f42p+7,
0x1.2b478caa38adcp+7, 0x1.2b8e6527c70e7p+7, 0x1.2bd53a4e90fdap+7,
0x1.2c1c0c1fd8882p+7, 0x1.2c62da9cdee0fp+7, 0x1.2ca9a5c6e4625p+7,
0x1.2cf06d9f288e5p+7, 0x1.2d373226ea1p+7, 0x1.2d7df35f66bbbp+7,
0x1.2dc4b149db905p+7, 0x1.2e0b6be784b7ep+7, 0x1.2e5223399d888p+7,
0x1.2e98d7416084fp+7, 0x1.2edf8800075dap+7, 0x1.2f263576caf15p+7,
0x1.2f6cdfa6e34e1p+7, 0x1.2fb3869187b1bp+7, 0x1.2ffa2a37ee8aep+7,
0x1.3040ca9b4d79bp+7, 0x1.308767bcd9509p+7, 0x1.30ce019dc614fp+7,
0x1.3114983f47p+7, 0x1.315b2ba28e7fap+7, 0x1.31a1bbc8ce36fp+7,
0x1.31e848b336ff1p+7, 0x1.322ed262f8e8p+7, 0x1.327558d943398p+7,
0x1.32bbdc1744734p+7, 0x1.33025c1e2a4e5p+7, 0x1.3348d8ef21bd7p+7,
0x1.338f528b56eddp+7, 0x1.33d5c8f3f547ep+7, 0x1.341c3c2a27703p+7,
0x1.3462ac2f1747dp+7, 0x1.34a91903eded6p+7, 0x1.34ef82a9d3bd7p+7,
0x1.3535e921f0538p+7, 0x1.357c4c6d6a8abp+7, 0x1.35c2ac8d687e4p+7,
0x1.360909830f8a5p+7, 0x1.364f634f844cdp+7, 0x1.3695b9f3eaa5fp+7,
0x1.36dc0d7165b8ep+7, 0x1.37225dc917ec8p+7, 0x1.3768aafc22ec3p+7,
0x1.37aef50ba7a84p+7, 0x1.37f53bf8c656fp+7, 0x1.383b7fc49e74cp+7,
0x1.3881c0704ec56p+7, 0x1.38c7fdfcf5543p+7, 0x1.390e386baf753p+7,
0x1.39546fbd99c53p+7, 0x1.399aa3f3d02afp+7, 0x1.39e0d50f6dd7ap+7,
0x1.3a2703118d475p+7, 0x1.3a6d2dfb4842p+7, 0x1.3ab355cdb7dbdp+7,
0x1.3af97a89f4761p+7, 0x1.3b3f9c3115bf9p+7, 0x1.3b85bac432b59p+7,
0x1.3bcbd64461a41p+7, 0x1.3c11eeb2b826bp+7, 0x1.3c5804104b293p+7,
0x1.3c9e165e2ee83p+7, 0x1.3ce4259d76f1ap+7, 0x1.3d2a31cf3625ap+7,
0x1.3d703af47eb6cp+7, 0x1.3db6410e622bp+7, 0x1.3dfc441df15c4p+7,
0x1.3e4244243c78dp+7, 0x1.3e88412253042p+7, 0x1.3ece3b1943d75p+7,
0x1.3f14320a1d21cp+7, 0x1.3f5a25f5ec69dp+7, 0x1.3fa016ddbe8d4p+7,
0x1.3fe604c29fc2p+7, 0x1.402befa59b96ap+7, 0x1.4071d787bcf2fp+7,
0x1.40b7bc6a0e18ap+7, 0x1.40fd9e4d98a3bp+7, 0x1.41437d33658b4p+7,
0x1.4189591c7d221p+7, 0x1.41cf3209e716dp+7, 0x1.421507fcaa751p+7,
0x1.425adaf5cda5bp+7, 0x1.42a0aaf6566f6p+7, 0x1.42e677ff49f73p+7,
0x1.432c4211acc13p+7, 0x1.4372092e82b1p+7, 0x1.43b7cd56cf0a5p+7,
0x1.43fd8e8b94717p+7, 0x1.44434ccdd4ecp+7, 0x1.4489081e91e14p+7,
0x1.44cec07ecc1acp+7, 0x1.451475ef83c4ep+7, 0x1.455a2871b86f7p+7,
0x1.459fd806690e1p+7, 0x1.45e584ae93f8ep+7, 0x1.462b2e6b36ed1p+7,
0x1.4670d53d4f0d2p+7, 0x1.46b67925d8e1dp+7, 0x1.46fc1a25d05a4p+7,
0x1.4741b83e30cccp+7, 0x1.4787536ff4f74p+7, 0x1.47ccebbc16ffdp+7,
0x1.481281239075p+7, 0x1.485813a75a4ecp+7, 0x1.489da3486cee9p+7,
0x1.48e33007c0202p+7, 0x1.4928b9e64b19dp+7, 0x1.496e40e5047d3p+7,
0x1.49b3c504e2578p+7, 0x1.49f94646da225p+7, 0x1.4a3ec4abe0c3bp+7,
0x1.4a844034ea8f1p+7, 0x1.4ac9b8e2eb458p+7, 0x1.4b0f2eb6d6163p+7,
0x1.4b54a1b19d9f1p+7, 0x1.4b9a11d433ed3p+7, 0x1.4bdf7f1f8a7d5p+7,
0x1.4c24e994923c4p+7, 0x1.4c6a51343b878p+7, 0x1.4cafb5ff762d9p+7,
0x1.4cf517f7316e8p+7, 0x1.4d3a771c5bfcap+7, 0x1.4d7fd36fe3fc8p+7,
0x1.4dc52cf2b705dp+7, 0x1.4e0a83a5c223ap+7, 0x1.4e4fd789f1d52p+7,
0x1.4e9528a0320dbp+7, 0x1.4eda76e96e359p+7, 0x1.4f1fc266912a6p+7,
0x1.4f650b18853f8p+7, 0x1.4faa5100343e9p+7, 0x1.4fef941e8767ep+7,
0x1.5034d4746772dp+7, 0x1.507a1202bc8e6p+7, 0x1.50bf4cca6e61cp+7,
0x1.510484cc640c6p+7, 0x1.5149ba098426dp+7, 0x1.518eec82b4c2dp+7,
0x1.51d41c38db6c3p+7, 0x1.5219492cdd28cp+7, 0x1.525e735f9e792p+7,
0x1.52a39ad203593p+7, 0x1.52e8bf84ef403p+7, 0x1.532de17945217p+7,
0x1.537300afe76cbp+7, 0x1.53b81d29b80eap+7, 0x1.53fd36e798711p+7,
0x1.54424dea697b9p+7, 0x1.548762330b94p+7, 0x1.54cc73c25e9e8p+7,
0x1.5511829941fe8p+7, 0x1.55568eb894968p+7, 0x1.559b982134c91p+7,
0x1.55e09ed40078dp+7, 0x1.5625a2d1d5091p+7, 0x1.566aa41b8f5e3p+7,
0x1.56afa2b20bde1p+7, 0x1.56f49e9626705p+7, 0x1.573997c8ba7eep+7,
0x1.577e8e4aa2f66p+7,
};

/* return an upper-approximation of gamma(x)*(psi(x)^2 + trigamma(x)) */
static inline void
bound_d2_aux (mpfr_t tmp, mpfr_t x)
{
  mpfr_t u;
  mpfr_init2 (u, mpfr_get_prec (tmp));
  mpfr_digamma (u, x, MPFR_RNDU);
  mpfr_sqr (u, u, MPFR_RNDU);
  mpfr_trigamma (tmp, x, MPFR_RNDU);
  mpfr_add (u, u, tmp, MPFR_RNDU);
  mpfr_gamma (tmp, x, MPFR_RNDU);
  mpfr_mul (tmp, tmp, u, MPFR_RNDU);
  mpfr_abs (tmp, tmp, MPFR_RNDU); /* for x<0 */
  mpfr_clear (u);
}

/* put in tmp a bound for the absolute value of the 2nd derivative of gamma(x)
   for x0 <= x < x1, where -184 < x0.
   We have gamma''(x) = gamma(x)*(psi(x)^2 + trigamma(x)) */
static inline void
bound_d2 (mpfr_t tmp, mpfr_t x0, mpfr_t x1)
{
  double c;
  assert (mpfr_cmp_si (x0, -184) > 0);
  /* Since t0, t1 should have the same sign in bacsel, we know that x0 and x1 have the
     same sign here. */
  if (mpfr_cmp_ui (x0, 0) < 0) // then x1 < 0 too
  {
    /* check that x0 and x1 are in the same interval (k,k+1) */
    long k = mpfr_get_si (x0, MPFR_RNDD);
    assert (mpfr_cmp_si (x0, k) > 0);    // x0 should not be an integer
    assert (mpfr_cmp_si (x1, k+1) <= 0); // x1 can equal k+1 since it is not tested
    assert (-184 <= k && k <= -1);
    c = T2[-k];
  }
  else
    c = 0x1.bdb5099cc9761p+0;
  /* For x < c, |gamma''(x)| is decreasing, then it is increasing. */
  if (mpfr_cmp_d (x0, c) >= 0)
    bound_d2_aux (tmp, x1); // |gamma''(x)| is increasing on [x0,x1]
  else if (mpfr_cmp_d (x1, c) <= 0)
    bound_d2_aux (tmp, x0); // |gamma''(x)| is decreasing on [x0,x1]
  else // x0 < c < x1 thus the maximum is attained in either x0 or x1
  {
    mpfr_t u;
    mpfr_init2 (u, mpfr_get_prec (tmp));
    bound_d2_aux (tmp, x0);
    bound_d2_aux (u, x1);
    if (mpfr_cmp (u, tmp) > 0)
      mpfr_set (tmp, u, MPFR_RNDU);
    mpfr_clear (u);
  }
}

static inline void
bound_d3_aux (mpfr_t tmp, mpfr_t x)
{
  assert (mpfr_cmp_d (x, 0x1.573fae561f648p+7) <= 0);
  /* For -0.5 < x <= 1, we see graphically that the 3rd derivative of gamma(x)
     is bounded in absolute value by 6/x^4. */
  if (mpfr_cmp_ui (x, 1) <= 0)
  {
    mpfr_ui_div (tmp, 1, x, MPFR_RNDU);
    mpfr_sqr (tmp, tmp, MPFR_RNDU);
    mpfr_sqr (tmp, tmp, MPFR_RNDU);
    mpfr_mul_ui (tmp, tmp, 6, MPFR_RNDU);
  }
  else if (mpfr_cmp_d (x, 0x1.12a784eb1fd98p+0) <= 0)
    /* for 1 <= x <= t1 := 0x1.12a784eb1fd98p+0, gamma'''(x) is negative and
       |gamma'''(x)| <= |gamma'''(1)| < 5.45 */
    mpfr_set_d (tmp, 5.45, MPFR_RNDU);
  else if (mpfr_cmp_d (x, 0x1.3e89e98377ca2p+0) <= 0)
    /* for t1 < x < t2 := 0x1.3e89e98377ca2p+0, gamma'''(x) is negative and
       |gamma'''(x)| <= 4 */
    mpfr_set_ui (tmp, 4, MPFR_RNDN);
  else if (mpfr_cmp_d (x, 0x1.68efbde239f31p+0) <= 0)
    /* for t2 < x < t3 := 0x1.68efbde239f31p+0, gamma'''(x) is negative and
       |gamma'''(x)| <= 2 */
    mpfr_set_ui (tmp, 2, MPFR_RNDN);
  else if (mpfr_cmp_d (x, 0x1.22f89ef37dad4p+1) <= 0)
    /* for t3 < x < t4 := 0x1.22f89ef37dad4p+1, -1 <= gamma'''(x) < 1 */
    mpfr_set_ui (tmp, 1, MPFR_RNDN);
  else { // now gamma'''(x) >= 1
    int a = 0, b = 1032;
    while (a + 1 < b) {
      int c = (a + b) / 2;
      if (mpfr_cmp_d (x, Gamma3[c]) < 0)
        b = c;
      else
        a = c;
    }
    /* now Gamma3[a] <= x < Gamma3[b] thus gamma'''(x) <= 2^b */
  mpfr_set_ui_2exp (tmp, 1, b, MPFR_RNDN);
  }
}

/* put in tmp a bound for the absolute value of the 3rd derivative of gamma(x)
   for x0 <= x < x1, where -1 < x0 < x1 <= 0x1.573fae561f648p+7,
   and x0, x1 are of same sign, and if negative, are in the same interval (k,k+1]
   for some negative integer k.
   If USE_PARI, there is no lower limit on x0. */
static inline void
bound_d3 (mpfr_t tmp, mpfr_t x0, mpfr_t x1)
{
#ifndef USE_PARI
  assert (mpfr_cmp_d (x0, -1) > 0);
#endif
  if (mpfr_sgn (x0) > 0) { // 0 < x0 < x1
#define c0 0x1.bdb5099cc976p+0
    /* gamma'''(x) is increasing on [0, +Inf), is negative for x < c0
     and positive for c0 < x */
    assert (mpfr_sgn (x0) > 0);
    if (mpfr_cmp_d (x1, c0) <= 0)
      bound_d3_aux (tmp, x0); // maximum attained at x0
    else if (mpfr_cmp_d (x0, c0) >= 0)
      bound_d3_aux (tmp, x1); // maximum attained at x1
    else { // maximum attained at x0 or x1
      mpfr_t u;
      mpfr_init2 (u, mpfr_get_prec (tmp));
      bound_d3_aux (tmp, x0);
      bound_d3_aux (u, x1);
      if (mpfr_cmp (u, tmp) > 0)
        mpfr_set (tmp, u, MPFR_RNDU);
      mpfr_clear (u);
    }
  }
  else if (mpfr_cmp_d (x0, -0.5) >= 0) {
    /* -0.5 < x0 < x1 < 0: gamma'''(x) is decreasing from -0.529 to -Inf,
       thus the maximum of |gamma'''(x)| is attained at x1, and for
       -0.5 < x < 0, we see graphically that |gamma'''(x)| is bounded by
       6/x^4 */
    mpfr_ui_div (tmp, 1, x1, MPFR_RNDA);
    mpfr_sqr (tmp, tmp, MPFR_RNDU);
    mpfr_sqr (tmp, tmp, MPFR_RNDU);
    mpfr_mul_ui (tmp, tmp, 6, MPFR_RNDU);
  }
  else if (mpfr_cmp_d (x0, -1) >= 0) { // -1 <= x0 < -0.5
#define c1 -0x1.002d390d7b12ep-1
    /* -1 < x0 < x1 <= -0.5 (since x0 and x1 are in the same binade):
       gamma'''(x) is decreasing from +Inf to -0.529, where it is > 0
       for x <= c1, and < 0 for x >= nextabove(c1).
       Moreover for -1 < x <= c1, we see graphically that |gamma'''(x)|
       is bounded by 6/(1+x)^4.
       We have 3 cases:
       (a) if x0 < x1 < c1, the maximum is attained at x0,
           and is bounded by 6/(1+x0)^4
       (b) if c1 < x0 < c1, the maximum is attained at x1,
           and is bounded by 0.530
       (c) if x0 < c1 < x1, the maximum is attained at x0 or x1,
           and is bounded by max(6/(1+x0)^4,0.530).
       In all cases, the maximum is bounded by max(6/(1+x0)^4,0.530). */
    mpfr_add_ui (tmp, x0, 1, MPFR_RNDZ);
    mpfr_ui_div (tmp, 1, tmp, MPFR_RNDA);
    mpfr_sqr (tmp, tmp, MPFR_RNDU);
    mpfr_sqr (tmp, tmp, MPFR_RNDU);
    mpfr_mul_ui (tmp, tmp, 6, MPFR_RNDU);
    if (mpfr_cmp_d (tmp, 0.530) < 0)
      mpfr_set_d (tmp, 0.530, MPFR_RNDU);
  }
#ifdef USE_PARI
  else { // x0 < -1
    /* check that x0 and x1 are in the same interval (k,k+1) */
    long k = mpfr_get_si (x0, MPFR_RNDD);
    assert (mpfr_cmp_si (x0, k) > 0);    // x0 should not be an integer
    assert (mpfr_cmp_si (x1, k+1) <= 0); // x1 can equal k+1 since it is not tested
    /* gamma'''(x) is monotonic on (k,k+1), thus the maximum of |gamma'''(x)| is
       attained at x0 or x1 */
      mpfr_t u;
      mpfr_init2 (u, mpfr_get_prec (tmp));
      bound_gamma3_pari (tmp, x0);
      bound_gamma3_pari (u, x1);
      if (mpfr_cmp (u, tmp) > 0)
        mpfr_set (tmp, u, MPFR_RNDU);
      mpfr_clear (u);
  }
#endif
}
#endif // NEED_TABLES

/* BNDDIFF(tmp, t0, t1, N, d) returns a bound for the (d+1)-th
   derivative of gamma(x) for t0/N <= x < t1/N. So far we only implement
   it for d=1 and 1 <= t0/N < t1/N <= 0x1.573fae561f648p+7,
   where 0x1.573fae561f648p+7 is the smallest x such that gamma(x)>=2^1024 */
#define BNDDIFF(tmp, t0, t1, N, d)                              \
  do {                                                          \
    mpfr_t x1;                                                  \
    mpfr_init2 (x1, mpfr_get_prec (tmp));                       \
    assert (d <= 2);                                            \
    mpfr_set_z (tmp, t0, MPFR_RNDU);                            \
    mpfr_div (tmp, tmp, N, MPFR_RNDU);                          \
    mpfr_set_z (x1, t1, MPFR_RNDU);                             \
    mpfr_div (x1, x1, N, MPFR_RNDU);                            \
    if (d == 1)                                                 \
      bound_d2 (tmp, tmp, x1);                                  \
    else                                                        \
      bound_d3 (tmp, tmp, x1);                                  \
    mpfr_clear (x1);                                            \
  } while (0)

/* The derivative of gamma(x) is gamma(x)*psi(x),
   and the 2nd derivative is gamma(x)*(psi(x)^2 + trigamma(x)) */
#define TAYLOR(tmp, tm, Nin, P, NN, d)          \
  {                                             \
    assert (d <= 2);                            \
    mpfr_t psi;                                 \
    mpfr_init2 (psi, mpfr_get_prec (tmp));      \
    mpfr_set_z (tmp, tm, MPFR_RNDN);            \
    mpfr_div (tmp, tmp, Nin, MPFR_RNDN);        \
    /* x = tm/Nin */                            \
    mpfr_gamma (P[0], tmp, MPFR_RNDN);          \
    /* P[0] = gamma(x) */                       \
    mpfr_mul (P[0], P[0], NN, MPFR_RNDN);       \
    /* P[0] = NN*gamma(x) */                    \
    mpfr_digamma (psi, tmp, MPFR_RNDN);         \
    mpfr_mul (P[1], psi, P[0], MPFR_RNDN);      \
    mpfr_mul_z (P[1], P[1], T, MPFR_RNDN);      \
    mpfr_div (P[1], P[1], Nin, MPFR_RNDN);      \
    /* P[1] = NN*gamma'(x)*(T/Nin) */           \
    if (d >= 2) {                               \
      mpfr_trigamma (P[2], tmp, MPFR_RNDN);     \
      mpfr_sqr (psi, psi, MPFR_RNDN);           \
      mpfr_add (P[2], P[2], psi, MPFR_RNDN);    \
      mpfr_mul (P[2], P[2], P[0], MPFR_RNDN);   \
      /* P[0] includes the factor NN */         \
      mpfr_mul_z (P[2], P[2], T, MPFR_RNDN);    \
      mpfr_div (P[2], P[2], Nin, MPFR_RNDN);    \
      mpfr_mul_z (P[2], P[2], T, MPFR_RNDN);    \
      mpfr_div (P[2], P[2], Nin, MPFR_RNDN);    \
      /* divide by 2! */                        \
      mpfr_div_2ui (P[2], P[2], 1, MPFR_RNDN);  \
    }                                           \
    mpfr_clear (psi);                           \
  }

