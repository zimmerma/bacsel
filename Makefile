VERSION=4.0
DEFSAL= -DBASIS=10 -DEXP -DVBSE -DAUTOMATIC
CFLAGS=-O3 -g -fno-strict-aliasing -W -Wall -Wunused-parameter -Wunused-variable -Wunused-value -Wunused -Wuninitialized -fopenmp
DPE_STATUS="with"
# GMP=/usr
# MPFR=$(GMP)
# FPLLL=...
# a C++ compiler is required for the FPLLL interface
CXX=g++
INC=-I$(GMP)/include -I$(MPFR)/include -I$(FPLLL)/include
# could also use LIB=-Wl,-Bstatic -lfplll -lmpfr -lgmp -Wl,-Bdynamic -lm ...
# LIB=-L$(FPLLL)/lib -L$(MPFR)/lib -L$(GMP)/lib -l:libfplll.a -l:libmpfr.a -l:libgmp.a -lm -fopenmp
LIB=-L$(GMP)/lib -L$(MPFR)/lib -L$(FPLLL)/lib -lfplll -lmpfr -lgmp -lm -fopenmp $(PARI)
OBJS=bacsel.o mat.o io.o taylor.o slz.o hensel.o poly.o utils.o getexp.o fplll_wrapper.o pow.o
FILES=bacsel.c bacsel.h mat.c mat.h io.c io.h taylor.c taylor.h slz.c slz.h   \
      hensel.c hensel.h poly.c poly.h utils.c utils.h \
      getexp.c getexp.h fplll_wrapper.cpp fplll_wrapper.h cos.h delta_eta.h   \
      exp.h hall.h ln.h oneoverx.h sin.h sqrt.h rsqrt.h twotox.h       \
      sinbig.h function.h Makefile README COPYING NEWS test.sh test.out \
      acos.h tanh.h tan.h log.h log2.h log10.h log1p.h log2p1.h log10p1.h \
      exp2.h exp10.h cbrt.h asin.h asinh.h asinpi.h acosh.h acospi.h \
      atan.h atanh.h atanpi.h pow.h pow.c sinpi.h cospi.h tanpi.h \
      sinh.h cosh.h tanh.h erf.h erfc.h expm1.h exp2m1.h exp10m1.h j1.h \
      y0.h y1.h gamma.h lgamma.h

all : bacsel

.c.o: 
	$(CXX) $(DEFSAL) $(CFLAGS) $(INC) -c $^ -o $@

bacsel: $(OBJS)
	$(CXX) $^ -g -o $@ $(LIB)

fplll_wrapper.o: fplll_wrapper.cpp
	$(CXX) $(DEFSAL) $(CFLAGS) $(INC) -c -std=c++11 $< -o $@

check:
	./test.sh

clean:
	rm -f *~ *.o bacsel 

dist: $(FILES)
	mkdir bacsel-$(VERSION)
	cp $(FILES) bacsel-$(VERSION)
	tar cf bacsel-$(VERSION).tar bacsel-$(VERSION)
	gzip -f --best bacsel-$(VERSION).tar
	rm -fr bacsel-$(VERSION)

