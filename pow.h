/*
  Copyright 2007-2020 Guillaume Hanrot, Vincent LefÃ¨vre, Damien StehlÃ© 
  and Paul Zimmermann.

  This program is free software; you can redistribute it and/or modify 
  it under the terms of the GNU General Public License as published by 
  the Free Software Foundation; either version 2 of the License, or 
  (at your option) any later version.

  This program is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
  General Public License for more details.

  You should have received a copy of the GNU General Public License 
  along with this program; see the file COPYING.  If not, write to 
  the Free Software Foundation, Inc., 59 Temple Place - Suite 330, 
  Boston, MA 02111-1307, USA.
*/

/* This experimental wrapper is for the function x^y for a fixed value of y
   (given in hexadecimal on the command line). The value of y is stored in
   the global variable pow_y. */

extern mpfr_t arg_y;
void bnddiff_pow (mpfr_t tmp, const mpz_t t0, const mpz_t t1, mpfr_t N, int d);
void taylor_pow (mpfr_t tmp, mpz_t tm, mpfr_t Nin, mpfr_t *P, mpfr_t NN, int d,
                 mpz_t T);

#define NAME "x^y"

/* EVAL(x,rnd_mode) puts in x the value of x^y */
#define EVAL(x, rnd_mode) mpfr_pow (x, x, arg_y, rnd_mode);

/* bnddiff_pow and taylor_pow are defined in pow.c */

#define BNDDIFF bnddiff_pow

#define TAYLOR taylor_pow

