/*
  Copyright 2007-2020 Guillaume Hanrot, Vincent Lefèvre, Damien 
  Stehlé and Paul Zimmermann.

  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful, 
  but WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public 
  License along with this program; see the file COPYING.  If not, 
  write to the Free Software Foundation, Inc., 59 Temple Place - 
  Suite 330, Boston, MA 02111-1307, USA.
*/

#include <sys/time.h>
#include <sys/resource.h>
#include "utils.h"

/* return the cputime in milliseconds */
long
cputime ()
{
  struct rusage rus;

  getrusage (0, &rus);
  return (long)rus.ru_utime.tv_sec * 1000 + (long)rus.ru_utime.tv_usec / 1000;
}

long 
small_invert(long a, long b) /* Euclide */
{
  long u = 1, v = 0, w, q, b0 = b, c;
  
  while (b != 0) {
    q = a / b; 
    c = a - q*b; a = b; b = c; 
    w = v; v = u - q*v; u = w; 
  }
  if (a == -1) u = -u;  
  return (u > 0 ? u : u + b0); 
}

long
newton_invert(long a, long z, long b) /* Newton-Hensel */
{
  if (!z) return small_invert(a, b);
  return ((z << 1) - ((a * z) % b) * z) % b;
}
