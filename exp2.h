/*
Copyright 2007-2023 Guillaume Hanrot, Vincent Lefèvre, Damien Stehlé 
and Paul Zimmermann.

  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful, 
  but WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public 
  License along with this program; see the file COPYING.  If not, 
  write to the Free Software Foundation, Inc., 59 Temple Place - 
  Suite 330, Boston, MA 02111-1307, USA.
*/

#define NAME "exp2"
#define EVAL(y, rnd_mode) mpfr_exp2 (y, y, rnd_mode);

/* BNDDIFF(tmp,t0,t1,N,d) puts in tmp an upper bound for the (d+1)-th
   derivative of 2^x over [t0/N,t1/N] (d >= 1).
   The d-th derivative of 2^x is 2^x*log(2)^d,
   thus we bound by 2^x*log(2)^(d+1). */
#define BNDDIFF(tmp, t0, t1, N, d)                              \
  assert (mpz_cmp_ui (t0, 0) > 0);                              \
  mpfr_set_z (tmp, t1, MPFR_RNDU);				\
  mpfr_div (tmp, tmp, Nin, MPFR_RNDU);				\
  mpfr_exp2 (tmp, tmp, MPFR_RNDU);				\
  { mpfr_t ump;                                                 \
    mpfr_init2 (ump, mpfr_get_prec (tmp));                      \
    mpfr_log_ui (ump, 2, MPFR_RNDU);                            \
    mpfr_pow_ui (ump, ump, d+1, MPFR_RNDU);                     \
    mpfr_mul (tmp, tmp, ump, MPFR_RNDU);                        \
    mpfr_clear (ump); }

/* The coefficient of degree i of the Taylor expansion of
   NN*2^(tm/Nin+x) at x=0 is NN*2^(tm/Nin)*log(2)^i/i!,
   which we scale by (T/Nin)^i. */
#define TAYLOR(tmp, tm, Nin, P, NN, d)		\
  { long i;					\
    mpfr_set_z (tmp, tm, MPFR_RNDN);		\
    mpfr_div (tmp, tmp, Nin, MPFR_RNDN);        \
    mpfr_exp2 (P[0], tmp, MPFR_RNDN);		\
    mpfr_mul (P[0], P[0], NN, MPFR_RNDN);	\
    mpfr_set_ui (tmp, 2, MPFR_RNDN);		\
    mpfr_log (tmp, tmp, MPFR_RNDN);		\
    /* tmp = log(2) */                          \
    for (i=1; i<=d; i++) {			\
      mpfr_mul_z (P[i], P[i-1], T, MPFR_RNDN);	\
      mpfr_div (P[i], P[i], Nin, MPFR_RNDN);	\
      mpfr_div_ui (P[i], P[i], i, MPFR_RNDN);	\
      mpfr_mul (P[i], P[i], tmp, MPFR_RNDN);	\
    }                                           \
  }

