/*
  Copyright 2007-2022 Guillaume Hanrot, Vincent Lefèvre, Damien Stehlé
  and Paul Zimmermann.

  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful, 
  but WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public 
  License along with this program; see the file COPYING.  If not, 
  write to the Free Software Foundation, Inc., 59 Temple Place - 
  Suite 330, Boston, MA 02111-1307, USA.
*/

#ifdef EXP
#include "exp.h"
#endif

#ifdef COS
#define SIN_OR_COS
#include "cos.h"
#endif

#ifdef SIN
#define SIN_OR_COS
#include "sin.h"
#endif

#ifdef TAN
#include "tan.h"
#endif

#ifdef HALL
#include "hall.h"
#endif

#ifdef LOG
#include "log.h"
#endif

#ifdef LOG2
#include "log2.h"
#endif

#ifdef LOG10
#include "log10.h"
#endif

#ifdef LOG1P
#include "log1p.h"
#endif

#ifdef LOG2P1
#include "log2p1.h"
#endif

#ifdef LOG10P1
#include "log10p1.h"
#endif

#ifdef SQRT
#include "sqrt.h"
#endif

#ifdef RSQRT
#include "rsqrt.h"
#endif

#ifdef EXP2
#include "exp2.h"
#endif

#ifdef SINBIG
#include "sinbig.h"
#endif

#ifdef ONEOVERX
#include "oneoverx.h"
#endif

#ifdef CBRT
#include "cbrt.h"
#endif

#ifdef ACOS
#include "acos.h"
#endif

#ifdef POW
#include "pow.h"
#endif

#ifdef EXP10
#include "exp10.h"
#endif

#ifdef ASIN
#include "asin.h"
#endif

#ifdef ASINH
#include "asinh.h"
#endif

#ifdef ASINPI
#include "asinpi.h"
#endif

#ifdef ACOSH
#include "acosh.h"
#endif

#ifdef ACOSPI
#include "acospi.h"
#endif

#ifdef ATAN
#include "atan.h"
#endif

#ifdef ATANH
#include "atanh.h"
#endif

#ifdef ATANPI
#include "atanpi.h"
#endif

#ifdef SINPI
#include "sinpi.h"
#endif

#ifdef COSPI
#include "cospi.h"
#endif

#ifdef TANPI
#include "tanpi.h"
#endif

#ifdef SINH
#include "sinh.h"
#endif

#ifdef COSH
#include "cosh.h"
#endif

#ifdef TANH
#include "tanh.h"
#endif

#ifdef ERF
#include "erf.h"
#endif

#ifdef ERFC
#include "erfc.h"
#endif

#ifdef EXPM1
#include "expm1.h"
#endif

#ifdef EXP2M1
#include "exp2m1.h"
#endif

#ifdef EXP10M1
#include "exp10m1.h"
#endif

#ifdef J1
#include "j1.h"
#endif

#ifdef Y0
#include "y0.h"
#endif

#ifdef Y1
#include "y1.h"
#endif

#ifdef GAMMA
#include "gamma.h"
#endif

#ifdef LGAMMA
#include "lgamma.h"
#endif
