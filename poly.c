/*
  Copyright 2007-2020 Guillaume Hanrot, Vincent Lefèvre, Damien 
  Stehlé and Paul Zimmermann.

  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful, 
  but WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public 
  License along with this program; see the file COPYING.  If not, 
  write to the Free Software Foundation, Inc., 59 Temple Place - 
  Suite 330, Boston, MA 02111-1307, USA.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <gmp.h>
#include <mpfr.h>
#include "mat.h"
#include "poly.h"

/*********************************************************************/
/**********************************************************************
BASIC STUFF ON POLYNOMIALS OF THE SHAPE 
1 x ... x^ad y yx ... yx^((a-1)d) .... y^a

Rank= dx+dy/2 * (d(2a-dy+1)+2)   [if we start at rank=0].

 dx+(dy*(d*(2*alpha-dy+1)+2))/2+1
**********************************************************************/
/*********************************************************************/


/* Derivatives of polynomials: diff1 is toward x, diff2 is toward y,
   diff is mod p, deriv_pol is over the mpz_t's */

void 
diff1(long *p1x, long *P1, long p, int d, int alpha) 
{
  int k = 0, xd, yd; 
  
  for (yd = 0; yd <= alpha; yd++) 
    {
      for (xd = 0; xd < d*alpha - d*yd; xd++, k++)
        // P1[k] is the coefficient of x^xd*y^yd and
        // P1[k+1] is the coefficient of x^(xd+1)*y^yd and
	p1x[k] = (P1[k+1] * (xd + 1)) % p; 
      // there is no coefficient of x^(xd+1)*y^yd
      p1x[k] = 0; k++; 
    }
}

void 
diff2(long *p1y, long *P1, long p, int d, int alpha) 
{
  int k = 0, xd, yd; 

  for (yd = 0; yd <= alpha; yd++) 
    for (xd = 0; xd <= d*alpha - d*yd; xd++) 
      {
	if (xd <= d*alpha - d*yd - d) 
	  p1y[k] = (P1[k + d*alpha - d*yd + 1] * (yd + 1)) % p; 
	else p1y[k] = 0; 
	k++; 
      }
}

void 
deriv_pol1(mpz_t *p1x, mpz_t *P1, int d, int alpha)
{
  int k = 0, xd, yd; 

  for (yd = 0; yd <= alpha; yd++) 
    for (xd = 0; xd <= d*(alpha - yd); xd++) 
      {
	if (xd < d*(alpha - yd)) 
	  mpz_mul_ui(p1x[k], P1[k+1], (xd + 1)); 
	else mpz_set_ui(p1x[k], 0); 
	k++; 
      }
}

void 
deriv_pol2(mpz_t *p1y, mpz_t *P1, int d, int alpha)
{
  int k = 0, xd, yd; 

  for (yd = 0; yd <= alpha; yd++) 
    for (xd = 0; xd <= d*(alpha - yd); xd++) 
      {
	if (xd <= d*(alpha - yd) - d) 
	  mpz_mul_ui(p1y[k], P1[k + d*alpha - d*yd + 1], (yd + 1)); 
	else mpz_set_ui(p1y[k], 0); 
	k++; 
      }
}


/* Evaluation of polynomials: mod p, over the mpz_t's */

/* FIXME = Have to prove that no overflow for reasonable degrees !! */ 
long
eval_mod(long *P1, long x, long y, long p, int d, int alpha)
{
  int k = (1 + alpha) * (1 + d*alpha) - d*alpha*(alpha + 1)/2 - 1; 
  long xd, yd, res = 0, resx = 0; 

  for (yd = alpha; yd >= 0; yd--) 
    {
      resx = 0; 

      for (xd = d*alpha - d*yd; xd >= 0; xd--) 
	{
	  resx = (x * resx + P1[k]) % p; 
	  k--; 
	}

      res = (y * res + resx) % p; 
    }

  return res % p; 
}

void
eval_pol(mpz_t res, mpz_t *P1, mpz_t x, mpz_t y, mpz_t M, int d, int alpha)
{
  int k = (1 + alpha) * (1 + d*alpha) - d*alpha*(alpha + 1)/2 - 1, 
    xd, yd;
  mpz_t resx; 

  mpz_init(resx); 

  mpz_set_ui (res, 0); 

  for (yd = alpha; yd >= 0; yd--) 
    {
      mpz_set_ui(resx, 0); 

      for (xd = d*alpha - d*yd; xd >= 0; xd--) 
	{
	  mpz_mul(resx, resx, x); 
	  mpz_fdiv_r(resx, resx, M); 
	  mpz_add(resx, resx, P1[k]); 
	  k--; 
	}
      
      mpz_mul(res, res, y); 
      mpz_fdiv_r(res, res, M); 
      mpz_add(res, res, resx); 
    }

  mpz_fdiv_r(res, res, M); 
  mpz_clear(resx); 
}

// divide p in place by its content
void
divide_by_content (mpz_t *p, int d, int alpha)
{
  mpz_t c;
  int ncoefs = (alpha + 1) * (d * alpha + 2) / 2;
  mpz_init (c);
  mpz_set (c, p[0]);
  for (int k = 1; k < ncoefs; k++)
    mpz_gcd (c, c, p[k]);
  for (int k = 0; k < ncoefs; k++)
    mpz_divexact (p[k], p[k], c);
  mpz_clear (c);
}
