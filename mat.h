/*
  Copyright 2005-2021 Damien Stehl� and Paul Zimmermann.

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; see the file COPYING.  If not, write to the Free
  Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
  02111-1307, USA.
*/

typedef struct {
  mpz_t **coeff;
  int NumRows, NumCols;
} mat_ZZ;

mpz_t* init_vec (int);
void clear_vec (mpz_t *, int);
void print_vec (mpz_t *, int);
/* Compute the d-dimensional L1 norm of V and put it in tmp. */
void Norm1 (mpz_t, mpz_t *, int); 

void init_matrix (mat_ZZ *, int, int);
void clear_matrix (mat_ZZ *, int, int);

void print_mat (mpz_t **, int, int);
void allocate_matrix (mpz_t ***, int, int);
void free_matrix (mpz_t ***, int, int);
