/*
  Copyright 2007-2020 Guillaume Hanrot, Vincent Lefèvre, Damien Stehlé 
  and Paul Zimmermann.

  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful, 
  but WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public 
  License along with this program; see the file COPYING.  If not, 
  write to the Free Software Foundation, Inc., 59 Temple Place - 
  Suite 330, Boston, MA 02111-1307, USA.
*/

#define MAYBE_UNUSED __attribute__ ((unused))

void print_uni_pol_mpfr(mpfr_t *, int);
void print_uni_pol_mpz(mpz_t *, int);
void print_pol_mpz(mpz_t *, int, int);
void print_pol_long(long *, int, int);
void print_q (long *);
void print_args(mpfr_t, mpfr_t, double, double, double, mp_prec_t,
		int, int, double, int, int, mpz_t, mpfr_t);
int  check_exponents (const mpz_t, const mpz_t, int, int, mpz_t);
void split_exponents (const mpz_t, mpz_t, int, int);
void check_t0_t1 (const mpz_t, const mpz_t, mpfr_t);
void analyse_argv(int, char **, int *, mp_prec_t *, double *, double *,
		  double *, double *, int *, int *, int *, mpz_t,
		  mpz_t, unsigned long*, mpz_t, unsigned long*, mpfr_t);
