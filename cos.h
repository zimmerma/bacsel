/*
Copyright 2007-2023 Guillaume Hanrot, Vincent Lefèvre, Damien Stehlé
and Paul Zimmermann.

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; see the file COPYING.  If not, write to the Free
  Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
  02111-1307, USA.
*/

#define NAME "cos" 
#define EVAL(y, rnd_mode) mpfr_cos(y, y, rnd_mode); 
/* BNDDIFF(tmp, t0, t1, N, d) returns a bound for the (d+1)-th
   derivative of cos(x) for t0/N <= x < t1/N. Since the n-th derivative of
   cos(x) is +/-cos(x) or +/-sin(x), we use 1 as bound. */
#define BNDDIFF(tmp, t0, t1, N, d)              \
  mpfr_set_ui (tmp, 1, MPFR_RNDU);

#define TAYLOR(tmp, tmp2, tm, Nin, P, NN, d)            \
  { long i, j;						\
    mpfr_set_z (tmp, tm, MPFR_RNDN);			\
    mpfr_div (tmp, tmp, Nin, MPFR_RNDN);                \
    mpfr_cos (P[0], tmp, MPFR_RNDN);			\
    mpfr_mul (P[0], P[0], NN, MPFR_RNDN);		\
    for (i=1; i<=d; i++) {				\
      if (i%2==1){					\
	mpfr_sin (P[i], tmp, MPFR_RNDN);                \
	for (j=1; j<=i; j++)				\
	  mpfr_div_ui (P[i], P[i], j, MPFR_RNDN);	\
	if (i%4==1)					\
	  mpfr_neg (P[i], P[i], MPFR_RNDN);             \
      }							\
      else{						\
	mpfr_cos (P[i], tmp, MPFR_RNDN);                \
	for (j=1; j<=i; j++)				\
	  mpfr_div_ui (P[i], P[i], j, MPFR_RNDN);	\
	if (i%4==2)					\
	  mpfr_neg (P[i], P[i], MPFR_RNDN);             \
      }							\
      mpfr_mul (P[i], P[i], NN, MPFR_RNDN);		\
      mpfr_set_z (tmp2, T, MPFR_RNDN);			\
      mpfr_div (tmp2, tmp2, Nin, MPFR_RNDN);		\
      mpfr_pow_ui (tmp2, tmp2, i, MPFR_RNDN);		\
      mpfr_mul (P[i], P[i], tmp2, MPFR_RNDN);		\
    }                                                   \
  }
  
