/* Copyright 2007-2023 Guillaume Hanrot, Vincent Lefèvre, Damien Stehlé 
   and Paul Zimmermann.

  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful, 
  but WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public 
  License along with this program; see the file COPYING.  If not, 
  write to the Free Software Foundation, Inc., 59 Temple Place - 
  Suite 330, Boston, MA 02111-1307, USA.
*/

#include <assert.h>

#define NAME "erfc"
#define EVAL(y, rnd_mode) mpfr_erfc (y, y, rnd_mode);

#define CST1 0x1.20dd750429b6ep+1 /* upper approximation of 4/sqrt(pi) */

/* tmp <- 4*x*e^(-x^2)/sqrt(pi), where one can modify x */
static inline void
bound_d2 (mpfr_t tmp, mpfr_t x)
{
  mpfr_mul (tmp, x, x, MPFR_RNDD);         /* x^2 */
  mpfr_neg (tmp, tmp, MPFR_RNDU);          /* -x^2 */
  mpfr_exp (tmp, tmp, MPFR_RNDU);          /* exp(-x^2) */
  mpfr_mul (tmp, tmp, x, MPFR_RNDU);       /* x*exp(-x^2) */
  mpfr_mul_d (tmp, tmp, CST1, MPFR_RNDU);
}

/* CST2 is an upper approximation of 8*exp(-3/2)/sqrt(pi) */
#define CST2 0x1.01d16ab491fb6p+0

/* tmp <- 4*(2*x^2 - 1)*e^(-x^2)/sqrt(pi), where one can modify x */
static inline void
bound_d3 (mpfr_t tmp, mpfr_t x)
{
  mpfr_mul (x, x, x, MPFR_RNDN);           /* x^2 */
  mpfr_neg (tmp, x, MPFR_RNDN);            /* -x^2 */
  mpfr_exp (tmp, tmp, MPFR_RNDU);          /* exp(-x^2) */
  mpfr_mul_ui (x, x, 2, MPFR_RNDU);        /* 2*x^2 */
  mpfr_sub_ui (x, x, 1, MPFR_RNDU);        /* 2*x^2-1 */
  mpfr_mul (tmp, tmp, x, MPFR_RNDU);       /* (2*x^2-1)*e^(-x^2) */
  mpfr_mul_d (tmp, tmp, CST1, MPFR_RNDU);
}

/* BNDDIFF(tmp, t0, t1, N, d) returns a bound for the absolute value of
   the (d+1)-th derivative of erfc(x) for t0/N <= x < t1/N. We only implement
   it for d=1 and d=2 so far. For d=1 the 2nd derivative of erfc(x)
   is 4*x*e^(-x^2)/sqrt(pi), and is maximal in absolute value
   at X1=sqrt(2)/2. For d=2 the 3rd derivative of erfc(x)
   is -4*(2*x^2 - 1)*e^(-x^2)/sqrt(pi), which increases in absolute value
   from 0 to X2=sqrt(6)/2, and decreases from X2 to infinity, thus:
   (a) if t1/N < X2 or X2 < t0/N, the maximum is attained at t0/N or t1/N
   (b) otherwise t0/N < X2 < t1/N, and we bound by the value at X2, which is
       8*e^(-3/2)/sqrt(pi) */
#define X1 0x1.6a09e667f3bcdp-1 /* sqrt(2)/2 */
#define X2 0x1.3988e1409212ep+0 /* sqrt(6)/2 */
#define BNDDIFF(tmp, t0, t1, N, d) {                            \
    assert (d == 1 || d == 2);                                  \
    mpfr_t t;                                                   \
    mpfr_init2 (t, mpfr_get_prec (tmp));                        \
    if (d == 1) {                                               \
      mpfr_set_z (t, t1, MPFR_RNDU);                            \
      mpfr_div (t, t, N, MPFR_RNDU);                            \
      if (mpfr_cmp_d (t, X1) <= 0)                              \
        /* 2nd derivative is maximal at t1/N */                 \
        bound_d2 (tmp, t);                                      \
      else {                                                    \
        mpfr_set_z (t, t0, MPFR_RNDN);                          \
        mpfr_div (t, t, N, MPFR_RNDN);                          \
        if (mpfr_cmp_d (t, X1) >= 0)                            \
          /* 2nd derivative is maximal at t0/N */               \
          bound_d2 (tmp, t);                                    \
        else {                                                  \
          mpfr_set_d (t, X1, MPFR_RNDN);                        \
          bound_d2 (tmp, t);                                    \
        }                                                       \
      }                                                         \
    } else {                                                    \
      mpfr_t u;                                                 \
      mpfr_init2 (u, mpfr_get_prec (tmp));                      \
      mpfr_set_z (t, t0, MPFR_RNDN);                            \
      mpfr_div (t, t, N, MPFR_RNDN);                            \
      mpfr_set_z (u, t1, MPFR_RNDN);                            \
      mpfr_div (u, u, N, MPFR_RNDN);                            \
      /* if X2 < t or u < X2, the maximum is attained at t or u */ \
      if (mpfr_cmp_d (t, X2) > 0 || mpfr_cmp_d (u, X2) < 0) {   \
        bound_d3 (tmp, t);                                      \
        bound_d3 (t, u);                                        \
        if (mpfr_cmp_abs (t, tmp) > 0)                          \
          mpfr_swap (tmp, t);                                   \
      } else                                                    \
        mpfr_set_d (tmp, CST2, MPFR_RNDU);                      \
      mpfr_clear (u);                                           \
    }                                                           \
    mpfr_abs (tmp, tmp, MPFR_RNDN);                             \
    mpfr_clear (t);                                             \
  }

/* Put in P[0]..P[d] the Taylor expansion of NN*f(x) at x=tm/Nin,
   with the coefficient of degree i scaled by (T/Nin)^i. */
#define TAYLOR(tmp, tm, Nin, P, NN, d) {                        \
    assert (d == 1 || d == 2);                                  \
    mpfr_t tmp2, tmp3;                                          \
    mpfr_init2 (tmp2, mpfr_get_prec (tmp));                     \
    mpfr_init2 (tmp3, mpfr_get_prec (tmp));                     \
    mpfr_set_z (tmp2, T, MPFR_RNDN);                            \
    mpfr_div (tmp2, tmp2, Nin, MPFR_RNDN);                      \
    /* tmp2 = T/Nin */                                          \
    mpfr_set_z (tmp, tm, MPFR_RNDN);                            \
    mpfr_div (tmp, tmp, Nin, MPFR_RNDN);			\
    /* tmp = x = tm/Nin */                                      \
    mpfr_erfc (P[0], tmp, MPFR_RNDN);                           \
    mpfr_mul (P[0], P[0], NN, MPFR_RNDN);                       \
    /* the 1st derivative is -2*e^(-x^2)/sqrt(pi) */            \
    mpfr_mul (P[1], tmp, tmp, MPFR_RNDN);                       \
    mpfr_neg (P[1], P[1], MPFR_RNDN);                           \
    mpfr_exp (P[1], P[1], MPFR_RNDN);                           \
    mpfr_const_pi (tmp3, MPFR_RNDN);                            \
    mpfr_sqrt (tmp3, tmp3, MPFR_RNDN);                          \
    mpfr_div (P[1], P[1], tmp3, MPFR_RNDN);                     \
    mpfr_mul_si (P[1], P[1], -2, MPFR_RNDN);                    \
    /* multiply P[1] by T/Nin and by NN */                      \
    mpfr_mul (P[1], P[1], tmp2, MPFR_RNDN);                     \
    mpfr_mul (P[1], P[1], NN, MPFR_RNDN);                       \
    if (d == 2) {                                               \
      /* the 2nd derivative is 4*x*e^(-x^2)/sqrt(pi) */         \
      /* i.e., the 1st derivative multiplied by -2x */          \
      mpfr_mul (P[2], P[1], tmp, MPFR_RNDN);                    \
      /* multiply by -2 and divide by 2! */                     \
      mpfr_neg (P[2], P[2], MPFR_RNDN);                         \
      /* multiply by tmp2=T/Nin */                              \
      mpfr_mul (P[2], P[2], tmp2, MPFR_RNDN);                   \
      /* P[1] was already scaled by NN */                       \
    }                                                           \
    mpfr_clear (tmp2);                                          \
    mpfr_clear (tmp3);                                          \
  }

