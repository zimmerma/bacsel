/*
  Copyright 2007-2020 Guillaume Hanrot, Vincent Lefèvre, Damien Stehlé 
  and Paul Zimmermann.

  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful, 
  but WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public 
  License along with this program; see the file COPYING.  If not, 
  write to the Free Software Foundation, Inc., 59 Temple Place - 
  Suite 330, Boston, MA 02111-1307, USA.
*/

#define NAME "sinbig" 
#define EVAL(y, rnd_mode)			      \
  {                                                   \
  mpfr_t yy;                                          \
  mpfr_init2 (yy, LARGE_PREC);                        \
  mpfr_div (yy, y, twopi, MPFR_RNDN);		      \
  mpfr_round(yy, yy);				      \
  mpfr_mul (yy, yy, twopi, MPFR_RNDN);                \
  mpfr_sub (y, y, yy, MPFR_RNDN);		      \
  mpfr_sin (y, y, rnd_mode);                          \
  mpfr_clear (yy);                                    \
  }
/* BNDDIFF(tmp, t0, t1, N, d) returns a bound for the (d+1)-th
   derivative of sin(x) for t0/N <= x < t1/N. Since the n-th derivative of
   sin(x) is +/-cos(x) or +/-sin(x), we use 1 as bound. */
#define BNDDIFF(tmp, t0, t1, N, d)              \
  mpfr_set_ui (tmp, 1, MPFR_RNDU);
#define TAYLOR(tmp, tm, Nin, P, NN, d)				  \
  { long i;							  \
    mpfr_set_z (tmp, tm, MPFR_RNDN);				  \
    mpfr_div (tmp, tmp, Nin, MPFR_RNDN);				  \
    mpfr_set_z (tmp2, T, MPFR_RNDN);				  \
    mpfr_mul (tmp2, tmp2, tau, MPFR_RNDN);			  \
    mpfr_div (yy, tmp, pi, MPFR_RNDN);				  \
    mpfr_round (yy, yy);					  \
    mpfr_mul (yy, yy, pi, MPFR_RNDN);				  \
    mpfr_sub (tmp, tmp, yy, MPFR_RNDN);				  \
    mpfr_sin_cos (P[0], P[1], tmp, MPFR_RNDN);			  \
    mpfr_mul (P[0], P[0], NN, MPFR_RNDN);			  \
    mpfr_mul (P[1], P[1], NN, MPFR_RNDN);			  \
    mpfr_mul (P[1], P[1], tmp2, MPFR_RNDN);			  \
    mpfr_mul (tmp2, tmp2, tmp2, MPFR_RNDN);			  \
    for (i = 2; i <= d; i ++) {					  \
      mpfr_neg(P[i], P[i - 2], MPFR_RNDN);			  \
      mpfr_div_ui(P[i], P[i], i*(i-1), MPFR_RNDN);		  \
      mpfr_mul(P[i], P[i], tmp2, MPFR_RNDN);			  \
    }								  \
  }

