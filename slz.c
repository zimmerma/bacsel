/*
  Copyright 2007-2021 Guillaume Hanrot, Vincent Lefèvre, Damien Stehlé and
  Paul Zimmermann.

  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful, 
  but WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public 
  License along with this program; see the file COPYING.  If not, 
  write to the Free Software Foundation, Inc., 59 Temple Place - 
  Suite 330, Boston, MA 02111-1307, USA.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <gmp.h>
#include <mpfr.h>
#include "mat.h"
#include "io.h"
#include "hensel.h" /* must come before slz.h */
#include "slz.h"
#include "delta_eta.h" /* for DELTA and ETA */
#include "utils.h"
#include "fplll_wrapper.h"
#include "poly.h"

/*#define EVAL_HENSEL
#define EVAL_LLL
*/
#define NBIT_HENSEL NBIT
#define NBIT_LLL NBIT
#define NBIT 1000

#ifdef D3A2
/* This routine reconstructs the output of LLL in the case d=3 and alpha=2,
   as it would be without D3A2, i.e.:
   column   0 1  2   3   4   5   6  7  8   9   10   11
   monomial 1 t t^2 t^3 t^4 t^5 t^6 u tu t^2u t^3u u^2
   With D3A2, we have:
   column   0 1  2   3   4   5   6  7
   monomial 1 t t^2 t^3 t^4  u  tu t^2u
   thus we should:
   * move columns 5,6,7 to columns 7,8,9
   * reconstruct columns 5,6,10,11
   The reconstruction of columns 5,6,10,11 uses the fact that the corresponding
   monomials (t^5,t^6,t^3u,u^2) only appear in the last polynomial, thus it
   suffices to recover the multiplier k of that polynomial in the LLL-reduced
   matrix. But that does not seem possible when c is 0, since then column t^2u
   is zero, column t^3 and column u are dependent, same for columns t^4 and tu,
   thus it remains only 5 columns (1,t,t^2,u,tu) to identify 6 multipliers.
   Thus we assume c is not zero.
*/
void
reconstruct (mpz_t *Bi, mpz_t c, mpz_t d, mpz_t ztmp, unsigned long dd){

  /* Shift columns 5,6,7 by 2 */
  mpz_set (Bi[9], Bi[7]); /* column t^2u */
  mpz_set (Bi[8], Bi[6]); /* column tu */
  mpz_set (Bi[7], Bi[5]); /* column u */

  /* Complete columns 10 and 11 */
  /* column 9 (t^2u) has 6T^2c, column 10 (t^3u) has 6T^3d */
  if (mpz_sgn (c) == 0)
  {
    fprintf (stderr, "Error in reconstruct: c=0\n");
    exit (1);
  }
  mpz_divexact (ztmp, Bi[9], c); /* ztmp = 6*k */
  mpz_mul (Bi[10], d, ztmp);     /* 6T^3d*k */

  /* column 11 corresponds to u^2 */
  mpz_divexact_ui (ztmp, ztmp, 2); /* ztmp = 3*k */
  mpz_mul_ui (Bi[11], ztmp, dd);   /* 9*k */

  /* Complete columns 5, 6 */
  mpz_divexact_ui (ztmp, ztmp, dd); /* ztmp = k */
  mpz_mul (ztmp, ztmp, d);          /* T^3d*k */

  mpz_mul (Bi[6], ztmp, d);
  mpz_mul_ui (ztmp, ztmp, 2);
  mpz_mul (Bi[5], ztmp, c);
}
#endif


/**************************************************************************/
/**************************************************************************/
/* SLZ: from a polynomial approximation to a set of possible solutions,
   via lattice reduction and Hensel lifting */
/**************************************************************************/
/**************************************************************************/

/* Look for the solutions of the equation
   |P(x_0/T) cmod 1| < 1/C, where  |x0| \leq T and C = floor( 1/(1/M+eps)).
   ztmp and ztmp2 are auxiliary variables.
   Return value is either >= 0 in case of success, or a negative value in
   case of failure (-2 if coefficient of PP[d] is 0 in automatic mode).
*/
int
slz (mpz_t **Res, int *Nb, mpfr_t *P, mpz_t C, mpz_t T, 
     int d, int alpha, mpfr_t tmp,
     mat_ZZ *W, int npols, int ncoefs, 
     mpz_t ztmp, mpz_t Calpha, mpz_t ztmp2, int *ok, mpz_t *PP, 
     mpz_t *Ptmp,
     mpz_t **B_lll,
     unsigned long int *time_4_lll MAYBE_UNUSED,
     unsigned long int *time_4_Hensel MAYBE_UNUSED,
     mpz_t **Sol, hensel_t h)
{
  int dd = (d+3)/2;
  int i, j, k, dx, dy, res;
  int l;
#ifndef D3A2
  int use_d3a2 = 0;
#else
  int use_d3a2 = 1;
#endif

#ifdef VBSE
#ifdef EVAL_LLL
  int time1;
#endif
#endif

  *Nb = 0;

#if DEBUG >= 1
  printf ("\n\n*****ENTERING slz d=%d alpha=%d\n", d, alpha);
  printf ("ncoefs=%d, npols=%d\n", ncoefs, npols);
  printf ("C:="); mpz_out_str (stdout, 10, C); printf (";\n");
#endif

  /* Set PP= \lceil C*P \rfloor */
  for (i=0; i<=d; i++) {
    mpfr_mul_z (tmp, P[i], C, MPFR_RNDN);
    mpfr_get_z (PP[i], tmp, MPFR_RNDN);
  }

  /* If the upper coefficient of PP is zero, something strange happens in
     hensel(). This can be reproduced with revision b7f6b0d:
     make GMP=$GMP MPFR=$MPFR DEFSAL="-DACOS -DBASIS=2 -DAUTOMATIC -DDEBUG=2"
     ./bacsel -rnd_mode all -prec 128 -n 53 -nn 53 -m 43 -t 0 -t0 4545820956669212 -t1 4545820956669214 -d 2 -alpha 2 -v -e_in -9 -nthreads 1
     where PP:=31093302508524778756033342159 X^0 + -4294969383 X^1 + 0 X^2
     and hensel() fails for all pairs 1 <= i < j <= 6
     (all polynomials after LLL reduction split in two linear factors,
     and all resultants are zero). */
  if (d >= 2 && mpz_sgn (PP[d]) == 0)
  {
#ifndef AUTOMATIC
    fprintf (stderr, "Coefficient of degree d=%d of PP is zero\n", d);
    fprintf (stderr, "Maybe you should decrease d?\n");
    exit (1);
#else
    return -2;
#endif
  }

#if DEBUG >= 2
      printf ("PP:="); print_uni_pol_mpz (PP, d); printf("\n");
#endif

  /* |P(x_0/T) cmod 1| < 1/C  ==> PP(x_0/T) + y0 cmod C =0   
   where  |x0| \leq T and |y_0| \leq (d+3)/2 = dd.*/
#ifdef EVAL_LLL 
      int cptr_lll;
#if DEBUG >= 1
      int st_lll = cputime();
#endif
#ifdef VBSE
  time1 = cputime();
#endif
  for (cptr_lll = 0; cptr_lll < NBIT_LLL; cptr_lll++) { 
#endif
#if DEBUG >= 1
  printf ("\n**Starting filling the matrix**\n");
#endif

#ifdef D3A2
  /* reconstruct assumes c = PP[2] is not zero */
  if (mpz_sgn (PP[2]) == 0)
    use_d3a2 = 0;
#endif
  if (use_d3a2 == 0) {
  k = 1;

  mpz_set (W->coeff[1][0], Calpha);
  for (i=1; i<ncoefs; i++)
    mpz_set_ui (W->coeff[1][i], 0);
  
  for (j=0; j<=alpha; j++) {
    /* W1 is (PP(x)+[(d+3)/2]y)^jC^(alpha-j).
       When j increases, W1 := W1 * (PP(x)+[(d+3)/2] y) / C */

    mpz_set_ui (ztmp, 1);     /* Here, ztmp stores T^i */

    for (i=0; i<=alpha-j; i++) {
      /* Fill row nr k of B, ie, the one 
	 corresponding to x^i (PP(x)+[(d+3)/2]y)^jC^(alpha-j).
	 We have to shift W1 by X^i.      
      */

      /* For d=alpha=2, the order of the columns is:
         1 x x^2 x^3 x^4 y x*y x^2*y y^2 */

      for (dy=0; dy<=alpha; dy++)
	for (dx=0; dx <=d*(alpha-dy); dx++) {
	  if (dx<i) {
#if DEBUG >= 2
	    if (dx+(dy*(d*(2*alpha-dy+1)+2))/2+1 > ncoefs) abort();
	    if (dx+(dy*(d*(2*alpha-dy+1)+2))/2+1 <=0 ) abort();
#endif

	    mpz_set_ui(B_lll[k-1][dx+(dy*(d*(2*alpha-dy+1)+2))/2],0);
	  }
	  else {
#if DEBUG >= 1
	    if (dx+(dy*(d*(2*alpha-dy+1)+2))/2+1 > ncoefs) abort();
	    if (dx+(dy*(d*(2*alpha-dy+1)+2))/2+1 <=0 ) abort();
	    if (dx-i+(dy*(d*(2*alpha-dy+1)+2))/2 <0) abort();
	    if (dx-i+(dy*(d*(2*alpha-dy+1)+2))/2 >= ncoefs) abort();
#endif

	    mpz_mul (B_lll[k-1][dx+(dy*(d*(2*alpha-dy+1)+2))/2], 
		     ztmp, W->coeff[1][dx-i+(dy*(d*(2*alpha-dy+1)+2))/2]);
	  }
	}
      k++;
      mpz_mul (ztmp, ztmp, T);
    }

    if (j < alpha) { /* otherwise we are at the end of the loop */
      for (i=0; i<ncoefs; i++) mpz_set_ui (Ptmp[i], 0);
      
      for (l=0; l<=d; l++) {
	/*W2:=PP[l]*x^l*W1;  Ptmp += W2 */      
	
	for (dy=0; dy<=alpha; dy++)
	  for (dx=0; dx <=d*(alpha-dy); dx++){
	    if (dx<l) 
	      mpz_set_ui(W->coeff[2][dx+(dy*(d*(2*alpha-dy+1)+2))/2],0);
	    else
	      mpz_set (W->coeff[2][dx+(dy*(d*(2*alpha-dy+1)+2))/2], 
		       W->coeff[1][dx-l+(dy*(d*(2*alpha-dy+1)+2))/2]);
	  }
	
	for (i=0; i<ncoefs; i++){
	  mpz_mul (W->coeff[2][i], W->coeff[2][i], PP[l]);
	  mpz_add (Ptmp[i], Ptmp[i], W->coeff[2][i]);
	}
      }

      
#if DEBUG >= 2
      for (i = 0; i<ncoefs; i++) {
	mpz_out_str (stdout, 10, Ptmp[i]);
	printf(" ");
      }
      printf("\n");
#endif
      
      /*W2:=[(d+3)/2]*y*W1;  Ptmp += W2 */
      
      for (dy=0; dy<=alpha; dy++)
	for (dx=0; dx <=d*(alpha-dy); dx++){
	  if (dy==0) mpz_set_ui (W->coeff[2][dx], 0);
	  else {
	    mpz_set (W->coeff[2][dx+(dy*(d*(2*alpha-dy+1)+2))/2], 
		     W->coeff[1][dx+((dy-1)*(d*(2*alpha-dy+2)+2))/2]);
	  }
	}
      
      for (i=0; i<ncoefs; i++)
	mpz_addmul_ui (Ptmp[i], W->coeff[2][i], dd);
      
#if DEBUG >= 2
      for (i = 0; i<ncoefs; i++) {
	mpz_out_str (stdout, 10, Ptmp[i]);
	printf(" ");
      }
      printf("\n");
#endif
      
      for (i=0; i<ncoefs; i++){
	mpz_set (W->coeff[1][i], Ptmp[i]);
	mpz_divexact (W->coeff[1][i], W->coeff[1][i], C);
      }
    }
  }
  }
  else { /* D3A2 case (Y=3) */

  /*
     1   t      t^2             t^3       u        t^4      tu     t^2u
    C^2
        C^2T
               C^2T^2
    Ca  CbT    CcT^2           CdT^3      CY 
        CaT    CbT^2           CcT^3              CdT^4     CTY
    a^2 2abT  (b^2+2ac)T^2  (2ad+2bc)T^3 2aY  (2bd+c^2)T^4  2bTY   2cT^2Y

    The other columns are skipped (see comment before reconstruct).
  */

  /* Calpha=C^2, PP[0]=a, PP[1]=bT, PP[2]=cT^2, PP[3]=dT^3 */


  /* column 0: monomial 1 */
  mpz_set (B_lll[0][0], Calpha);    /* C^2 */
  mpz_set_ui (B_lll[1][0], 0);
  mpz_set_ui (B_lll[2][0], 0);
  mpz_mul (B_lll[3][0], PP[0], C);  /* Ca */
  mpz_set_ui (B_lll[4][0], 0);
  mpz_mul (ztmp, PP[0], PP[0]);
  mpz_set (B_lll[5][0], ztmp);      /* a^2 */

  /* column 1: monomial t */
  mpz_set_ui (B_lll[0][1], 0);
  mpz_mul (B_lll[1][1], Calpha, T);      /* C^2T */
  mpz_set_ui (B_lll[2][1], 0);
  mpz_mul (B_lll[3][1], PP[1], C);       /* CbT */
  mpz_mul (B_lll[4][1], B_lll[3][0], T); /* CaT */
  mpz_mul (ztmp, PP[1], PP[0]);
  mpz_mul_ui (B_lll[5][1], ztmp, 2);     /* 2abT */

  /* column 2: monomial t^2 */
  mpz_set_ui (B_lll[0][2], 0);
  mpz_set_ui (B_lll[1][2], 0);
  mpz_mul (B_lll[2][2], B_lll[1][1], T); /* C^2T^2 */
  mpz_mul (B_lll[3][2], PP[2], C);       /* CcT^2 */
  mpz_mul (B_lll[4][2], B_lll[3][1], T); /* CbT^2 */
  mpz_mul (ztmp, PP[1], PP[1]);          /* b^2T^2 */
  mpz_mul (ztmp2, PP[0], PP[2]);         /* acT^2 */
  mpz_mul_ui (ztmp2, ztmp2, 2);          /* 2acT^2 */
  mpz_add (B_lll[5][2], ztmp, ztmp2);    /* (b^2+2ac)T^2 */

  /* column 3: monomial t^3 */
  mpz_set_ui (B_lll[0][3], 0);
  mpz_set_ui (B_lll[1][3], 0);
  mpz_set_ui (B_lll[2][3], 0);
  mpz_mul (B_lll[3][3], PP[3], C);       /* CdT^3 */
  mpz_mul (B_lll[4][3], B_lll[3][2], T); /* CcT^3 */
  mpz_mul (ztmp, PP[1], PP[2]);          /* bcT^3 */
  mpz_mul (ztmp2, PP[0], PP[3]);         /* adT^3 */
  mpz_add (ztmp, ztmp, ztmp2);           /* (ad+bc)T^3 */
  mpz_mul_ui (B_lll[5][3], ztmp, 2);     /* (2ad+2bc)T^3 */

  /* column 4: monomial t^4 */
  mpz_set_ui (B_lll[0][4], 0);
  mpz_set_ui (B_lll[1][4], 0);
  mpz_set_ui (B_lll[2][4], 0);
  mpz_set_ui (B_lll[3][4], 0);
  mpz_mul (B_lll[4][4], B_lll[3][3], T); /* CdT^4 */
  mpz_mul (ztmp, PP[1], PP[3]);          /* bdT^4 */
  mpz_mul_ui (ztmp, ztmp, 2);            /* 2bdT^4 */
  mpz_mul (ztmp2, PP[2], PP[2]);         /* c^2T^4 */
  mpz_add (B_lll[5][4], ztmp, ztmp2);    /* (2bd+c^2)T^4 */

  /* monomial t^5 is skipped */
#if 0
  mpz_set_ui (B_lll[0][5], 0);
  mpz_set_ui (B_lll[1][5], 0);
  mpz_set_ui (B_lll[2][5], 0);
  mpz_set_ui (B_lll[3][5], 0);
  mpz_set_ui (B_lll[4][5], 0);
  mpz_mul (ztmp, PP[2], PP[3]);
  mpz_mul_ui (B_lll[5][5], ztmp, 2);
#endif

  /* monomial t^6 is skipped */
#if 0
  mpz_set_ui (B_lll[0][6], 0);
  mpz_set_ui (B_lll[1][6], 0);
  mpz_set_ui (B_lll[2][6], 0);
  mpz_set_ui (B_lll[3][6], 0);
  mpz_set_ui (B_lll[4][6], 0);
  mpz_mul (B_lll[5][6], PP[3], PP[3]);
#endif

  /* column 5: monomial u, where dd=Y */
  mpz_set_ui (B_lll[0][5], 0);
  mpz_set_ui (B_lll[1][5], 0);
  mpz_set_ui (B_lll[2][5], 0);
  mpz_mul_si (B_lll[3][5], C, dd);       /* CY */
  mpz_set_ui (B_lll[4][5], 0);
  mpz_mul_si (B_lll[5][5], PP[0], 2*dd); /* 2aY */

  /* column 6: monomial tu */
  mpz_set_ui (B_lll[0][6], 0);
  mpz_set_ui (B_lll[1][6], 0);
  mpz_set_ui (B_lll[2][6], 0);
  mpz_set_ui (B_lll[3][6], 0);
  mpz_mul (B_lll[4][6], B_lll[3][5], T); /* CTY */
  mpz_mul_si (B_lll[5][6], PP[1], 2*dd); /* 2bTY */


  /* column 7: monomial t^2u */
  mpz_set_ui (B_lll[0][7], 0);
  mpz_set_ui (B_lll[1][7], 0);
  mpz_set_ui (B_lll[2][7], 0);
  mpz_set_ui (B_lll[3][7], 0);
  mpz_set_ui (B_lll[4][7], 0);
  mpz_mul_si (B_lll[5][7], PP[2], 2*dd); /* 2cT^2Y */

  }

  /*
  print_mat (B_lll, npols, ncoefs-2); 
  exit(0);
  */

#if DEBUG >= 1
#if DEBUG >= 2
  printf("Matrix before LLL: \n");
  print_mat (B_lll, npols, ncoefs);
#endif
  printf ("**Entering LLL**\n");
#endif

  /*************** LLL-reduce the matrix B ****************/

  int ncoefs2 = ncoefs;
  if (use_d3a2)
    ncoefs2 = ncoefs - 4;

  LLLReduce (B_lll, npols, ncoefs2, mpfr_get_default_prec ());

#if DEBUG >= 1
  printf ("**Exit LLL**\n");
#endif

  /*
  print_mat (B_lll, npols, ncoefs); 
  exit (0);
  */

#ifdef EVAL_LLL
  }
#ifdef VBSE
  *time_4_lll += cputime()-time1;
#else
#if DEBUG >= 1
  printf("LLL %d\n", (cputime() - st_lll)/NBIT_LLL); 
#endif
#endif
#endif


#if DEBUG >= 2
  printf ("LLL reduction completed\n");
  print_mat (B_lll, npols, ncoefs); 
#endif


  /* Take the first two vectors, look if their L1 norm are small (< C^alpha),
     and transform them into two polynomials 
     If it fails, take the first and the third, etc.

     ok[i] is -1 if the length of B->coeff[i] has not been computed yet,
     0 if it has been computed but it is too long or zero,
     1 if it has been computed and W[i] has also been computed

     We are looking for the common roots of W1 and W2 over Z 
     that are of the shape (tsol/T, ysol/[(d+3)/2]T^d), where tsol and ysol
     are integers satisfying |tsol| <= T, |ysol| <= [(d+3)/2] T^d.
     
     For Hensel's lifting, we require integer roots, so we change
     W1, W2 to T^(d*alpha) Wi(x/T,y/[(d+3)/2]T^d), 
     so that we will find tsol and ysol, of which only tsol is of interest.
     
     We did not perform this change of variables before lattice reduction
     because this would have been more expensive.
  */

  res = -1;
  for (i=1; i<=npols; i++) ok[i]=-1;
  
  for (i = 1; (res == -1) && (i < npols); i++) {
    if (ok[i] == -1) { // not treated yet

#ifdef D3A2
      if (use_d3a2)
        reconstruct (B_lll[i-1], PP[2], PP[3], ztmp, dd);
#endif

      /*
	for (k=0; k<12; k++){
	mpz_out_str (stderr, 10, B_lll[i-1][k]);
	fprintf (stderr, " ");
	}
	fprintf (stderr, "\n");
      */


      Norm1 (ztmp, B_lll[i-1], ncoefs);
      if (mpz_cmp (ztmp, Calpha)>=0) ok[i] = 0;
      else if (mpz_cmp_ui (ztmp, 0) == 0) ok[i] = 0;
      else ok[i]=1;
#if DEBUG >= 1
      printf ("i=%d, ok[%d]=%d\n", i, i, ok[i]);
#endif  
      if (ok[i] == 1) {
	for (k=0; k<ncoefs; k++) 
	  mpz_set (W->coeff[i][k], B_lll[i-1][k]);

	mpz_pow_ui (ztmp, T, d*alpha);
	for (k=0; k<ncoefs; k++) 
	  mpz_mul (W->coeff[i][k], W->coeff[i][k], ztmp);
	
	for (dy=0; dy<=alpha; dy++)
	  for (dx=0; dx <=d*(alpha-dy); dx++){
	    /* divide x^dx y^dy by T^(dx+d*dy)*[(d+3)/2]^dy */
	    mpz_pow_ui (ztmp, T, dx+d*dy);
	    mpz_set_ui (ztmp2, dd);
	    mpz_pow_ui (ztmp2, ztmp2, dy);
	    mpz_mul (ztmp, ztmp, ztmp2);

	    if (mpz_sgn (ztmp)!=0)
	      mpz_divexact (W->coeff[i][dx+(dy * (d*(2*alpha-dy+1)+2))/2], 
			    W->coeff[i][dx+(dy * (d*(2*alpha-dy+1)+2))/2], 
			    ztmp);
	    else mpz_set_ui (W->coeff[i][dx+(dy * (d*(2*alpha-dy+1)+2))/2], 0);
	  }
	
#if DEBUG >= 2
	printf("This is W[%d] \n", i);
	for (k = 0; k<ncoefs; k++) {
	  mpz_out_str (stdout, 10, W->coeff[i][k]);
	  printf(" ");
	}
	printf("\n");
#endif
	
          // divide W[i] by its content (will make Hensel lifting easier)
          divide_by_content (W->coeff[i], d, alpha);
      }
    }
    if (ok[i]==0) continue;
    
    for (j = i+1; (res==-1) && (j<=npols); j++) {
#if DEBUG >= 1
      printf ("j=%d\n", j);
#endif      
      if (ok[j] == -1) { // not treated yet
#ifdef D3A2
        if (use_d3a2)
          reconstruct (B_lll[j-1], PP[2], PP[3], ztmp, dd);
#endif

      /*
	for (k=0; k<12; k++){
	mpz_out_str (stderr, 10, B_lll[j-1][k]);
	fprintf (stderr, " ");
	}
	fprintf (stderr, "\n");
      */

	Norm1 (ztmp, B_lll[j-1], ncoefs);
	if (mpz_cmp (ztmp, Calpha)>=0) ok[j]=0;
	else if (mpz_cmp_ui (ztmp, 0) == 0) ok[j] = 0;
	else ok[j] = 1;

#if DEBUG >= 2 
	printf ("j=%d, ok[%d]=%d\n", j, j, ok[j]);
#endif  
	
	if (ok[j] == 1) {
	  for (k=0; k<ncoefs; k++)
	    mpz_set (W->coeff[j][k], B_lll[j-1][k]);

	  mpz_pow_ui (ztmp, T, d*alpha);
	  for (k=0; k<ncoefs; k++) 
	    mpz_mul (W->coeff[j][k], W->coeff[j][k], ztmp);
	  
	  for (dy=0; dy<=alpha; dy++)
	    for (dx=0; dx <=d*(alpha-dy); dx++){
	      /* divide x^dx y^dy by T^(dx+d*dy)*[(d+3)/2]^dy */
	      mpz_pow_ui (ztmp, T, dx+d*dy);
	      mpz_set_ui (ztmp2, dd);
	      mpz_pow_ui (ztmp2, ztmp2, dy);
	      mpz_mul (ztmp, ztmp, ztmp2);
	      if (mpz_sgn (ztmp)!=0)
		mpz_divexact (W->coeff[j][dx+(dy * (d*(2*alpha-dy+1)+2))/2], 
			      W->coeff[j][dx+(dy * (d*(2*alpha-dy+1)+2))/2], 
			      ztmp);
	      else 
		mpz_set_ui (W->coeff[j][dx+(dy * (d*(2*alpha-dy+1)+2))/2], 0);
	    }

#if DEBUG >= 2
	printf("This is W[%d] \n", j);
	for (k = 0; k<ncoefs; k++) {
	  mpz_out_str (stdout, 10, W->coeff[j][k]);
	  printf(" ");
	}
	printf("\n");
#endif
	
          // divide W[j] by its content (will make Hensel lifting easier)
          divide_by_content (W->coeff[j], d, alpha);
	}
      }
      if (ok[j]==0) continue;
      
#if DEBUG >= 1
      printf ("**Passed the norm conditions for i=%d and j=%d\n", i, j);
#endif 
      
      mpz_pow_ui (ztmp, T, d);
      mpz_mul_ui (ztmp, ztmp, dd);
      mpz_add_ui (ztmp, ztmp, 1);

#ifdef EVAL_HENSEL
      int st = cputime();
      int li;  
#ifdef VBSE
	time1 = cputime();
#endif
      for (li = 0; li < NBIT_HENSEL; li++) 
#endif
        res = hensel (Nb, W->coeff[i], W->coeff[j], T, d, alpha, Sol, h);

#ifdef EVAL_HENSEL
#ifdef VBSE
      *time_4_Hensel += cputime()-time1;
#else
#if DEBUG >= 1
      printf("Hensel %d\n", (cputime() - st) / NBIT_HENSEL); 
#endif
#endif
#endif


#if DEBUG >= 2
      printf ("Nb[0] is %d\n", Nb[0]);
      if (Nb[0]>0) gmp_printf ("Sol[0][0] is %Zd\n", Sol[0][0]);
#endif
      
    }
  }

  if ((res==1)&&(Nb[0] > 0)) {
    Res[0] = (mpz_t *) malloc(Nb[0]*sizeof(mpz_t)); 
    
    for (j=0; j< Nb[0]; j++) {
      mpz_init (Res[0][j]);
      mpz_set (Res[0][j], Sol[0][j]);
    }      
  }


#if DEBUG >= 2
      printf ("Nb[0] is %d\n", Nb[0]);
      if (Nb[0] > 0) gmp_printf ("Res[0][0] is %Zd\n", Res[0][0]);
#endif


#if DEBUG >= 1
  printf ("*****EXITING slz*****\n\n\n");
#endif  

  return res;
}
