/* Copyright 2007-2023 Guillaume Hanrot, Vincent Lefèvre, Damien Stehlé 
   and Paul Zimmermann.

  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful, 
  but WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public 
  License along with this program; see the file COPYING.  If not, 
  write to the Free Software Foundation, Inc., 59 Temple Place - 
  Suite 330, Boston, MA 02111-1307, USA.
*/

#define NAME "cosh"
#define EVAL(y, rnd_mode) mpfr_cosh (y, y, rnd_mode);
/* BNDDIFF(tmp, t0, t1, N, d) returns a bound for the (d+1)-th
   derivative of cosh(x) for t0/N <= x < t1/N. Since the n-th derivative of
   cosh(x) is sinh(x) or cosh(x), we use cosh(t1/N) as bound,
   assuming 0 <= t0 < t1. */
#define BNDDIFF(tmp, t0, t1, N, d) {            \
    mpfr_set_z (tmp, t1, MPFR_RNDU);            \
    mpfr_div (tmp, tmp, N, MPFR_RNDU);          \
    mpfr_cosh (tmp, tmp, MPFR_RNDU);            \
}

/* Put in P[0]..P[d] the Taylor expansion of NN*f(tm/Nin+x) at x=0,
   with the coefficient of degree i scaled by (T/Nin)^i.
   The n-th derivative of cosh(x) is:
   * sinh(x) for n odd
   * cosh(x) for n even */
#define TAYLOR(tmp, tm, Nin, P, NN, d)                          \
  { long i, j;                                                  \
    mpfr_t tmp2;                                                \
    mpfr_init2 (tmp2, mpfr_get_prec (tmp));                     \
    mpfr_set_z (tmp, tm, MPFR_RNDN);                            \
    mpfr_div (tmp, tmp, Nin, MPFR_RNDN);			\
    /* tmp = tm/Nin */                                          \
    mpfr_cosh (P[0], tmp, MPFR_RNDN);                           \
    mpfr_mul (P[0], P[0], NN, MPFR_RNDN);                       \
    for (i=1; i<=d; i++) {                                      \
      if (i%2==1)                                               \
	mpfr_sinh (P[i], tmp, MPFR_RNDN);                       \
      else                                                      \
        mpfr_cosh (P[i], tmp, MPFR_RNDN);                       \
      for (j=1; j<=i; j++)                                      \
        mpfr_div_ui (P[i], P[i], j, MPFR_RNDN);                 \
      mpfr_mul (P[i], P[i], NN, MPFR_RNDN);                     \
      mpfr_set_z (tmp2, T, MPFR_RNDN);                          \
      mpfr_div (tmp2, tmp2, Nin, MPFR_RNDN);                    \
      mpfr_pow_ui (tmp2, tmp2, i, MPFR_RNDN);                   \
      mpfr_mul (P[i], P[i], tmp2, MPFR_RNDN);                   \
    }                                                           \
    mpfr_clear (tmp2);                                          \
    }

